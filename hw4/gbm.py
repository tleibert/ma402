# %%
# gbm.py
# Simulate the stock marked using Geometric Brownian Motion
import numpy
import matplotlib.pyplot as plt
# %%----------------------#

NTRIALS = 100 # number of trials
T = 30 # number of years to simulate
SPEND = 60000 # expendatures per year

mu = 0.05 # yearly rate
sg = 0.04 # volatility
MONEY = 1000000 # starting money

# %%----------------------#


nsteps = 12*T
dt = float(T)/nsteps; 

ss = numpy.zeros(nsteps+1)
ts = numpy.linspace(0,T,nsteps+1)
endvals = numpy.zeros(NTRIALS)

plt.figure()
plt.subplot(1,2,1)

for n in range(NTRIALS):
    s = MONEY
    for itn in range(nsteps):
        ss[itn] = s 
        s += s*(mu*dt + sg*numpy.random.normal()*numpy.sqrt(dt)) - SPEND*dt
        s = max(s,0) # This kicks in if you go broke

    ss[-1] = s
    endvals[n] = s
    plt.plot(ts,ss)
    
plt.title("Portfolio Value over Time")
plt.ylabel("Value (dollars)")
plt.xlabel("Time (years)")

plt.subplot(1,2,2)
plt.hist(endvals,bins=30)
plt.locator_params(axis='x',nbins=4)

plt.title("Portfolio Value at Time T")
plt.ylabel("Value")
plt.xlabel("Frequency")

plt.show()
# %%
