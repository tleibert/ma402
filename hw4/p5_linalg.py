# %% imports
import numpy as np

# %% define matrix
A = np.matrix(np.array([
    [9, 6, 0],
    [12, 8, 6],
    [0, 0, 4]
]))

# %% a
print(A @ A.T)
# %% b
print(A.T @ A)
# %% c
print(np.linalg.matrix_rank(A))
# %% d
# transpose has same rank as original
print(np.linalg.matrix_rank(A.T))
# %% e
v = np.matrix("2;-3;0")
print(A * v)
# %% f
e1 = np.matrix(np.eye(3)[:,0]).T
e2 = np.matrix(np.eye(3)[:,1]).T
e3 = np.matrix(np.eye(3)[:,2]).T

# the straightfoward answer
print(e1.T * A ** 2 * e2)
# %%
