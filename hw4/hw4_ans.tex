\documentclass{article}[10pt]

% PACKAGES AND DEFINITIONS
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{graphicx,enumitem,multicol}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{dsfont}
\usepackage{minted}

\theoremstyle{definition}
\newtheorem{problem}{Problem}
\specialcomment{solution}{{\bf Solution: }}{}
\setlength{\parindent}{0pt}

\newcommand{\x}{{\bf x}}
\newcommand{\y}{{\bf y}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\1}{\mathds{1}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\unif}{\mathcal{U}}
\newcommand{\inv}{^{-1}}
\newcommand{\ts}{^{T}}

\DeclareMathOperator*{\var}{Var}
\DeclareMathOperator*{\prob}{Pr}
\DeclareMathOperator*{\Ber}{Ber}
%\excludecomment{solution}

\begin{document}
	
	
\title{Homework \#4}
\author{Trevor Leibert}	
\date{\today}

\maketitle



% Problem: Double Exponential
\begin{problem}[Mixed Random Variable]
	Let $X$ be a $\text{Ber}(1/2)$ Bernoulli random variable (i.e. a fair 0/1 coin flip) and let $Y$ be a $\text{Unif}(0,1)$ random variable such that $X$ and $Y$ are independent. Let $Z = \begin{cases}
		1/2 & X = 1,\\
		Y 	& X = 0.
	\end{cases}$ 
	
	In other words, consider a game where you flip a coin and spin a uniform (0,1) spinner. If the coin lands on heads, you get 50 cents. If it lands on tails, you get the amount shown on the spinner. 
	
	\begin{enumerate}[label=\alph*)]
		\item What is $P[Z \leq 0.4]$? Hint: it may help to break things into cases depending on whether $X = 0$ or $X = 1$. 
		\item What is $P[Z \leq 0.6]$? 
		\item Describe or sketch the CDF of $Z$. 
		\item Does the PDF of $Z$ exist? If so, what is it? If not, why not? 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item $P[Z\leq 0.4] = 0.2$\\
		Case 1: X = 1. In this case Z cannot be below 0.4, it will always be 0.5. Case 2: X = 0. In this case, Z depends on Y having a value $\leq$ 0.4. This will happen with a probability of 0.4, as Y is uniformly distributed between 0 and 1.\\
		Combining these cases, we multiply the 0.5 probability that we even get to sample Y by the 0.4 probability of Y being less than or equal to 0.4, to get $P[Z\leq 0.4] = 0.2$.
		\item $P[Z\leq 0.6] = 0.8$\\
		Case 1: X = 1. In this case the condition is met.\\
		Case 2: X = 0. In this case Y must return a value $\leq$ 0.6. This will happen with a probability of 0.6, as Y is uniformly distributed between 0 and 1. The probability of both of these events happening is $0.5 \cdot 0.6 = 0.3$.\\
		Because case 1 and case 2 are mutally exclusive, we can add their probabilities together to get the total probability of either event happening.
		\[
			P[Z\leq 0.6] = 0.5 + 0.3 = 0.8
		\]
		\item The CDF of $Z$ is dependent on both X and Y, which would imply it is best depicted in 3d. 
		However, because X is Bernoulli(1/2) it would be easy to depict it on a 2d plane. 
		The graph would have two ``sections''--one where X is 0, and one where X is 1.
		In the section where X is zero, the plot is a line starting at zero and going up to 1/2.
		When X is 1, this graph jumps immediately to have a static value of 1.
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[scale=0.3]{p1_cdf_sketch.jpg}
			\end{center}
			\caption{My sketch of $Z$'s CDF.}
		\end{figure}
		\item The PDF corresponding to $Z$'s CDF does not exist, as $Z$'s CDF is not continuous/continuously differentiable.
	\end{enumerate}
\end{solution}

\pagebreak

% Problem: Joke
\begin{problem}[Hilarious joke]
	Among the Amazon reviews of the book {\it A Million Random Digits with 100,000 Normal Deviates}\footnote{\url{https://www.amazon.com/Million-Random-Digits-Normal-Deviates/dp/0833030477/}}, one five-star review offered the following critique: 
	\begin{it}
		\begin{quote}
			Save yourself the time, and if you need a random number, just sort of think of a random number in your head and write it down. Odds are it's in the book already, and you saved yourself $\$80$.
		\end{quote}
	\end{it}
	Please explain why this would be a bad idea. 
\end{problem}

\begin{solution}
	This is a bad idea for many reasons, the simplest being that there will \emph{always} be an unconscious bias in the ``random'' numbers that a person thinks of. Even if there were no bias, a person would not be able to just ``think'' of normally distributed numbers.
\end{solution}

\pagebreak

% Hospital problem
\begin{problem}[Hospital Problem]
	The county hospital is located at the center of a square whose sides are 3 miles wide. If an accident occurs within this square, then the hospital sends out an ambulance. The road network is rectangular, so the travel distance from the hospital, whose coordinates are $(0, 0)$, to the point $(x_1, x_2)$ is $|x_1| + |x_2|$ (that is, the 1-norm or the Manhattan distance).
	\begin{enumerate}[label=\alph*)]
		\item Assume an accident occurs at a point that is uniformly distributed in the square. Show that the expected value of the travel distance is exactly equal to 1.5. One way to solve this is to write the expected value in terms of a double integral, although it is not strictly necessary to do so. 
		\item Suppose the hospital sends a helicopter instead, which can fly directly to the accident. Express $\E[\|(x_1,x_2)\|_2]$ in terms of a double integral. You do not have to solve or estimate the integral.
	\end{enumerate}
	
	
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item Travel distance in this case is defined as $D = \|X\|_1$, where X is a 2-dimensional vector with uniformly distributed points from -1.5 to 1.5 for both axes. If we redefine X to be the x-axis points, and $Y$ to be the y-axis points, we can consider each to be its own independent uniform random variable:
		\begin{align*}
			D &= |X| + |Y| \text{ for } X,Y \sim \mathcal{U}(-1.5, 1.5) \\
			\E[D] &= \E[|X| + |Y|] \\
			\E[D] &= \E[|X|] + \E[|Y|]
		\end{align*}
		We can utilize the definition of expected value for a Uniform distribution:
		\[
			\E[\mathcal{U}(a, b)] = \frac{a + b}{2}
		\]
		In this case, the distribution of $E[|X|]$ is from from 0 to 1.5, instead of $\E[X]$, which is from -1.5 to 1.5, as it is encased in an absolute value.
		Since the distribution is symmetric around zero, each negative ``counterpart'' directly maps to its negated positive self, not affecting the probability of getting any particular number, as \emph{each and every} value has effectively double the probability of being hit.
		We can continue to sub the values of $\E[|X|]$ and $\E[|Y|]$.
		\begin{align*}
			\E[D] &= \frac{1.5 + 0}{2}  + \frac{1.5 + 0}{2} \\
			\E[D] &= \frac{1.5}{2} + \frac{1.5}{2} \\
			\E[D] &= 0.75 + 0.75 \\
			\E[D] &= 1.5
		\end{align*}

		\item Using our definitions of $X$ and $Y$ from earlier:
		\begin{align*}
			D &= \|(X, Y)\|_2 \\
			D &= \sqrt{X^2 + Y^2} \\
			\E[D] &= \E\left[\sqrt{X^2 + Y^2}\right] \\
			\E[D] &= \int_{-1.5}^{1.5}\int_{-1.5}^{1.5} xy\sqrt{x^2 + y^2} \,dy\,dx
		\end{align*}
	\end{enumerate}
\end{solution}

% Stock Market
\begin{problem}[Retirement Planning]
	Modify the code \texttt{gbm.m} (or \texttt{gbm.py}) on the course website to answer the following questions. In all cases, suppose you have 1,000,000 dollars saved to begin with. 
	\begin{enumerate}[label = \alph*)]
		\item If you spend 50,000 dollars each year, what is the approximate probability you will go broke within the next 30 years? Here, take $\mu = 0.04$ and $\sigma = 0.04$. 
		\item Same as part a), but what if $\sigma = 0.02$ instead? 
		\item Same as part a), but what if you spend 60,000 dollars a year instead? 
		\item Same as part c), but what if the stock market grows around 5 percent per year instad of 4 percent? 
	\end{enumerate}
\end{problem}	

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item Spending 50,000 dollars a year and starting at 1,000,000 dollars, we go broke about 23\% of the time:
		\item With $\sigma$ changed to 0.02, we only go broke less than 2\% of the time:
		\item With 60,000 dollars per year of expenditures and $\sigma=0.04$, we go broke a bit more than 70\% of the time, with the earliest time at about 17 years.
		\item With the $\mu = 0.05$, meaning the stock market grows 5\% per year, we go broke about 27\% of the time with \$60,000 in expenditures every year.
	\end{enumerate}
\end{solution}

\begin{problem}[Linear Algebra]
	Consider the matrix 
	\[ 
	 {\bf A} = \begin{bmatrix}
	 	9 & 6 & 0\\ 12 & 8 & 6 \\ 0 & 0 & 4
	 \end{bmatrix}
	\]
	\begin{enumerate}[label = \alph*)]
		\item What is ${\bf AA}^T$? 
		\item What is ${\bf A}^T{\bf A}$? 
		\item What is the rank of ${\bf A}$? 
		\item What is the rank of ${\bf A}^T$? How can you quickly determine the answer to this based on the answers to the previous parts? 
		\item If ${\bf A}$ has full rank, solve ${\bf Ax} = {\bf e}_1$. If not, find a nonzero vector ${\bf v}$ such that ${\bf Av} = {\bf 0}$. 
		\item What is ${\bf e}_1\ts {\bf A}^2{\bf e}_2$? Find a way to solve this problem without computing ${\bf A}^2$ explicitly. 
	\end{enumerate}
\end{problem}

\begin{solution}
	My code is in the file \texttt{p5\_linalg.py}
	\begin{enumerate}[label=\alph*)]
		\item 
		\[
			\bf{AA}^T = \begin{bmatrix}
				117 & 156 & 0 \\ 156 & 244 & 24 \\ 0 & 24 & 16
			\end{bmatrix}
		\]
		\item
		\[
			\bf{A}^T\bf{A} = \begin{bmatrix}
				255 & 150 & 72 \\ 150 & 100 & 48 \\ 72 & 48 & 52
			\end{bmatrix}
		\]
		\item
		\[
			\text{rank}[\bf{A}] = 2
		\]
		\item We already know {\bf A}'s rank is 2, and as transposing is merely the act of swapping the rows and the columns, the rank of ${\bf A}^T$ will be the same as the rank of {\bf A}, 2.
		\item A does not have full rank, so we are solving ${\bf Av} = {\bf 0}$. Columns 1 and 2 are both linearly dependent, and can be cancelled, while column 3 can be discarded:
		\[
			\bf{v} = \begin{bmatrix}
				2 \\ -3 \\ 0
			\end{bmatrix}
		\]
		\item To avoid explicitly calculating ${\bf A}^2$ we can split this calculation up:
		\[
			{\bf e}_1\ts {\bf A}^2{\bf e}_2 = ({\bf e}_1\ts{\bf A})({\bf A}{\bf e}_2)
		\]
		Now this operation, instead of involving a daunting matrix power operation, is merely selecting row 1 from A, selecting column 2 from A, and then dotting those two vectors together:
		\begin{gather*}
			\left(
			\begin{bmatrix}
				1&0&0
			\end{bmatrix}
			\begin{bmatrix}
				9 & 6 & 0\\ 12 & 8 & 6 \\ 0 & 0 & 4
			\end{bmatrix}
			\right)
			\left(
			\begin{bmatrix}
				9 & 6 & 0\\ 12 & 8 & 6 \\ 0 & 0 & 4
			\end{bmatrix}
			\begin{bmatrix}
				0 \\ 1 \\ 0
			\end{bmatrix}
			\right) \\
			\begin{bmatrix}
				9 & 6 & 0
			\end{bmatrix}
			\begin{bmatrix}
				6 \\ 8 \\ 0
			\end{bmatrix} \\
			6 \cdot 9 + 8 \cdot 6 \\
			54 + 48 \\
			102
		\end{gather*}
	\end{enumerate}
\end{solution}

\begin{problem}[Householder reflections]
	Let ${\bf u} \in \R^n$ be a unit vector, and let ${\bf H} = {\bf I}_{n} - 2{\bf uu}\ts$. 
	\begin{enumerate}[label=\alph*)]
		\item Show that ${\bf H}$ is symmetric. 
		\item Show that ${\bf H}$ is orthogonal. 
		\item What is the rank of ${\bf H}$? 
		\item For any vector ${\bf v}$, consider the {\it orthogonal decomposition} ${\bf v} = \alpha {\bf u} + \beta {\bf u}^{\bot}$, where ${\bf u}\ts {\bf u}^{\bot} = 0$. Find ${\bf H}{\bf v}$ in terms of $\alpha$, $\beta$, ${\bf u}$, and ${\bf u}^{\bot}$. 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item Definition of symmetric:
		\[
			{\bf H} = {\bf H}\ts
		\]
		Working from substitution and using the properties of the transpose:
		\begin{align*}
			{\bf I}_{n} - 2 {\bf uu}\ts &= \left({\bf I}_{n} - 2 {\bf uu}\ts\right)\ts \\
			{\bf I}_{n} - 2 {\bf uu}\ts &= {\bf I}_{n}\ts - \left(2 {\bf uu}\ts\right)\ts \\
			{\bf I}_{n} - 2 {\bf uu}\ts &= {\bf I}_{n}\ts - 2\left({\bf uu}\ts\right)\ts \\
			{\bf I}_{n} - 2 {\bf uu}\ts &= {\bf I}_{n}\ts - 2\left({\bf u}\ts\right)\ts{\bf u}\ts \\
			{\bf I}_{n} - 2 {\bf uu}\ts &= {\bf I}_{n}\ts - 2{\bf u}{\bf u}\ts \\
			{\bf I}_{n} - 2 {\bf uu}\ts &= {\bf I}_{n} - 2{\bf u}{\bf u}\ts \\
		\end{align*}
		The transpose of the identity matrix is itself.
		\item If the matrix {\bf H} is orthogonal, it will satisfy this property:
		\[
			{\bf H}{\bf H}\ts = {\bf I}_n
		\]
		We can rewrite this as we know that H is symmetric:
		\[
			{\bf H}{\bf H}\ts = {\bf H}{\bf H} = {\bf H}^2 = {\bf I}_n
		\]
		Plug in the definition of {\bf H}:
		\begin{align*}
			{\bf I}_n &= {\bf HH} \\
			{\bf I}_n &= \left({\bf I}_{n} - 2 {\bf uu}\ts\right)\left({\bf I}_{n} - 2 {\bf uu}\ts\right) \\
			{\bf I}_n &= {\bf I}_n{\bf I}_n + 2\left({\bf I}_n\left(-2{\bf uu}\ts\right)\right) + \left(-2{\bf uu}\ts\right)\left(-2{\bf uu}\ts\right) \\
			{\bf I}_n &= {\bf I}_n - 4{\bf uu}\ts + 4 \left({\bf uu}\ts\right)\left({\bf uu}\ts\right) \\
			{\bf I}_n &= {\bf I}_n - 4{\bf uu}\ts + 4 \left({\bf uu}\ts{\bf uu}\ts\right) \\
			{\bf I}_n &= {\bf I}_n - 4{\bf uu}\ts + 4 \left({\bf u}\cdot 1 \cdot {\bf u}\ts\right) \\
			{\bf I}_n &= {\bf I}_n - 4{\bf uu}\ts + 4 {\bf u}{\bf u}\ts \\
			{\bf I}_n &= {\bf I}_n
		\end{align*}
		Note: ${\bf u}\ts {\bf u} = 1$ is true as its the dot product of {\bf u} with itself, and {\bf u} is a unit vector, meaning its norm is 1. This dot product gives the value of the norm squared, which is also 1.
		\item {\bf H} is of rank $n$. I can prove this with some assertions:
		\begin{enumerate}[label=\arabic*.]
			\item {\bf H} is its own inverse, as ${\bf HH} = {\bf I}_n$.
			\item Therefore H has an inverse.
			\item Therefore H's determinant is nonzero.
			\item Therefore all rows of H are linearly independent.
			\item Therefore H has full rank.
			\item H is square $n \times n$, as it comes from subtraction from ${\bf I}_n$.
		\end{enumerate}
		\item Starting from the definitions of {\bf H} and {\bf v}, and using the fact that ${\bf u}\ts{\bf u} = 1$ and ${\bf u}\ts{\bf u}^{\bot} = 0$:
		\begin{align*}
			{\bf Hv} &= \left({\bf I}_{n} - 2 {\bf uu}\ts\right)\left(\alpha {\bf u} + \beta {\bf u}^{\bot}\right) \\
			{\bf Hv} &= {\bf I}_n\left(\alpha{\bf u} + \beta {\bf u}^{\bot}\right) - 2{\bf uu}\ts\left(\alpha{\bf u} + \beta {\bf u}^{\bot}\right) \\
			{\bf Hv} &= \alpha{\bf u} + \beta {\bf u}^{\bot} - 2{\bf uu}\ts\left(\alpha{\bf u} + \beta {\bf u}^{\bot}\right) \\
			{\bf Hv} &= \alpha{\bf u} + \beta {\bf u}^{\bot} - 2{\bf uu}\ts\left(\alpha{\bf u}\right)  - 2{\bf uu}\ts\left(\beta {\bf u}^{\bot}\right) \\
			{\bf Hv} &= \alpha{\bf u} + \beta {\bf u}^{\bot} - 2\alpha{\bf uu}\ts{\bf u}  - 2\beta{\bf uu}\ts{\bf u}^{\bot} \\
			{\bf Hv} &= \alpha{\bf u} + \beta {\bf u}^{\bot} - 2\alpha{\bf u}\cdot1  - 2\beta{\bf u}\cdot0 \\
			{\bf Hv} &= \alpha{\bf u} + \beta {\bf u}^{\bot} - 2\alpha{\bf u} \\
			{\bf Hv} &= \beta {\bf u}^{\bot} - \alpha{\bf u}
		\end{align*}
	\end{enumerate}
\end{solution}

\pagebreak

% Problem: Orthogonal transformations
\begin{problem}[Preservation of norms]
	Let ${\bf Q}\in \R^{m\times n}$ be an orthonormal matrix, and let ${\bf x},{\bf y}$ be arbitrary vectors in $\R^n$. 
	\begin{enumerate}[label=\alph*)]
		\item Show that $\|{\bf Qx}\|_2 = \|{\bf x}\|_2$. Hint: use the property for the 2-norm that $\|{\bf x}\|_2 = \sqrt{{\bf x}\ts {\bf x}}$. 
		\item Show that $\|{\bf Qx} - {\bf Qy}\|_2 = \|{\bf x}-{\bf y}\|_2$. 
		\item Find examples showing that these equivalences do not necessarily hold in the 1-norm or $\infty$-norm. Hint: experiment using the matrix 
		\[{\bf Q}= \frac{1}{\sqrt{2}}\begin{bmatrix}
			1 & 1 \\ 1 & -1
		\end{bmatrix}.\]
		Provide a sketch to accompany your examples. 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item Using the given equation and the given property of the 2-norm:
		\begin{align*}
			\|{\bf Qx}\|_2 &= \|{\bf x}\|_2 \\
			\sqrt{\left({\bf Qx}\right)\ts\left({\bf Qx}\right)} &= \sqrt{{\bf x}\ts{\bf x}} \\
			\sqrt{\left({\bf Qx}\right)\ts\left({\bf Qx}\right)}^2 &= \sqrt{{\bf x}\ts{\bf x}}^2 \\
			\left({\bf Qx}\right)\ts\left({\bf Qx}\right) &= {\bf x}\ts{\bf x} \\
			\left({\bf x}\ts{\bf Q}\ts\right)\left({\bf Qx}\right) &= {\bf x}\ts{\bf x} \\
			{\bf x}\ts\left({\bf Q}\ts{\bf Q}\right){\bf x} &= {\bf x}\ts{\bf x} \\
			{\bf x}\ts{\bf I}_n{\bf x} &= {\bf x}\ts{\bf x} \\
			{\bf x}\ts{\bf x} &= {\bf x}\ts{\bf x}
		\end{align*}
		\item Using the same rules as in part (a):
		\begin{align*}
			\|{\bf Qx} - {\bf Qy}\|_2 &= \|{\bf x}-{\bf y}\|_2 \\
			\sqrt{({\bf Qx} - {\bf Qy})\ts({\bf Qx} - {\bf Qy})} &= \sqrt{({\bf x}-{\bf y})\ts({\bf x}-{\bf y})} \\
			\sqrt{({\bf Qx} - {\bf Qy})\ts({\bf Qx} - {\bf Qy})}^2 &= \sqrt{({\bf x}-{\bf y})\ts({\bf x}-{\bf y})}^2 \\
			({\bf Qx} - {\bf Qy})\ts({\bf Qx} - {\bf Qy}) &= ({\bf x}-{\bf y})\ts({\bf x}-{\bf y}) \\
			\left(({\bf Qx})\ts - ({\bf Qy})\ts\right)({\bf Qx} - {\bf Qy}) &= \left({\bf x}\ts - {\bf y}\ts\right)({\bf x} - {\bf y}) \\
			% linebreak
			\left({\bf x}\ts{\bf Q}\ts - {\bf y}\ts{\bf Q}\ts\right)({\bf Qx} - {\bf Qy}) &= {\bf x}\ts{\bf x} - {\bf x}\ts{\bf y} - {\bf x}{\bf y}\ts + {\bf y}\ts{\bf y} \\
			% linebreak
			\left({\bf x}\ts{\bf Q}\ts\right)({\bf Qx})
			+ \left({\bf x}\ts{\bf Q}\ts\right)(-{\bf Qy})\\
			+ \left(- {\bf y}\ts{\bf Q}\ts\right)({\bf Qx})
			+ \left(- {\bf y}\ts{\bf Q}\ts\right)(-{\bf Qy})
			&= {\bf x}\ts{\bf x} - {\bf x}\ts{\bf y} - {\bf x}{\bf y}\ts + {\bf y}\ts{\bf y} \\
			{\bf x}\ts{\bf Q}\ts{\bf Qx} - {\bf x}\ts{\bf Q}\ts{\bf Qy} - {\bf y}\ts{\bf Q}\ts{\bf Qx} + {\bf y}\ts{\bf Q}\ts{\bf Qy} &= {\bf x}\ts{\bf x} - {\bf x}\ts{\bf y} - {\bf x}{\bf y}\ts + {\bf y}\ts{\bf y} \\
			{\bf x}\ts{\bf I}_n{\bf x} - {\bf x}\ts{\bf I}_n{\bf y} - {\bf y}\ts{\bf I}_n{\bf x} + {\bf y}\ts{\bf I}_n{\bf y} &= {\bf x}\ts{\bf x} - {\bf x}\ts{\bf y} - {\bf x}{\bf y}\ts + {\bf y}\ts{\bf y}\\
			{\bf x}\ts{\bf x} - {\bf x}\ts{\bf y} - {\bf x}{\bf y}\ts + {\bf y}\ts{\bf y} &= {\bf x}\ts{\bf x} - {\bf x}\ts{\bf y} - {\bf x}{\bf y}\ts + {\bf y}\ts{\bf y}
		\end{align*}
		\item I can prove that these equivalences are not always true when working with norms other than the 2-norm. Let {\bf x} and {\bf y} be vectors defined as follows:
		\[
			{\bf x} = \begin{bmatrix}1 \\ 0\end{bmatrix}, {\bf y} = \begin{bmatrix}0 \\ -2\end{bmatrix}
		\]
		The following python code proves that with these vectors,
		\begin{align*}
			\|{\bf Qx}\|_1 &\neq \|{\bf x}\|_1 &\qquad \|{\bf Qx}\|_{\infty} &\neq \|{\bf x}\|_{\infty} \\
			\|{\bf Qx} - {\bf Qy}\|_1 &\neq \|{\bf x} - {\bf y}\|_1 &\qquad \|{\bf Qx} - {\bf Qy}\|_{\infty} &\neq \|{\bf x} - {\bf y}\|_{\infty}. 
		\end{align*}
		\begin{minted}{Python}
# %% imports
import numpy as np

# %% defs
Q = (1 / np.sqrt(2)) * np.matrix("1,1;1,-1")
x = np.matrix("1;0")
y = np.matrix("0;-2")
# %%
one_norm_q = np.linalg.norm(Q * x, 1)
one_norm = np.linalg.norm(x, 1)
inf_norm_q = np.linalg.norm(Q * x, np.inf)
inf_norm = np.linalg.norm(x, np.inf)
print("Testing equivalency 1:")
print(f"{one_norm_q} ?= {one_norm}: {one_norm_q == one_norm}")
print(f"{inf_norm_q} ?= {inf_norm}: {inf_norm_q == inf_norm}")


one_q = np.linalg.norm(Q * x - Q * y, 1)
one_no_q = np.linalg.norm(x - y, 1)
inf_q = np.linalg.norm(Q * x - Q * y, np.inf)
inf_no_q = np.linalg.norm(x - y, np.inf)

print("Testing equivalency 2:")
print(f"{one_q} ?= {one_no_q}: {one_q == one_no_q}")
print(f"{inf_q} ?= {inf_no_q}: {inf_q == inf_no_q}")
		\end{minted}
		Actual output:\\
		\texttt{
			Testing equivalency 1:\\
			1.414213562373095 ?= 1.0: False\\
			0.7071067811865475 ?= 1.0: False\\
			Testing equivalency 2:\\
			2.82842712474619 ?= 3.0: False\\
			2.1213203435596424 ?= 2.0: False}
	\end{enumerate}
	The code is in the file \texttt{p7\_example.py}
\end{solution}

\begin{problem}[Programming]
	For two matrices ${\bf A}\in \R^{m\times n}$ and ${\bf B}\in \R^{n\times p}$, consider the standard matrix multiplication algorithm which for $1\leq i\leq m$ and $1\leq j\leq p$ computes 
	\[({\bf A}{\bf B})_{ij} = \sum_{k=1}^n {\bf A}_{ik}{\bf B}_{kj}.\]
	\begin{enumerate}[label=\alph*)]
		\item Using the standard algorithm, about how many additions and multiplications are needed to compute the matrix product ${\bf A}{\bf B}$? 
		\item For some number $N> 100$, use Matlab or Python with \texttt{randn} to generate random matrices ${\bf A}_1\in \R^{N\times 100}$, ${\bf A}_2\in \R^{100\times N}$, and ${\bf A}_3\in\R^{N\times 100}$. Use \texttt{tic} and \texttt{toc} (or e.g., \texttt{timeit.default\_timer()}) to estimate the amount of time it takes to compute the products 
		\[{\bf A}_1({\bf A}_2{\bf A}_3)\quad \text{and}\quad ({\bf A}_1{\bf A}_2){\bf A}_3.\]
		Which one is faster for large $N$? 
		\item Explain your findings, using your answer from part (a). 
	\end{enumerate}
\end{problem}

\begin{solution}
	The code for this part is in the file \texttt{p8\_example.py}
	\begin{enumerate}[label=\alph*)]
		\item Each entry in {\bf AB} takes $n$ multiplications and $n - 1$ additions to compute. There are $m \times p$ entries in {\bf AB}, bringing the total up to $m \cdot n \cdot p$ multiplications and $m \cdot p \cdot (n - 1)$ additions.
		\item I'll use $N = 500$ for these equations. I found that ${\bf A}_1({\bf A}_2{\bf A}_3)$ is much faster for large $N$. The speedup gets larger and larger as $N$ increases. If $N$ is really really big, $({\bf A}_1{\bf A}_2){\bf A}_3$ isn't even possible to calculate due to RAM limitations.
		\item This is because, with ${\bf A}_1\in \R^{N\times 100}$, ${\bf A}_2\in \R^{100\times N}$, and ${\bf A}_3\in\R^{N\times 100}$, the operation ${\bf A}_2{\bf A}_3$ has $100 \cdot N \cdot 100$ multiplications, producing a $100 \times 100$ matrix. This matrix's multiplication with ${\bf A}_1$ has $N \cdot 100 \cdot 100$ multiplications, bringing the total number up to $2 \cdot 100 \cdot 100 \cdot N$ multiplications, and a comparably sized number of additions.\\
		The mathematically-equivalent operation $({\bf A}_1{\bf A}_2){\bf A}_3$ requires $N \cdot 100 \cdot N$ multiplications for the first step (and memory to store the $N\times N$ size result!), and then a further $N \cdot N \cdot 100$ multiplications for the second step, for a total of $2 \cdot 100 \cdot N \cdot N$ multiplications. This scales according to $N^2$, while the first operation scales according to $N$.
	\end{enumerate}
\end{solution}

	
\end{document}




