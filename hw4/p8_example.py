# %% imports
import numpy as np
import time

# %% defs
N = 20000
A1 = np.random.randn(N, 100)
A2 = np.random.randn(100, N)
A3 = np.random.randn(N, 100)
# %% time the multiplications
start = time.time()
b = A1 @ (A2 @ A3)
elapsed1 = time.time() - start

start = time.time()
c = (A1 @ A2) @ A3
elapsed2 = time.time() - start

print(f"Time for A1(A2 * A3): {elapsed1}")
print(f"Time for (A1 * A2)A3: {elapsed2}")
# %%
