# %% imports
import numpy as np

# %% defs
Q = (1 / np.sqrt(2)) * np.matrix("1,1;1,-1")
x = np.matrix("1;0")
y = np.matrix("0;-2")
# %%
one_norm_q = np.linalg.norm(Q * x, 1)
one_norm = np.linalg.norm(x, 1)
inf_norm_q = np.linalg.norm(Q * x, np.inf)
inf_norm = np.linalg.norm(x, np.inf)
print("Testing equivalency 1:")
print(f"{one_norm_q} ?= {one_norm}: {one_norm_q == one_norm}")
print(f"{inf_norm_q} ?= {inf_norm}: {inf_norm_q == inf_norm}")


one_q = np.linalg.norm(Q * x - Q * y, 1)
one_no_q = np.linalg.norm(x - y, 1)
inf_q = np.linalg.norm(Q * x - Q * y, np.inf)
inf_no_q = np.linalg.norm(x - y, np.inf)

print("Testing equivalency 2:")
print(f"{one_q} ?= {one_no_q}: {one_q == one_no_q}")
print(f"{inf_q} ?= {inf_no_q}: {inf_q == inf_no_q}")

# %%
