\documentclass{article}[10pt]
\usepackage{enumitem}
\usepackage{amsmath,amsfonts}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{xcolor}
\setlength{\parindent}{0pt}
\definecolor{light-gray}{gray}{0.95}
\newcommand{\code}[1]{\colorbox{light-gray}{\texttt{#1}}}

\begin{document}
	\title{Project \#3}
	\author{Trevor Leibert}	
	\date{\today}

	\maketitle
	
	\pagebreak
	\section*{Part 1 Writeup}
	\subsection*{Introduction}
	This project shows the utility of linear algebra in image processing, especially as it relates to image blurring, restoration, and compression.
	We will be working with this image:
	\begin{figure}[ht]
		\centering
		\includegraphics[width=8cm]{satellite.png}
		\caption{A 64x64 image of a satellite.}
	\end{figure}\\
	This section will illustrate the limitations of naive matrix operations, both in terms of time-complexity and in terms of correctness when attempting to reconstruct the source image from one that has been blurred and effected by random noise.
	
	\subsection*{Python Code}
	After a bit of difficulty I realized that Numpy's singular value decomposition function returns the \emph{transpose} of {\bf v}, and that the singular values it returns are in a flat vector and must be placed on the diagonal of a zero matrix as follows:
	\begin{minted}{Python}
# %% compute singular values
# note that s is a flat vector and "v" is actually v transpose
u, s, vt = np.linalg.svd(A)
sm = np.diag(s)
v = vt.T
	\end{minted}
	This process is a bit more complicated when working with a non-square matrix, however our blurring operator {\bf A} is square, so this simple solution is enough. The full code for this part can be found in the file \texttt{deblurring\_image.py}

	\pagebreak
	\subsection*{Plots, Figures and Analysis}
	There are three initial variants of the image: the original, a blurred version, and a blurred version with random noise:
	\begin{figure}[ht]
		\centering
		\includegraphics[width=12cm]{original_3_pictures.png}
		\caption{The three original images}
	\end{figure}\\
	We can attempt to reconstruct the original image from its blurred, noisy version by solving the system \[
		{\bf\tilde b} = {\bf A x}
	\]
	for {\bf x}, where ${\bf\tilde b}$ is the blurred, noisy image as a vector, {\bf A} is the blurring operator matrix, and {\bf x} is the desired ``original'' image, also as a vector. The results of solving this system can be seen below:
	\begin{figure}[ht]
		\centering
		\includegraphics[width=8.5cm]{deblur.png}
		\caption{The original image compared to its naive reconstruction.}
	\end{figure}\\
	As we can see the satellite is barely discernable in the naive reconstruction. The small noise errors in ${\bf \tilde b}$ compound massively when run through the inverse of the blurring operator {\bf A}. This is due to the high condition number of {\bf A}--about 5039.94. The condition number of a matrix is similar in its use to the condition number of a function as obtained through calculus, and is obtained by dividing the first, largest, singular value by the last, smallest, singular value. The relative error in the input is given by the 2-norm of the differences between ${\bf \tilde b}$ and {\bf b}, 0.5. When multiplied by the condition number of the matrix (which is also the condition number of its inverse), we get a good idea of the relative error in the output:
	\begin{align*}
		\texttt{relative\_output\_error} &= \kappa_{rel} \cdot \texttt{relative\_input\_error} \\
		\texttt{relative\_output\_error} &= 5039.94 \cdot 0.5 \\
		\texttt{relative\_output\_error} &= 2519.97
	\end{align*}
	As we can see, the relative error in the output is guaranteed to be pretty large when the relative error in the input is not \emph{very} small.
	Therefore, simply trying to solve the system
	\[
		{\bf\tilde b} = {\bf A x}
	\]
	is a no-go if we're trying to get a discernable result back. However, there is still hope yet of recovering (an approximation of) our original satellite! For this, we turn to the singular value decomposition yet again.\\
	It is possible to \emph{approximate} a matrix to rank $k$ by truncating its singular value decomposition to the first $k$ columns of {\bf U}, the $k\times k$ upper-left submatrix of $\Sigma$, and first $k$ rows of ${\bf V}^T$.
	\[
		{\bf A}_k = {\bf U}_k{\bf \Sigma}_k{\bf V}_k^{\text T}
	\]
	This truncation can be done to save space/processing time as the resulting matrix will be of a lower rank, or, in our case, for error reduction. By reducing the matrix {\bf A} to rank $k$, we only keep the singular values 1 through $k$. This will reduce the condition number of the matrix produced by the truncated SVD, as $\sigma_1 \geq \sigma_2 \geq \sigma_3 \geq \dots \geq \sigma_n$. We can see that this holds true for our matrix {\bf A} as well:
	\begin{figure}[ht]
		\centering
		\includegraphics[width=10cm]{sing-vals.png}
		\caption{{\bf A}'s singular values plotted on a log scale}
	\end{figure}\\
	We can see here that the singular values of A decline in a somewhat steady exponential manner up until the last 400 or so, where their values suddenly start to drop off. This sudden drop in values is the source of {\bf A}'s high condition number.
	Recalling the definition of a matrix condition number:
	\[
		\kappa_{rel} = \frac{\sigma_1}{\sigma_n}.	
	\]
	By reducing $n$ by truncating to column rank $k$, we increase the value of $\sigma_n$, lowering the value of $\kappa_{rel}$. However, by lowering the rank of ${\bf A}_k$, we lose information that A contained, meaning that our approximate deblurring, while more well-conditioned, begins to lose its accuracy as $k$ goes to zero. The system that represents one of these reconstructions is as follows:
	\[
		{\bf x} = {\bf V}_k {\bf \Sigma}_k^{-1}{\bf U}_k^{\text T}{\bf \tilde b}
	\]
	\begin{figure}[ht]
		\centering
		\includegraphics[width=12cm]{reconstructions.png}
		\caption{Reconstructions of the original image}
	\end{figure}\\
	We can see that the rank-1600 approximation is pretty clear and noise-free, while the rank-2000 and rank-2400 approximations are even more crisp but with increasing amounts of noise. These images represent good trade-offs in the loss of accuracy in the de-blurring and gains in minimizing error magnification. Looking at the relative errors in the varying-rank reconstructions versus the original image:
	\pagebreak
	\begin{figure}[ht]
		\centering
		\includegraphics[width=10cm]{relative_errors.png}
		\caption{Relative errors in the varying-rank reconstructions under the 2-norm}
	\end{figure}\\
	Mathematically speaking, the rank-1600 approximation is the ``best'' of the ones that were generated as it has the lowest relative error from the original image. The error seems to follow an order-2 polynomial with a local and absolute minimum at $k=1600$ However, its behavior as x goes to 1 or 4000 must reach some finite limit, so the order-2 polynomial approximation of the relative error would only really be valid close to $k=1600$.
	\subsection*{Conclusions}
	Getting to do guided, hands-on work with the SVD and its truncations as a means of error/noise reduction was really useful. Combined with how a truncated SVD can be more quickly and easily calculated than a full one, this process of approximation has potential for applications in many poorly-conditioned systems. The visual nature of this report's investigation was very effective hammering home the point for me. I am itching to do more linalg as it relates to computer graphics or image processing.
	\pagebreak
	\section*{Part 2 Writeup}
	\subsection*{Introduction}
	This part of the project focuses on using least-squares to fit models to data. In this example, we will be fitting a model to jokes that have been scored 0-10, and using this model to predict the scores of certain jokes based on the performance of earlier ones. This experiment will show the power of least-squares techniques for modeling and prediction. \\
	{\bf NOTE: I am leaving out most of the long numeric answers to the ``Find x'' questions from the formal writeup, as those are long and can be found in the code, which is blocked out by question letter. I am however analyzing them and their context.}
	\subsection*{Python Code}
	My full code for this part of the project can be found in the file \texttt{joke\_analysis.py}. I didn't have much trouble doing the code for this project. One thing to note is that Numpy's least squares solver gets angry if the {\bf A} matrix you give it is a one-dimensional array of $n$ elements. One must expand this vector to be a $n \times 1$ array with the \texttt{np.expanddims} function:
	\begin{minted}{Python}
alpha, resid_Sq, _, _ = np.linalg.lstsq(np.expand_dims(x20, 1), x22)
	\end{minted}
	Another thing to note is that the least-squares solver will return 4 values--the first is the solution to the least-squares problem, and the second is the square of the 2-norm of the residuals to the least-squares solution. This did most of the work for part e) for me!
	\subsection*{Plots, Figures, and Analysis}
	With such a variety of jokes, some corny, some mathematical, it would seem likely that there are relationships between the scores that each individual that rated them gave.
	One person may like corny jokes, and rate similar jokes more highly. Maybe one would get sick of them the more that they read, and issue lower and lower scores progressively. The possibilities are endless.
	We'll start by looking at the relationships between the scores given to Joke 20 and Joke 22.\\\\
	Joke 20: `A mathematician, physicist, and engineer are asked to measure the volume of a red rubber ball. The mathematician measures the circumference with a string, then computes the volume. The physicist puts the ball in a beaker of water and measures the displacement. The engineer weighs the ball, then looks up the conversion formula in a Red Rubber Ball table.'\\\\
	Joke 22: `Milk production at a dairy farm was low, so the farmer wrote to the local university asking for help. A physicist comes to the farm for an intensive on-site investigation, and returns to the university to evaluate the data. The physicist soon returns to the farm, saying to the farmer "I have the solution, but it works only in the case of spherical cows in a vacuum".' \\\\
	Both of these jokes mention physicists, so similarly-minded individuals might give this joke a higher score (or maybe lower). The first step to getting ready to interpret this data is to normalize each joke's scores around that joke's mean score. This will make fitting linear models simpler as the y-intercept of every line of best fit will be zero, among other things. To do this, we take the mean of each column of the Joke score matrix {\bf J}:
	\begin{minted}{Python}
means = np.mean(J, axis=0)
	\end{minted}
	To normalize each column around zero, we simply subtract the mean of each column from each element in that column, taking advantage of Numpy's dimension-broadcasting:
	\begin{minted}{Python}
J_mod = J - means
	\end{minted}
	Each element in \texttt{J\_mod} is now the deviation of that person's score from the mean of that joke's score. In addition, the mean of each column is now zero.
	To fit a model to this problem we must solve the problem
	\[
		{\bf x}_{22} \approx \alpha {\bf x}_{20}
	\] for $\alpha$. This alpha value will be the slope of our line-of-best-fit.
	After solving with \texttt{numpy.linalg.lstsq}:
	\[
		\alpha = 0.720615
	\]
	This number indicates how many points from the mean the rater's score should be for Joke 22 for each point from the mean the rater's score was from Joke 20. The relationship becomes more apparent when the scores for Jokes 20 and 22, along with the line of best fit given by $\alpha$, are plotted together:
	\begin{figure}[ht]
		\centering
		\includegraphics[width=10cm]{joke_20_22.png}
		\caption{Linear model fit to model scores for joke 22 based on scores for joke 20}
	\end{figure}\\
	Success! The line-of-best-fit seems to fit the data reasonably well. It also seems like the theory that the jokes' scores would be related holds some water. We'll take a look at one person's scores in particular. Person 13 scored Joke 20 at -2.67 off of the mean, and Joke 22 at -3.29 off the mean. Our model predicts that they'll score it as follows when multiplying by $\alpha$:
	\[
		-1.92 = 0.721 \cdot -2.67
	\]
	This is kinda close to the true value, with a relative error of \emph{only} 27.94\%.
	To get a more concrete idea of how good of a model this is, we turn to the 2-norm of the residuals. The residuals are simply the differences between the exact vector and our approximated version. In our case:
	\[
		\|{\bf x}_{22}-\alpha {\bf x}_{20}\|_2 = 13.51
	\]
	To get an idea of how to interpret the norm of the residuals, we'll look at the 2-norm of the actual values of ${\bf x}_{22}$:
	\[
		\|{\bf x}_{22}\|_2 = 19.34
	\]
	As we can see, the 2-norm of the residuals is almost 70\% of the 2-norm of the actual values of ${\bf x}_{22}$. Not good. \\
	We'll give this another shot with a model that uses more jokes for its inputs. The goal is to model the responses of the first seven students to the last two jokes, which were posted after those students had already submitted their scores. Their responses were filled in with the means of their respective columns as a placeholder. Ten jokes were selected to use as predictor variables: {\bf p} = [6,15,18,23,25,26,27,31,32,36]. A linear model was fitted to the 8th through last rows of the Joke score matrix, as we don't want to include the placeholder scores in the model.
	The model returned a $2 \times 10$ matrix, with the top row representing the share each joke in the set $p$ has in creating the final score of joke 41, and the bottom row representing the same for joke 42. I used the following code to apply the model to the normalized score matrix:
	\begin{minted}{Python}
ratings = J_mod[:7, p] @ c.T
	\end{minted}
	The scores came out as follows:
	\[\begin{bmatrix}
	-0.9712791 & 1.53518505 \\
	0.38000041 & -0.94346206 \\
	2.27904465 & 2.4774686 \\
	-1.35354893 & -1.38317536 \\
	-1.35302349 & -0.79637186 \\
	-0.27223831 & 0.25518385 \\
	1.04641833 & -0.23436229
	\end{bmatrix}\]
	\subsection*{Conclusions}
	Least-squares fits provide a simple and powerful way to establish a mathematical relationship between variables, and allow for relationships to be established in systems that are over or under-determined, something that one cannot easily do by hand with algebra. This project also shows that while powerful, the solutions produced by least squares aren't necessarily very good, they are just the best possible fits for the given order function.
	
\end{document}