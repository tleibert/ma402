# %% imports
import scipy, scipy.io
import numpy as np
import matplotlib.pyplot as plt


# %% load stuff
deblur = scipy.io.loadmat("deblur.mat")

# get all matrices as numpy arrays
A = np.array(deblur["A"])
b = np.array(deblur["b"])
bn = np.array(deblur["bn"])
xtrue = np.array(deblur["xtrue"])
# shape of image (image is square but flattened into a 1 col list)
shape = (int(np.sqrt(len(b))),)*2


# %% show images

# plot images
fig, axs = plt.subplots(1, 3, figsize=(8,3))
datas = [xtrue, b, bn]
labels= ["Original Image", "Blurred Image", "Blurred + Noisy Image"]
for i in range(3):
    axs[i].imshow(np.reshape(datas[i], shape).T, cmap="gray")
    axs[i].set_xticks([])
    axs[i].set_yticks([])
    axs[i].set_xlabel(labels[i])

save = False

if save:
    plt.savefig("original_3_pictures.png", dpi=300)

# %% solve for original image
x_recovered = np.linalg.solve(A, bn)
fig, axs = plt.subplots(1, 2)
datas = [xtrue, x_recovered]
labels = ["Original Image", "Naive De-blurred Image"]
for i in range(2):
    axs[i].imshow(np.reshape(datas[i], shape).T, cmap="gray")
    axs[i].set_xticks([])
    axs[i].set_yticks([])
    axs[i].set_xlabel(labels[i])

save = False

if save:
    plt.savefig("devblur.png", dpi=300)
    
# %% compute singular values
# note that s is a flat vector and "v" is actually v transpose
u, s, vt = np.linalg.svd(A)
sm = np.diag(s)
v = vt.T

# %% plot computed values
fig, ax = plt.subplots()
ax.semilogy(s)
ax.set_xlabel("Singular value no.", size=12)
ax.set_ylabel("Value of $S_n$", size=12)
ax.set_title("Singular values of ${\\bf A}$", size=18)

save = True

if save:
    plt.savefig("sing-vals.png", dpi=300)

# %% compute condition number
sig_1 = s[0]
sig_2 = s[-1]
kappa = sig_1 / sig_2
print(f"Condition number of A: {kappa}")

# %% part e approximate inverse svd for rank-k approximations
# k from 400 to 3600 in intervals of 400

# semi-expensive inverse operation
# sm_inv = np.linalg.inv(sm)
approximations = [v[:, :i] @ np.linalg.solve(sm[:i, :i], u[:, :i].T @ bn) for i in range(400, 3601, 400)]


# %% plot images for part e
labels = [f"rank-{i}" for i in range(400, 3601, 400)]
fig, axs = plt.subplots(3, 3)
counter = 0
for i in range(3):
    for j in range(3):
        axs[i, j].imshow(np.reshape(approximations[3 * i + j], shape).T, cmap="gray")
        axs[i, j].set_xticks([])
        axs[i, j].set_yticks([])
        axs[i, j].set_xlabel(labels[3 * i + j], size=8)

save = False

if save:
    plt.savefig("reconstructions.png", dpi=300)

# %% part f plot relative error in reconstructed solutions
rel_errors = [np.linalg.norm(approx - xtrue, 2) / np.linalg.norm(xtrue, 2) for approx in approximations]
print(rel_errors)
fig, ax = plt.subplots()
ax.plot(range(400, 3601, 400), rel_errors)
ax.set_xlabel("Rank of approximation, $k$")
ax.set_ylabel("Relative error in approximation")
ax.set_title("Relative error in reconstructed image using approximated solutions")
ax.set_xticks(range(400, 3601, 400))

save = False

if save:
    plt.savefig("relative_errors.png", dpi=300)
# %%
