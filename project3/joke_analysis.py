# %% imports
import scipy, scipy.io
import numpy as np
import matplotlib.pyplot as plt

# %% part a - load data
jokeData = scipy.io.loadmat("jokeData.mat")
J = jokeData["J"]
jokeTexts = jokeData["jokeTexts"]
jokes = jokeData["jokes"]

# %% part b - find means and normalize matrix
means = np.mean(J, axis=0)
# the means of each column represent the average scores of each joke
print(means)
# every value is now a deviation from the mean score of that column's joke
# (and the mean of each column is now zero)
J_mod = J - means

# %% part c - solve x22 = alpha * x20
x20 = J_mod[:, 19]
x22 = J_mod[:, 21]
alpha, resid_Sq, _, _ = np.linalg.lstsq(np.expand_dims(x20, 1), x22)
print(f"Alpha: {alpha[0]}")
plt.scatter(x20, x22)
xvals = np.linspace(-7, 5)
plt.plot(xvals, alpha * xvals, label=f"Best-fit line $x_{{22}} = {alpha[0]:.3} \cdot x_{{20}}$")
plt.xlabel("Deviation from Joke 20's mean score")
plt.ylabel("Deviation from Joke 22's mean score")
plt.legend()
plt.title("Predicting score of joke 22 with score of joke 20", fontsize=16)

save = False
if save:
    plt.savefig("joke_20_22.png", dpi=300)

# %% part d - apply model to one case
print(f"Person 13's rating of joke 20: {J_mod[12, 19]}, joke 22: {J_mod[12, 21]}")
expected = J_mod[12, 19] * alpha[0]
rel_error = np.abs((expected - J_mod[12, 19]) / J_mod[12, 19])
print(f"Model's expected rating: {expected}, Relative error: {rel_error}")

#part e - numpy already gave us the residual
resid_2norm = np.sqrt(resid_Sq)
print(f"2-norm of residuals: {resid_2norm}, 2-norm of x22: {np.linalg.norm(x22, 2)}")

# %%  part f - create a model
p = np.array([6, 15, 18, 23, 25, 26, 27, 31, 32, 36])
# account for 0-based indexing
p -= 1
c, _, _, _ = np.linalg.lstsq(J_mod[7:, 40:], J_mod[7:, p])
print("Values in c:")
print(c)

# %% part g - apply the model
ratings = J_mod[:7, p] @ c.T
print("Approximated ratings:")
print(ratings)

# %%
