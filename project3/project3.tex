\documentclass{article}[10pt]
\usepackage{enumitem}
\usepackage{amsmath,amsfonts}
\usepackage{graphicx}
\setlength{\parindent}{0pt}

\begin{document}
	\title{Project \#3}
	\author{MA 402 \\ Mathematics of Scientific Computing}	
	\date{Due: Wed, April 7}

	\maketitle
	
	\subsection*{Writeup (4 pts)}
	
	In general, your writeup for each part should include the following: 
	\begin{itemize}
		\item A {\it short} introduction explaning the nature and purpose of the assignment. 
		\item A brief description of your code or algorithms, if necessary. 
		\item A section for your plots/tables/figures, appropriately labeled and with further description if necessary. 
		\item A paragraph for your conclusions. What valuable life lessons did you draw from the assignment?
	\end{itemize}
	The aim is that a student who knows a little about the relevant material, but has not seen or worked on this assignment specifically, should be able to read your writeup and get a general idea of what you did and why. \\
	
	Files submitted should include the following: 
	\begin{itemize}
		\item Your report (PDF/docx), which includes all relevant plots/tables/figures.
		\item Any code (.m/.py/.ipynb) used for your experiments. 
	\end{itemize}
	It will not affect your grade one way or another, but it is slightly preferred that you do {\it not} compress your files in a .zip format (this will make the grader's work easier). 
	
	\begin{figure}
		\centering
		\includegraphics[width=8cm]{satellite}
		\caption{Picture of the original image}\label{fig:satellite}
	\end{figure}
	
	\pagebreak
	
	\subsection*{Part 1 (9 pts)}
	In this part of the assignment, you will get experience with deblurring a blurred image that has been corrupted by noise. 
	\begin{enumerate}[label=\alph*)]
		\item Load the file ``deblur.mat'' using \texttt{load} in Matlab or \texttt{scipy.io.loadmat} in Python.. You will find the variables \texttt{A} (blurring operator, size $4096\times 4096$), \texttt{b} (blurred image, size $4096\times 1$), \texttt{bn} (blurred and noisy image, size $4096\times 1$), and \texttt{xtrue} (true image, size $4096\times 1$). For reference, Figure \ref{fig:satellite} shows what the true image is supposed to look like. 
		\item (1 pt) Plot the three images (original, blurred, blurred+noisy) in {\bf a single figure with 3 subplots}. Note that you will have to \texttt{reshape} the vectors into $64\times 64$ images. 
		\item (1 pt) Given the problem setup 
		\begin{align*}
			{\bf b} &= {\bf Ax}, \\
			{\bf \tilde{b}} &= {\bf Ax} + {\bf \eta}, 
		\end{align*}
		we have the naive solution ${\bf x}_{\text{naive}} = {\bf A}^{-1} \tilde{{\bf b}}$. In {\bf a figure with 2 subplots}, plot this solution as an image next to \texttt{xtrue}. Matlab users should use the backslash operator \textbackslash ~to solve the system, and Python users should use \texttt{numpy.linalg.solve}. Do not compute the inverse directly!\footnote{You can speed up this step by converting ${\bf A}$ to a sparse matrix first, using \texttt{sparse(A)}.}
		\item (2 pts) Plot the singular values\footnote{Note that this step may take your computer a while. In order to save time, consider computing the SVD just once, then \texttt{save} it as a .mat file and \texttt{load} it again the next time you need it.} of ${\bf A}$ on a \texttt{semilogy} plot and compute the condition number $\kappa({\bf A}) = \sigma_1/\sigma_n$. Given that $\|\tilde{{\bf b}}-{\bf b}\|_2/\|{\bf b}\|_2 = 0.05$, explain why you would expect the naive solution to perform poorly. 
		\item (2 pts) Using the SVD ${\bf A} = {\bf U\Sigma V}^T$, implement the truncated SVD formula 
			\[{\bf x}_k = {\bf V}_k {\bf \Sigma}_k^{-1}{\bf U}_k^T \tilde{{\bf b}}\]
			for $k = 400,800,\ldots,3600$. In a single figure with 9 subplots, plot the reconstructed vectors ${\bf x}_k$ as images. 
			\item (2 pts) Plot the relative error in the reconstructed solution as a function of the truncation rank $k$. For (approximately) what value of $k$ is the minimum attained? 
			\item (1 pt) In your own words, explain the behavior of the error as a function of $k$. 
	\end{enumerate}
	
	\pagebreak
	
	\subsection*{Part 2 (7 pts)}
	It's time to analyze some hilarious jokes! With Matlab, solving a least-squares problem is as simple as \texttt{A\textbackslash B}. In Python, \texttt{numpy.linalg.lstsq} is your friend. 
	\begin{enumerate}[label=\alph*)]
		\item Load the file ``jokeData.mat''. The file contains a matrix ${\bf J} \in \mathbb{R}^{49\times 42}$ representing 49 students who have rated 42 jokes. The other variables contain the text of the jokes and nicknames for the jokes. 
		\item (0.5 pt) Find the mean of each column of ${\bf J}$. What do these numbers represent? Modify ${\bf J}$ by subtracting the column mean from each entry in the corresponding column\textemdash you will work with this modified matrix for the remainder of the project. What do the numbers in this new matrix represent?
		\item (2 pts) Use a least-squares solver to find a linear fit of the form\footnote{We could use a linear model of the form ${\bf x}_{22}\approx \alpha{\bf x}_{20} + \beta$, but since both variables have been normalized to have zero mean, we would just end up getting $\beta = 0$. } ${\bf x}_{22}\approx \alpha {\bf x}_{20}$, where ${\bf x}_{20}$ is a vector containing the ratings for Joke 20 and similarly for ${\bf x}_{22}$. What is the value of $\alpha$, and what does it mean? Make a {\bf scatterplot} of the ratings {\bf with a line }for your linear fit.
		\item (1 pt) Take a look at how Person 13 rated Jokes 20 and 22. Based on their rating for Joke 20, how does their rating for Joke 22 compare with what your linear model would predict? 
		\item (0.5 pt) What is the norm of the residual vector $\|{\bf x}_{22}-\alpha {\bf x}_{20}\|_2$, and how does it compare to $\|{\bf x}_{22}\|_2$? What do these numbers tell you? 
		\item (2 pts) As it turns out, the last two jokes were added to the data set after the first seven students had already filled out the form. The current data set fills in those values using the column means of the corresponding jokes, but we can try to do better than that. Using the ten jokes ${\bf p} = [6,15,18,23,25,26,27,31,32,36]$ as predictor variables, fit a linear model of the form 
		\[{\bf J}(8:49,41:42) \approx {\bf J}(8:49,{\bf p}){\bf c}, \quad {\bf c}\in \mathbb{R}^{10\times 2}.\]
		What are the fitted coefficients ${\bf c}$, and what do they tell you? 
		\item (1 pt) Using the linear model from the previous part, predict what ratings the first seven students might have given Jokes 41 and 42 had they gotten the chance to read them. 
		\item (Bonus) Take another look at predicting the ratings for Joke 22, but this time split the users into a {\it training} group and a {\it testing} group. How well does the model perform for each group? How do these numbers change as you increase the number of jokes used as predictor variables? 
	\end{enumerate}
	

	
\end{document}