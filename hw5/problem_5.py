# %% imports
import numpy as np

# %% definitions
A = np.array([[1, 1 + 1e-8], [1 + 1e-8, 1]])
b = np.array([[1], [1]])
btilde = np.array([[1 + 1e-12], [1]])

# %% a - find singular values
u, s, vt = np.linalg.svd(A)
print("Singular values of A: ", s)

# %% b - conditon number of A
con = np.linalg.cond(A)
calcond = s[0] / s[1]
print("cond(A): ", con)
print("Condition number of A by s_0 / s_k:", calcond)

# %% c - relative error of xtilde and btilde
x = np.linalg.solve(A, b)
xtilde = np.linalg.solve(A, btilde)
rel_err_x = np.linalg.norm(xtilde - x) / np.linalg.norm(x)
rel_err_b = np.linalg.norm(btilde - b) / np.linalg.norm(b)

print("Relative error of xtilde: ", rel_err_x)
print("Relative error of btilde: ", rel_err_b)
print("Ratio of x's relative error to b's relative error: ", rel_err_x / rel_err_b)

# %%
