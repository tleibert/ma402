# %% imports
import numpy as np
import matplotlib.pyplot as plt

# %% definitions
xs = np.array([1, 3, 4, 6, 6, 7])

# %% part A , minimize sum((x - x_i)^2)
xvals = np.linspace(0, 10, 5000)
ys = [np.sum((val - xs) ** 2) for val in xvals]
plt.plot(xvals, ys)
plt.title("sum((x - x_i)^2)")
# looks like mean
plt.savefig("parta.png", dpi=300)
# %% part b minimize sum(|x - x_i|)
ys = [np.sum(np.abs(val - xs)) for val in xvals]
plt.plot(xvals, ys)
plt.title("sum(|x - x_i|)")
plt.savefig("partb.png", dpi=300)
# funky flat bottom plot
# %% part d minimize max(|x - x_i|)
ys = [np.max(np.abs(val - xs)) for val in xvals]
plt.plot(xvals, ys)
plt.title("max(|x - x_i|)")
plt.savefig("partd.png", dpi=300)
# looks like 4
# %% part e
xvals = np.linspace(0, 50, 20000)
xs = np.array([1, 3, 4, 6, 6, 7, 78])
fig, axs = plt.subplots(1, 3, figsize=(9, 4))
titles = ["sum((x - x_i)^2)", "sum(|x - x_i|)", "max(|x - x_i|)"]
yvals = [
    [np.sum((val - xs) ** 2) for val in xvals],
    [np.sum(np.abs(val - xs)) for val in xvals],
    [np.max(np.abs(val - xs)) for val in xvals],
]

for i in range(3):
    axs[i].plot(xvals, yvals[i])
    axs[i].set_title(titles[i])
fig.suptitle("All three plots, but with the new x value", fontsize=16)
index_of_min = yvals[2].index(np.min(yvals[2]))
minim = xvals[index_of_min]
print(np.mean(xs))
print(np.median(xs))
print(minim)
plt.savefig("parte.png", dpi=300)
# %%
