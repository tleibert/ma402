# %% imports
import numpy as np
import matplotlib.pyplot as plt
import scipy.io

# %% setup
hw5 = scipy.io.loadmat("hw5.mat")
xs = hw5["xs"]
yn = hw5["yn"]
A = np.concatenate((np.ones((len(xs), 1)), xs, xs ** 2, xs ** 3), axis=1)
# %% solve
soln, resid, _, _ = np.linalg.lstsq(A, yn)

print(soln)

# %% plot
f = lambda x: soln[0][0] + soln[1][0] * x + soln[2][0] * x ** 2 + soln[3][0] * x ** 3
xvals = np.linspace(0, 1, 1000)
plt.scatter(xs, yn, label="Datapoints")
plt.plot(xvals, f(xvals), label="Best-fit polynomial")
plt.legend()
plt.xlabel("X values")
plt.ylabel("Y values")
plt.title("Plot of data versus best-fit polynomial")
plt.savefig("prob_8.png", dpi=300)
# %%
