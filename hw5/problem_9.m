%% Initial data
A = [9 8 10;
    2 10 -6;
    5 3 7;
    10 1 19];
b = [9;
    10;
    7;
    6;];

%% part a
x1 = pinv(A) * b;
disp("part a with pseudoinverse")
disp(x1)

%% part b
x2 = A\b;
disp("part b with A\b")
disp(x2)

%% part c
fprintf("Norm of x1: %f, Norm of x2: %f\n", norm(x1, 2), norm(x2, 2))
disp("x1 has the smaller 2-norm")

%% part d
format long
resid_1 = norm(b - (A * x1), 2);
resid_2 = norm(b - (A * x2), 2);
disp("residual of x1")
disp(resid_1)
disp("residual of x2")
disp(resid_2)
disp("X1 is just a little tiny bit smaller")

%% part e
r1 = b - A * x1;
r2 = b - A * x2;
result = norm(r1 - r2, 2);
disp("2-norm of differences of residuals")
disp(result)

%% part f
x3 = x1 + null(A);
% null(A) is one column vector in this case, because A is rank 2, and it
% has 3 columns. We are guaranteed that x = pinv(A) * b + (any linear
% combination of the null space basis vectors) is a solution to the least
% squares problem. I'm going with just adding 1 * the basis vector to keep
% things simple.
disp("Third solution")
disp(x3)
