# %% imports
from timeit import default_timer
import numpy as np

# %% definitions
N = 5000
A = np.random.randn(N, N)
b = np.random.randn(N, 1)

# %% solve
tic = default_timer()
sol1 = np.linalg.solve(A, b)
elapsed = default_timer() - tic
print(f"np.linalg.solve took {elapsed} seconds!")
# %% invert
tic = default_timer()
sol2 = np.linalg.inv(A) @ b
elapsed = default_timer() - tic
print(f"invert-and-multiply took {elapsed} seconds!")
# %%
