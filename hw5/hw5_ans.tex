\documentclass{article}[10pt]

% PACKAGES AND DEFINITIONS
\usepackage{amsmath,amsthm,amssymb}
\usepackage{graphicx,enumitem,multicol}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{dsfont}
\usepackage{caption}

\theoremstyle{definition}
\newtheorem{problem}{Problem}
\specialcomment{solution}{{\bf Solution: }}{}
\setlength{\parindent}{0pt}

\newcommand{\x}{{\bf x}}
\newcommand{\y}{{\bf y}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\1}{\mathds{1}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\unif}{\mathcal{U}}
\newcommand{\inv}{^{-1}}
\newcommand{\ts}{^{T}}
\newcommand{\pinv}{^{\dagger}}

\DeclareMathOperator*{\var}{Var}
\DeclareMathOperator*{\prob}{Pr}
\DeclareMathOperator*{\Ber}{Ber}
%\excludecomment{solution}

%\excludecomment{solution}

\begin{document}
	
	
\title{Homework \#5}
\author{Trevor Leibert}	
\date{\today}

\maketitle

% Problem: SVD plus programming
\begin{problem}[SVD I]
	Consider the matrix
	\[{\bf A} = \begin{bmatrix}
		1 & 1 & 0 & 0\\
		0 & 1 & 1 & 0\\
		0 & 0 & 1 & 1\\
		1 & 0 & 0 & 1
	\end{bmatrix}.\]
	Using \texttt{svd} in Matlab or \texttt{numpy.linalg.svd} in Python, answer the following questions: 
	\begin{enumerate}[label=\alph*)]
		\item What is the rank of ${\bf A}$? 
		\item What are the 2-norm and Frobenius norm of ${\bf A}$? 
		\item What is the compact SVD of ${\bf A}$? (You should be able to express the answer exactly in terms of $\sqrt{2}$.)
		\item Using the full SVD, find a vector ${\bf z}$ such that ${\bf Az} = {\bf 0}$. 
		\item What is the best rank-1 approximation to ${\bf A}$? 
		\item Is the best rank-2 approximation to ${\bf A}$ unique? Why or why not? 
		\item Find the symmetric projection matrix $\Pi$ that projects onto the column space of ${\bf A}$.
	\end{enumerate}
\end{problem}

\begin{solution}
	My code for this problem is in the file \texttt{problem\_.py}
	\begin{enumerate}[label=\alph*)]
		\item Rank of A: 3
		\item 2-norm of A: 2\\Frobenius norm of A: 2.8284
		\item Compact SVD:
		\[
			{\bf U \Sigma V}^T = \begin{bmatrix}
				-0.5 & 0.5 & -0.5 \\
				-0.5 & -0.5 & -0.5 \\
				-0.5 & -0.5 & 0.5 \\
				-0.5 & 0.5 & 0.5
			\end{bmatrix}
			\begin{bmatrix}
				2 & 0 & 0 \\
				0 & \sqrt{2} & 0 \\
				0 & 0 & \sqrt{2}
			\end{bmatrix}
			\begin{bmatrix}
				-0.5 & -0.5 & -0.5 & -0.5 \\
				\frac{\sqrt{2}}{2} & 0 & \frac{-\sqrt{2}}{2} & 0 \\
				0 & \frac{-\sqrt{2}}{2} & 0 & \frac{\sqrt{2}}{2}
			\end{bmatrix}
		\]
		\item {\bf z} could be the zero vector\dots\\
		or we can pull it from the null space given by row 4 of ${\bf V}^T$ or column 4 of {\bf V}, which is the basis of {\bf A}'s null space.
		\[
			{\bf V}^T[3, :] = \begin{bmatrix}
				-0.5 & 0.5 & -0.5 & 0.5
			\end{bmatrix}
		\]
		We can use this basis to make  a nice clean whole number {\bf z}.
		\[
			{\bf z} = \begin{bmatrix}
				-1 \\ 1 \\ -1 \\ 1
			\end{bmatrix}
		\]
		\item Best rank 1 approximation of A, given by Rank-1 truncated SVD of A:
		\[
			\begin{bmatrix}
				-0.5 \\ -0.5 \\ -0.5 \\ -0.5
			\end{bmatrix}
			\begin{bmatrix}
				2
			\end{bmatrix}
			\begin{bmatrix}
				-0.5 & -0.5 & -0.5 & -0.5
			\end{bmatrix}
			=
			\begin{bmatrix}
				0.5 & 0.5 & 0.5 & 0.5 \\
				0.5 & 0.5 & 0.5 & 0.5 \\
				0.5 & 0.5 & 0.5 & 0.5 \\
				0.5 & 0.5 & 0.5 & 0.5
			\end{bmatrix}
		\]
		\item The best rank-2 approximation of a is NOT unique, as the second and third singular values of A are the same. Therefore one could make the best possible rank-2 approximation using singular values/associated vectors 1 and 2, OR 1 and 3.
		\item To find $\Pi$ we multiply the first three columns of U by the first 3 rows of its transpose:
		\[
			{\bf U}[:, :3] \cdot {\bf U}[:, :3]^T = \begin{bmatrix}
				0.75 & 0.25 & -0.25 & 0.25 \\
				0.25 & 0.75 &  0.25 & -0.25 \\
				-0.25 & 0.25 & 0.75 & 0.25 \\
				0.25 & -0.25 & 0.25 & 0.75
			\end{bmatrix}
		\]
	\end{enumerate}
\end{solution}

\pagebreak
% Problem: SVD II
\begin{problem}[SVD II]
	Let ${\bf A}$ have the SVD ${\bf U\Sigma V}\ts$. In terms of ${\bf U}$, ${\bf \Sigma}$, and ${\bf V}$, find the SVDs of ${\bf A}\ts$, ${\bf AA}\ts$, and ${\bf A}\ts {\bf A}$. Assuming ${\bf A}$ is invertible, find the SVD of ${\bf A}\inv$.
\end{problem}

\begin{solution}
	\begin{itemize}
		\item ${\bf A}^T$:
		\begin{align*}
			{\bf A}^T &= \left({\bf U\Sigma V}^T\right)^T \\
			{\bf A}^T &= {\bf V \Sigma U}^T
		\end{align*}
		\item ${\bf AA}^T$:
		\begin{align*}
			{\bf AA}^T &= \left({\bf U\Sigma V}^T\right)\left({\bf U\Sigma V}^T\right)^T \\
			{\bf AA}^T &= \left({\bf U\Sigma V}^T\right)\left({\bf V\Sigma U}^T\right) \\
			{\bf AA}^T &= \left({\bf U\Sigma}\right){\bf V}^T{\bf V}\left({\bf \Sigma U}^T\right) \\
			{\bf AA}^T &= \left({\bf U\Sigma}\right){\bf I}\left({\bf \Sigma U}^T\right) \\
			{\bf AA}^T &= {\bf U \Sigma}^2{\bf U}^T
		\end{align*}
		\item ${\bf A}^T{\bf A}$:
		\begin{align*}
			{\bf A}^T {\bf A} &= \left({\bf U\Sigma V}^T\right)^T \left({\bf U\Sigma V}^T\right) \\
			{\bf A}^T {\bf A} &= \left({\bf V\Sigma U}^T\right) \left({\bf U\Sigma V}^T\right) \\
			{\bf A}^T {\bf A} &= \left({\bf V\Sigma}\right){\bf U}^T{\bf U}\left({\bf \Sigma V}^T\right) \\
			{\bf A}^T {\bf A} &= \left({\bf V\Sigma}\right){\bf I}\left({\bf \Sigma V}^T\right) \\
			{\bf A}^T {\bf A} &= {\bf V \Sigma}^2{\bf V}^T
		\end{align*}
		\item ${\bf A}^{-1}$: ${\bf V \Sigma}^{-1}{\bf U}^T$ (from project 3)
		\begin{align*}
			{\bf A}^{-1} &= \left({\bf U \Sigma V}^T\right)^{-1} \\
			{\bf A}^{-1} &= {\bf V}^{T^{-1}}{\bf \Sigma}^{-1}{\bf U}^{-1} \\
			\text{ As we saw ear}&\text{lier, since }{\bf U}\text{ and }{\bf V}\text{ are orthonormal,}\\\text{ their inverses }&\text{are the same as their transposes.}\\
			{\bf A}^{-1} &= {\bf V}^{T^T}{\bf \Sigma}{\bf U}^T \\
			{\bf A}^{-1} &= {\bf V}{\bf \Sigma}{\bf U}^T
		\end{align*}
		Note: ${\bf U}^T = {\bf U}^{-1}$ specifically because A is invertible. (Also for {\bf V})
	\end{itemize}
\end{solution}


% Problem: Joke
\begin{problem}[Hilarious joke] The professor is not aware of any hilarious linear algebra jokes, so here's one that references some material from earlier in the semester:
 \begin{center}
 	\begin{quote}
 		Q: Why did the programmer dress up as Santa for Halloween? \\
		A: Because Oct(31) = Dec(25)!
 	\end{quote}
 \end{center}
 Please explain the joke.
\end{problem}

\begin{solution}
	The joke is funny because Octal 31 is the same as Decimal 25. We can check this ourselves:
	\begin{align*}
		31_8 &= 25 \\
		3 \cdot 8 + 1 &= 25 \\
		24 + 1 &= 25
	\end{align*}
\end{solution}


\begin{problem}[Speed test]
	Generate random matrices \texttt{A = randn(N)} and \texttt{b = randn(N,1)} for \texttt{N = 5000} or so (large enough that solving \texttt{Ax=b} takes a few seconds). Using \texttt{tic} and \texttt{toc} in Matlab or \texttt{timeit.default\_timer()} from the \texttt{timeit} module in Python, compare the methods
	\begin{itemize}
		\item \texttt{A\textbackslash b} (or \texttt{numpy.linalg.solve} in Python)
		\item \texttt{inv(A)*b}
	\end{itemize}
	How did the two methods compare? What was the relative error between the two answers? 
\end{problem}

\begin{solution}
	On my machine, with Python in a Linux VM, with $N = 5000$, \texttt{numpy.linalg.solve} took 1.553 seconds, and using an invert-then-multiply approach took 5.691 seconds. The relative error between the 2 solutions under the 2-norm was $1.5828 \cdot 10^{-13}$. Clearly it is much better to use \texttt{np.linalg.solve} than to directly invert a matrix and then multiply it. \\
	My work is in the file \texttt{problem\_4.py}
\end{solution}

\begin{problem}[Condition Number of a Matrix]
	Consider solving the linear system ${\bf Ax} = {\bf b}$, where 
	\[{\bf A} = \begin{bmatrix}
		1 & 1+10^{-8}\\
		1+10^{-8} & 1
	\end{bmatrix}, \quad {\bf b} = \begin{bmatrix}
		1\\1
	\end{bmatrix},\quad \tilde{{\bf b}} = \begin{bmatrix}
		1+10^{-12}\\1
	\end{bmatrix}\]
	Recall that the 2-norm condition number of ${\bf A}$ has the following property: if ${\bf Ax} = {\bf b}$ and ${\bf A\tilde{x}} = {\bf \tilde{b}}$, then 
	\[
		\frac{\|{\bf \tilde{x}}-{\bf x}\|_2}{\|{\bf x}\|_2} \leq \kappa \cdot \frac{\|{\bf \tilde{b}}-{\bf b}\|_2}{\|{\bf b}\|_2}. 
	\]
	
	Using \texttt{svd} in Matlab or \texttt{numpy.linalg.svd} in Python, answer the following questions: 
	\begin{enumerate}[label=\alph*)]
		\item What are the singular values of ${\bf A}$?
		\item What is $\kappa({\bf A})$ (the 2-norm condition number of ${\bf A}$)? The Matlab function \texttt{cond} will give you the answer directly, but you should also find it in terms of the singular values. 
		\item What was the relative error of $\tilde{{\bf x}}$? What is the relative error of $\tilde{{\bf b}}$? What is the ratio between the two quantites? Why is it so large? 
	\end{enumerate}
\end{problem}

\begin{solution}
	My code for this part is, predictably, in the file \texttt{problem\_5.py}
	\begin{enumerate}[label=\alph*)]
		\item The singular values of A are $2$, and $9.99999973\cdot 10^{-9}$. 
		\item Where $\sigma_i$ is the $i$'th singular value of {\bf A}:
		\begin{align*}
			\kappa({\bf A}) &= \frac{\sigma_1}{\sigma_n} \\
			\kappa({\bf A}) &= \frac{2}{9.99999973 \cdot 10^{-9}} \\
			\kappa({\bf A}) &= 200000006
		\end{align*}
		This method of dividing the first singular value by the last returned the same result as \texttt{np.linalg.cond}.
		\item Relative error of ${\bf \tilde b}: 7.071696 \cdot 10^{-13}$\\
		Relative error of ${\bf \tilde x}: 0.0001$.
		\[
			\frac{\text{rel}[{\bf \tilde x}]}{\text{rel}[{\bf \tilde b}]} = \frac{0.0001}{7.071696 \cdot 10^{-13}} = 141421358
		\]
		The ratio between these two quantities is large because the small relative errors in ${\bf \tilde x}$ get massively amplified by {\bf A}'s poor conditioning, which is indicated by its large condition number of 200000006. The ratio between the relative errors in ${\bf \tilde x}$ and ${\bf \tilde b}$ fall well within the below property of the condition number:
		\begin{align*}
			\frac{\|{\bf \tilde{x}}-{\bf x}\|_2}{\|{\bf x}\|_2} &\leq \kappa \cdot \frac{\|{\bf \tilde{b}}-{\bf b}\|_2}{\|{\bf b}\|_2}\\
			\frac{\frac{\|{\bf \tilde{x}}-{\bf x}\|_2}{\|{\bf x}\|_2}}{\frac{\|{\bf \tilde{b}}-{\bf b}\|_2}{\|{\bf b}\|_2}} &\leq \kappa
		\end{align*}
	\end{enumerate}
\end{solution}

\pagebreak

\begin{problem}[SVD]
	Using the singular value decomposition, verify the following facts about the pseudoinverse: 
	\begin{enumerate}[label=\alph*)]
		\item ${\bf A}{\bf A}\pinv{\bf A} = {\bf A}$.
		\item ${\bf A}\pinv {\bf A}{\bf A}\pinv = {\bf A}\pinv$.
		\item If ${\bf A}$ has full column rank, then ${\bf A}\pinv = ({\bf A}\ts {\bf A})\inv {\bf A}\ts$. 
		\item ${\bf A}{\bf A}\pinv$ is the projection onto $\mathcal{R}({\bf A})$. 
		\item The trace (i.e.~sum of the diagonal elements) of ${\bf A}{\bf A}\pinv$ is equal to the rank of ${\bf A}$. (Hint: you make take it as a given that $\text{trace}({\bf AB}) = \text{trace}({\bf BA})$ whenever both products are defined.)
	\end{enumerate}
\end{problem}

\begin{solution}
	Utilizing definition of pseudoinverse: ${\bf A}\pinv = {\bf V\Sigma}^{-1}{\bf U}^T$:
	\begin{enumerate}[label=\alph*)]
		\item ${\bf A}{\bf A}\pinv{\bf A} = {\bf A}$
		\begin{align*}
			{\bf A}{\bf A}\pinv{\bf A} &= {\bf A} \\
			\left({\bf U\Sigma V}^T\right)\left({\bf V\Sigma}^{-1}{\bf U}^T\right)\left({\bf U \Sigma V}^T\right) &= {\bf U \Sigma V}^T \\
			\left({\bf U \Sigma}\right){\bf V}^T{\bf V}\left({\bf \Sigma}^{-1}\right){\bf U}^T{\bf U}\left({\bf \Sigma V}^T\right) &= {\bf U \Sigma V}^T \\
			\left({\bf U \Sigma}\right)I\left({\bf \Sigma}^{-1}\right)I\left({\bf \Sigma V}^T\right) &= {\bf U \Sigma V}^T \\
			\left({\bf U \Sigma}\right)\left({\bf \Sigma}^{-1}{\bf \Sigma V}^T\right) &= {\bf U \Sigma V}^T \\
			\left({\bf U \Sigma}\right)I\left({\bf V}^T\right) &= {\bf U \Sigma V}^T \\
			{\bf U \Sigma V}^T  &= {\bf U \Sigma V}^T 
		\end{align*}
		\item ${\bf A}\pinv {\bf A}{\bf A}\pinv = {\bf A}\pinv$:
		\begin{align*}
			{\bf A}\pinv {\bf A}{\bf A}\pinv &= {\bf A}\pinv \\
			\left({\bf V\Sigma}^{-1}{\bf U}^T\right)\left({\bf U\Sigma V}^T\right)\left({\bf V\Sigma}^{-1}{\bf U}^T\right) &= {\bf V\Sigma}^{-1}{\bf U}^T \\
			\left({\bf V\Sigma}^{-1}\right){\bf U}^T{\bf U}\left({\bf \Sigma}\right){\bf V}^T{\bf V}\left({\bf \Sigma}^{-1}{\bf U}^T\right) &= {\bf V\Sigma}^{-1}{\bf U}^T \\
			\left({\bf V\Sigma}^{-1}\right)I\left({\bf \Sigma}\right)I\left({\bf \Sigma}^{-1}{\bf U}^T\right) &= {\bf V\Sigma}^{-1}{\bf U}^T \\
			\left({\bf V\Sigma}^{-1}\right)\left({\bf \Sigma}{\bf \Sigma}^{-1}{\bf U}^T\right) &= {\bf V\Sigma}^{-1}{\bf U}^T \\
			\left({\bf V\Sigma}^{-1}\right)I\left({\bf U}^T\right) &= {\bf V\Sigma}^{-1}{\bf U}^T \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= {\bf V\Sigma}^{-1}{\bf U}^T
		\end{align*}
		\item If ${\bf A}$ has full column rank, then ${\bf A}\pinv = ({\bf A}\ts {\bf A})\inv {\bf A}\ts$: \\
		Remembering that {\bf U} and {\bf V} are unitary:
		\begin{align*}
			{\bf A}\pinv &= ({\bf A}\ts {\bf A})\inv {\bf A}\ts \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= \left(\left({\bf V \Sigma U}^T\right)\left({\bf U \Sigma V}^T\right)\right)^{-1}\left({\bf V \Sigma U}^T\right) \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= \left(\left({\bf U \Sigma V}^T\right)^{-1}\left({\bf V \Sigma U}^T\right)^{-1}\right)\left({\bf V \Sigma U}^T\right) \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= \left({\bf V \Sigma}^{-1}{\bf U}^T\right)\left({\bf U \Sigma}^{-1} {\bf V}^T\right)\left({\bf V \Sigma U}^T\right) \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= \left({\bf V \Sigma}^{-1}\right){\bf U}^T{\bf U}\left({\bf \Sigma}^{-1}\right) {\bf V}^T{\bf V}\left({\bf \Sigma U}^T\right) \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= \left({\bf V \Sigma}^{-1}\right)I\left({\bf \Sigma}^{-1}\right)I\left({\bf \Sigma U}^T\right) \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= \left({\bf V \Sigma}^{-1}\right)\left({\bf \Sigma}^{-1}{\bf \Sigma U}^T\right) \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= \left({\bf V \Sigma}^{-1}\right)I\left({\bf U}^T\right) \\
			{\bf V\Sigma}^{-1}{\bf U}^T &= {\bf V\Sigma}^{-1}{\bf U}^T
		\end{align*}
		\item ${\bf A}{\bf A}\pinv$ is the projection onto $\mathcal{R}({\bf A})$:
		\begin{align*}
			{\bf A}{\bf A}\pinv &= {\bf UU}^T \\
			\left({\bf U \Sigma V}^T\right)\left({\bf V\Sigma}^{-1}{\bf U}^T\right) &= {\bf UU}^T \\
			\left({\bf U \Sigma}\right){\bf V}^T{\bf V}\left({\bf \Sigma}^{-1}{\bf U}^T\right) &= {\bf UU}^T \\
			\left({\bf U \Sigma}\right)I\left({\bf \Sigma}^{-1}{\bf U}^T\right) &= {\bf UU}^T \\
			\left({\bf U}\right){\bf \Sigma}{\bf \Sigma}^{-1}\left({\bf U}^T\right) &= {\bf UU}^T \\
			\left({\bf U}\right)I\left({\bf U}^T\right) &= {\bf UU}^T \\
			{\bf UU}^T &= {\bf UU}^T
		\end{align*}
		\item The trace (i.e.~sum of the diagonal elements) of ${\bf A}{\bf A}\pinv$ is equal to the rank of ${\bf A}$.
		\begin{align*}
			\text{Trace}[{\bf A}{\bf A}\pinv] &= \text{Trace}[{\bf A}\pinv{\bf A}] \\
			\text{Trace}[{\bf A}\pinv{\bf A}] &= \text{Trace}[{\bf V \Sigma}^{-1}{\bf U}^T{\bf U \Sigma V}^T] \\
			\text{Trace}[{\bf A}\pinv{\bf A}] &= \text{Trace}[{\bf  V}^T{\bf V U}^T{\bf U \Sigma}^{-1}{\bf \Sigma}] \\
			\text{Trace}[{\bf A}\pinv{\bf A}] &= \text{Trace}[I_{\text{Rank[\bf A]}}] \\
			\text{Trace}[{\bf A}\pinv{\bf A}] &= \text{Rank}[{\bf A}]
		\end{align*}
	\end{enumerate}
\end{solution}

\begin{problem}[Data Approximation] % Data approximation
	Consider the set of data 
	\[
	\{x_1,\ldots,x_6\} = \{1,3,4,6,6,7\}. 
	\]
	\begin{enumerate}[label=\alph*)]
		\item Find a number $x$ that minimizes $\sum_{i=1}^6 (x-x_i)^2$. Is $x$ unique? 
		\item Find a number $x$ that minimizes $\sum_{i=1}^6 |x-x_i|$. Is $x$ unique? 
		\item What are the mean and median of the data set? How do they relate to your answers to the previous parts? 
		\item Find a number $x$ that minimizes $\max_{1\leq i \leq 6} |x-x_i|$. Is $x$ unique? 
		\item Repeat the first three parts after adding to the data set the point $x_7 = 78$. How much do each of the answers change? 
	\end{enumerate}
\end{problem}

\begin{solution}
	The code I used for this problem is in the file \texttt{problem\_7.py}
	\begin{enumerate}[label=\alph*)]
		\item x = 4.5 \\
		Solve for where derivative is zero.
		\begin{align*}
			0 &= \frac{d}{dx} \sum_{i=1}^6 (x - x_i)^2 \\
			0 &= \sum_{i=1}^6 \frac{d}{dx} (x - x_i)^2 \\
			0 &= \sum_{i=1}^6 2 (x - x_i) \\
			0 &= 12x - 2\sum_{i=1}^6 x_i \\
			0 &= 12x - 2(27) \\
			54 &= 12x \\
			4.5 &= x
		\end{align*}
		There's only one answer/zero to this problem, so x is unique. I made a plot just to be sure:\\
		\begin{minipage}{\linewidth}
			\centering
			\includegraphics[width=10cm]{parta.png}
			\captionof{figure}{Graph of $\sum_{i=1}^6 (x-x_i)^2$.}
		\end{minipage}\\
		\item $4 < x < 6$\\
		Solve for where derivative is zero.
		\begin{align*}
			0 &= \frac{d}{dx} \sum_{i=1}^6 v \\
			0 &= \sum_{i=1}^6 \frac{d}{dx} |x - x_i| \\
			0 &= \sum_{i=1}^6 \text{sign}[x - x_i]
		\end{align*}
		This can obviously only be zero if the number of elements above x is the same as the number of elements below x. Therefore x must be between 4 and 6. We can say it's 5, but it can be any value between 4 and 6, and is therefore not unique. I made a plot just to be sure:\\
		\begin{minipage}{\linewidth}
			\centering
			\includegraphics[width=10cm]{partb.png}
			\captionof{figure}{Graph of $\sum_{i=1}^6 |x-x_i|$.}
		\end{minipage}\\
		\item The mean of the dataset is 4.5 and the median is 5. These are the same as our first two answers, respectively. (Well 5 satisfies answer 2). This is because the mean of a dataset minimizes the first equation, while the median of a dataset minimizes the second equation.
		\item x = 4. This is a unique answer. I found it by plotting the function and graphing it, with 5000 datapoints.\\
		\begin{minipage}{\linewidth}
			\centering
			\includegraphics[width=10cm]{partd.png}
			\captionof{figure}{Graph of $\max_{1\leq i \leq 6} |x-x_i|$.}
		\end{minipage}\\
		\item Value to minimize $\sum_{i=1}^7 (x-x_i)^2$: 15 \\
		Value to minimize $\sum_{i=1}^7 |x-x_i|$: 6 \\
		Value to minimize $\max_{1\leq i \leq 6} |x-x_i|$ : 39.5 \\\\
		The mean increased a lot, the median not so much: it stayed in the range set by the solution to part (b). The last value changed A LOT. I found these with the help of this graph:\\\\
		\begin{minipage}{\linewidth}
			\centering
			\includegraphics[width=11cm]{parte.png}
			\captionof{figure}{All three earlier functions graphed, including the new datapoint $x_7 = 78$.}
		\end{minipage}
	\end{enumerate}
\end{solution}

\pagebreak

\begin{problem}[Cubit Fit] %cubic fit
	The data set $(x_i,y_i)$ found in \texttt{hw6p3.mat} was produced using the equation
	\[y_i = p_3(x_i) + \xi_i,\]
	where $p_3$ is a degree 3 polynomial and $\xi_i$ is Gaussian noise (i.e., a zero-mean normally distributed random variable). 
	\begin{enumerate}[label = \alph*)]
		\item Explain how to find a polynomial $\tilde{p}_3$ that approximately fits the data by setting up and solving a least-squares problem. 
		\item Solve this least-squares problem. What are the coefficients of the polynomial $\tilde{p}_3$ that you recovered? You may find it helpful to make a plot of your polynomial approximation and the original data, but you do not have to do so. 
	\end{enumerate}
\end{problem}

\begin{solution}
	This is just like problem 5 on exam 2. My code is in the file \texttt{problem\_8.py}.
	\begin{enumerate}[label=\alph*)]
		\item Our goal here is to find the coefficients of the polynomial, which can be written as:
		\[
			y = \alpha_0 + \alpha_1 x + \alpha_2 x^2 + \alpha_3 x^3.
		\] This can be represented the following linear system:
		\[
			\begin{bmatrix}
				y_0 \\
				y_1 \\
				\vdots \\
				y_n
			\end{bmatrix}
			=
			\begin{bmatrix}
				1 & x_0 & x_0^2 & x_0^3 \\
				1 & x_1 & x_1^2 & x_1^3 \\
				\vdots & \vdots & \vdots & \vdots \\
				1 & x_n & x_n^2 & x_n^3
			\end{bmatrix}\begin{bmatrix}
				\alpha_0 \\
				\alpha_1 \\
				\alpha_2 \\
				\alpha_3
			\end{bmatrix}
		\]
		We can easily calculate the entries in the middle matrix, and solve for the $\alpha$ vector with a least-squares solver.
		\item The best-fit coefficients are as follows:
		\[
			\begin{bmatrix}
				\alpha_0 \\
				\alpha_1 \\
				\alpha_2 \\
				\alpha_3
			\end{bmatrix}
			=
			\begin{bmatrix}
				1.0391324 \\
				-1.30903441 \\
				3.59026285 \\
				0.66894116
			\end{bmatrix}
		\]
		\begin{minipage}{\linewidth}
			\centering
			\includegraphics[width=10cm]{prob_8.png}
			\captionof{figure}{Data plotted along with the best-fit polynomial.}
		\end{minipage}
	\end{enumerate}
\end{solution}

\pagebreak

\begin{problem}[SVD and Least Squares]
	Consider the least-squares problem $\min_{{\bf x}} \|{\bf A}{\bf x} - {\bf b}\|_2$, where  
	\[ A = \begin{bmatrix}
		9 & 8 & 10 \\ 2 & 10 & -6 \\ 5 & 3 & 7\\ 10 & 1 & 19
	\end{bmatrix}\quad \text{and}\quad  {\bf b} = \begin{bmatrix}
		9 \\ 10 \\ 7 \\ 6
	\end{bmatrix}.\]
	\begin{enumerate}[label=\alph*)]
		\item To four decimal places, what is the value of ${\bf x}_1 = {\bf A}\pinv {\bf b}$?
		\item To four decimal places, what answer does Matlab return for ${\bf x}_2 = {\bf A}\backslash {\bf b}$? [For Python, use \texttt{numpy.linalg.lstsq}.]
		\item Between ${\bf x}_1$ and ${\bf x}_2$, which has the smaller 2-norm? 
		\item Between ${\bf x}_1$ and ${\bf x}_2$, which leaves the smaller residual $\|{\bf b} - {\bf Ax}\|_2$? 
		\item If we define ${\bf r}_1 = {\bf b} - {\bf A}{\bf x}_1$ and ${\bf r}_2 = {\bf b} - {\bf A}{\bf x}_2$, what is $\|{\bf r}_1 - {\bf r}_2\|_2$? What do you conclude? 
		\item Find a solution to the least-squares problem that is not equal to either ${\bf x}_1$ or ${\bf x}_2$. How did you come up with it? 
	\end{enumerate}
\end{problem}

\begin{solution}
	I copped out and went to MATLAB for this one. My code is in the file \texttt{problem\_9.m}
	\begin{enumerate}[label=\alph*)]
		\item
		\[ x_1 =
			\begin{bmatrix}
				0.4329 \\
				0.8456 \\
				0.0203
			\end{bmatrix}
		\]
		\item 
		\[ x_2 = 
			\begin{bmatrix}
				0 \\
				1.0620 \\
				0.2368 
			\end{bmatrix}
		\]
		\item $x_1$ has the smaller 2-norm, of 0.950171 versus $x_2$'s norm of 1.088106.
		\item $x_1$'s residual is ever so slightly smaller than $x_2$'s, by just a \emph{hair}. This is probably just a rounding error.
		\begin{align*}
			\|{\bf b} - {\bf Ax_1}\|_2 &= 2.993055723723570 \\
			\|{\bf b} - {\bf Ax_2}\|_2 &= 2.993055723723571
		\end{align*}
		\item $\|{\bf r}_1 - {\bf r}_2\|_2 = 1.2307 \cdot 10^{-14}$ I conclude that x1 and x2 are pretty much identical in terms of their ``goodness.'' This is to be expected, as this system is overdetermined, and can have infinitely many solutions as a result.
		\item 
		\[
			{\bf x}_3 = \begin{bmatrix}
				-0.3836 \\
				1.2538 \\
				0.4286
			\end{bmatrix}.
		\]
		This solution is a linear combination of the pseudoinverse solution and a linear combination of the basis vectors of the nullspace to {\bf A}'s column space. To be precise I added the 3rd column of {\bf V} to ${\bf x}_1$, as this column is the only basis vector for the nullspace, as A is rank 2. (lecture 20 notes)
		
	\end{enumerate}
\end{solution}


\end{document}