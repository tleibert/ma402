# %% imports
import numpy as np

A = np.array([[1, 1, 0, 0], [0, 1, 1, 0], [0, 0, 1, 1], [1, 0, 0, 1]])


# %% part a
print(np.linalg.matrix_rank(A))

# %% part b
print("2-norm of A ", np.linalg.norm(A, 2))
print("Frobenius norm of A ", np.linalg.norm(A, "fro"))

# %% part c
u, s, vt = np.linalg.svd(A)
v = vt.T
# rank 3 so we truncate to rank 3
print(u[:, :3])
print(np.diag(s[:3]))
print(vt[:3, :])
# %% part d
# last column of vt is the basis of the null space
# as A is rank 3
print(vt[3, :].T * 2)

# %% part e best rank-1 approximation will use the rank-1 truncated svd
print(u[:, :1] @ np.diag(s[:1]) @ vt[:1, :])

# %% part f
# use first 3 columns of U only as they define its column space
pi = u[:, :3] @ u[:, :3].T

# %%
