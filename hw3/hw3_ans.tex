\documentclass{article}[10pt]

% PACKAGES AND DEFINITIONS
\usepackage{amsmath,amsthm,amssymb}
\usepackage{graphicx,enumitem,multicol}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{dsfont}
\usepackage{minted}
\usepackage{caption}

\theoremstyle{definition}
\newtheorem{problem}{Problem}
\specialcomment{solution}{{\bf Solution: }}{}
\setlength{\parindent}{0pt}

\newcommand{\x}{{\bf x}}
\newcommand{\y}{{\bf y}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\1}{\mathds{1}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\unif}{\mathcal{U}}
\newcommand{\inv}{^{-1}}

\DeclareMathOperator*{\var}{Var}
\DeclareMathOperator*{\prob}{Pr}
\DeclareMathOperator*{\Ber}{Ber}

% TOGGLE SOLUTION HERE
%\excludecomment{solution}

\begin{document}
	
	
\title{Homework \#3}
\author{Trevor Leibert}	
\date{\today}

\maketitle


\begin{problem}[The Wisdom of Crowds]
	Suppose that in a blind test, an expert can correctly tell a red wine from a white 75\% of the time. An amateur can do the same 60\% of the time. 
	\begin{enumerate}[label=\alph*)]
		\item Suppose that three amateurs join forces: they make their guesses independently and go with the majority vote. Will they outperform a single expert? 
		\item How does the probability of guessing correctly change as the number of amateurs in the group increases? You don't need to do any calculations for this part, but do cite the Law of Large Numbers. 
		\item How might things be affected if the amateurs talk to each other rather than guessing independently? 
	\end{enumerate}
\end{problem}

\begin{solution}
	This scenario can be modeled as a binomial distribution, with $p = 0.6$ representing the chance of an amateur successfully identifying a wine.
	\begin{enumerate}[label=\alph*)]
		\item My python code is as follows:
		\begin{minted}{Python}
			import scipy.special

			ch = 0.6

			# chance of getting x hits in n trials in a binomial distribution
			# with a chance of success specified by c
			p = lambda n, x: scipy.special.binom(n, x) * ch ** x * (1 - ch) ** (n - x)
			
			# cumulative chance of getting x hits thru n hits in n trials
			c = lambda n, x: sum(p(n, i) for i in range(x, n + 1))
			
			print(c(3, 2))
		\end{minted}
		The output from this code is 0.648, which is better than the amateurs would do on their own, but not better than the 0.75 probability that the expert manages.
		\item As the number of amateurs increases the probability of a majority of the group correctly identifying the wine approaches 1. This tendency can be seen when running this addendum to the prior code:
		\begin{minted}{Python}
		import math
		for i in range(3, 101, 2):
    x_maj = math.ceil(i / 2)
    print(f"c({i}, {x_maj}): {c(i, x_maj)}")
		\end{minted}
		The outputs from this loop approach 1 as \texttt{i} increases. The law of large numbers states that as the number of trials increases (number of amateur tasters increases), the average of the results from the trials will approach the expected value. The expected value from our amateur tasters is 0.6 or 60\% will correctly identify the wine.
		If they go with the majority vote on each independent guess, the chance of them being right will approach 1, or 100\%, as their expected value of 60\% right is greater than the \% needed for a simple majority.
		\item If the amateurs talk to eachother instead of guessing independently, this property of ``group wisdom'' will be lost, as the ``effective'' number of datapoints drops.
		Furthermore, because the amateurs are talking to one another, we loose the property of each amateur's vote being independent, and can therefore no longer model this situation as a binomial distribution.
	\end{enumerate}
	The code for this part is inside the file \texttt{problem\_1.py}.
\end{solution}

\begin{problem}[Discrete Distribution]
	Consider a game where you flip 3 fair coins. If you get all heads {\it or} all tails, you win 6 dollars. If you get 2 heads and 1 tail, you win 3 dollars. If you get 1 head and 2 tails, you lose 4 dollars. Let $X$ be the random variable representing your winnings. 
	\begin{enumerate}[label = \alph*)]
		\item What is the probability mass function (PMF) of $X$? Give your answer in the form of either a table or a graph. 
		\item If you play this game twice, what is the probability that you will win at least 1 dollar? Assume that the two runs of the game have independent outcomes. 
		\item What is $\E[X]$? 
		\item What is $\var[X]$? 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item Here's my table for the PMF:
		\begin{center}
			\begin{tabular}{c|c|c|c}
				\hline
				Outcome in \$ $(x)$ & -4 & 3 & 6 \\
				\hline
				Probability $(P(x))$ & 3/8 & 3/8 & 1/4 \\
				\hline
			\end{tabular}
		\end{center}
		\pagebreak
		\item There are three possible scenarios where the player \emph{does  not} win at least \$1:
		\begin{itemize}
			\item Lose \$4, then lose \$4 again. $P(x) = 3/8 \cdot 3/8 = 9/64$
			\item Lose \$4, then win \$3. $P(x) = 3/8 \cdot 3/8 = 9/64$
			\item Win \$3, then lose \$4. $P(x) = 3/8 \cdot 3/8 = 9/64$
		\end{itemize}
		The probability of any one of these three scenarios occurring, where the player \emph{does not} win at least \$1, is $\frac{27}{64}$. \\
		Therefore, the probability that the player \emph{does} win at least \$1 is $1 - \frac{27}{64}$, or $\frac{37}{64}$.
		\item $\E[x] = 1.125$
		\begin{align*}
			\E[X] &= \left(-4 \cdot \frac{3}{8}\right) + \left(3 \cdot \frac{3}{8}\right) + \left(6 \cdot \frac{1}{4}\right) \\
			\E[X] &= \frac{-12}{8} + \frac{9}{8} + \frac{12}{8} \\
			\E[X] &= \frac{9}{8}
		\end{align*}
		\item $\var[X] = 17.109375$
		\begin{align*}
			\E[X] &= \frac{9}{8} \\
			\var[X] &= \E[(X - \E[X])^2] \\
			\var[X] &= \E\left[\left(X - \frac{9}{8}\right)^2\right] \\
			\var[X] &= \left(-4 - \frac{9}{8}\right) ^ 2 \cdot \frac{3}{8}+ \left(3 - \frac{9}{8}\right) ^ 2 \cdot \frac{3}{8} + \left(6 - \frac{9}{8}\right) ^ 2 \cdot \frac{1}{4} \\
			\var[X] &= \left(\frac{-41}{8}\right)^2 \cdot \frac{3}{8} + \left(\frac{15}{8}\right)^2 \cdot \frac{3}{8} + \left(\frac{39}{8}\right)^2 \cdot \frac{1}{4} \\
			\var[X] &= \frac{1681}{64} \cdot \frac{3}{8} + \frac{225}{64} \cdot \frac{3}{8} + \frac{1521}{64} \cdot \frac{1}{4} \\
			\var[X] &= \frac{5043}{512} + \frac{675}{512} + \frac{1521}{256} \\
			\var[X] &= \frac{1095}{64}
		\end{align*}
	\end{enumerate}
\end{solution}

\pagebreak

% One on the basic measurements for a continuous distribution 
\begin{problem}[Exponential Distribution]
	Consider modeling the lifespan of a certain lightbulb with a random variable $X$, where 
	\[f_X(x) = \begin{cases}
		\alpha e^{-\lambda x} & x \geq 0, \\
		0 & x < 0,
	\end{cases}\]
	and $\lambda$ is a known parameter having to do with the quality of the lightbulb. 
	\begin{enumerate}[label = \alph*)]
		\item What value of $\alpha$ will make $f_X$ a valid probability density function? 
		\item What is the cumulative distribution function (CDF) of $X$? 
		\item Set up an integral that gives you the expected lifespan of the lightbulb (you do not have to solve it). 
		\item What is the {\it median} lifespan of the lightbulb? This is the value $m$ such that $\prob[X \geq m] = 0.5$.  
		\item Set up an integral that gives you the variance of $X$ (you do not have to solve it). How would you use this answer to find the standard deviation of $X$? 
		\item How is the value of $\lambda$ related to the quality of the lightbulb? 
	\end{enumerate}
\end{problem}


\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item To find $\alpha$ we must solve the equation
		\begin{align*}
			1 = \int_{-\infty}^\infty\alpha e^{-\lambda x} \,dx
		\end{align*}
		for it. We can make the lower bound 0 because the $f_X(x)$ is defined as being 0 when $x<0$.
		\begin{align*}
			1 &= \int_0^\infty\alpha e^{-\lambda x} \,dx \\
			1 &= \alpha \int_0^\infty e^{-\lambda x} \,dx \\
			1 &= \alpha \left[\frac{e^{-\lambda x}}{-\lambda}\right]_0^\infty \\
			1 &= \alpha \left(0 - \frac{1}{-\lambda}\right) \\
			1 &= \alpha \left(\frac{1}{\lambda}\right) \\ 
			\alpha &= \lambda
		\end{align*}
		\item The CDF of X can be obtained by integrating the PDF from $-\infty$ to $x$. Because this PDF is defined as being zero before $x=0$, we can integrate it from 0 to $x$ instead. I'll change the variable $x$ to $t$ inside the integral for sake of clarity.
		\begin{align*}
			F_X(x) &= \int_0^x f_X(t) \,dt \\
			F_X(x) &= \int_0^x \lambda e^{-\lambda t} \,dt \\
			F_X(x) &= \left[\frac{\lambda e^{-\lambda t}}{-\lambda}\right]_0^x \\
			F_X(x) &= \left[-e^{-\lambda t}\right]_0^x \\
			F_X(x) &= -e^{-\lambda x} - -e^{-\lambda 0} \\
			F_X(x) &= -e^{-\lambda x} + 1 \\
			F_X(x) &= 1 - e^{-\lambda x}
		\end{align*}
		Now we just combine this part where $x\geq0$ with the part where $x<0$:
		\[F_X(x) = \begin{cases}
			1 - e^{-\lambda x} & x \geq 0, \\
			0 & x < 0,
		\end{cases}\]
		\item
		\begin{align*}
			\E[X] &= \int_{-\infty}^\infty x f_X(x) \,dx \\
			f_X(x) &= 0 \text{ for } x<0 \\
			\E[X] &= \int_0^\infty x f_X(x) \,dx \\
			\E[X] &= \int_0^\infty x (\lambda e^{-\lambda x}) \,dx
		\end{align*}
		\item To find the value of $m$ to satisfy $\prob[X \geq m] = 0.5$, we can use the CDF of $X$. We can also use the fact that $\prob[X < m] = 0.5$, which comes from the complement of the set.
		\begin{gather*}
			\prob[X < m] = 0.5 \\
			\text{Definition of CDF} \\
			\prob[X \leq m] = F_X(m) \\
			\text{The overlap at the equals mark is negligible.} \\
			0.5 = 1 - e^{-\lambda m} \\
			0.5 = e^{-\lambda m} \\
			\log(0.5) = -\lambda m \\
			m = \frac{-\log(0.5)}{\lambda} 
		\end{gather*}
		\item The variance of $X$ depends on the expected value of $X$, which evaluates to $\frac{1}{\lambda}$. (I got this with a symbolic solver on my answer to part c).
		\begin{align*}
			\var[X] &= \E[(X - \E[X])^2] \\
			\var[X] &= \E\left[\left(X - \frac{1}{\lambda}\right)^2\right] \\
			\var[X] &= \int_0^\infty \left(x - \frac{1}{\lambda}\right)^2 \,dx 
		\end{align*}
		\item As $\lambda$ increases, the lightbulb's lifespan decreases.
		This can be seen clearly using the answer to part d.
		The median of the lifespan of the lightbulb decreases as $\lambda$ increases, and increases as $\lambda$ decreases.
		Therefore $\lambda$ is inversely related to the lifespan of the lightbulb.
	\end{enumerate}
\end{solution}

\begin{problem}[Independence]
	Let $X$ and $Y$ be the numbers shown when rolling two fair six-sided dice, and assume that $X$ and $Y$ are independent. Consider taking the maximum $Z = \max\{X,Y\}$ and minimum $W = \min\{X,Y\}$. Are $Z$ and $W$ also independent? Why or why not? 
\end{problem}

\begin{solution}
	The values of $Z$ and $W$ are not independent, as we know that the maximum, $Z$, will never be less than the minimum, $W$. $W$ will also never be more than $Z$. Therefore they are not independent, as knowing one means that we know about another.
\end{solution}

\pagebreak

\begin{problem}[Random Variable Algebra]
	Let $X$ and $Y$ be independent random variables such that $X\sim \unif(1,3)$ and $Y\sim \unif(0,1)$. Compute the following: 
	\begin{enumerate}[label = \alph*)]
		\item $\E[2X - Y]$
		\item $\E[X^2 + 2XY]$
		\item $\var[X - 3Y]$
	\end{enumerate}
	It may help to know that $\E[X] = 2$, $\E[X^2] = 13/3$, $\E[Y] = 1/2$ and $\E[Y^2]=1/3$. 
\end{problem}

\begin{solution}
	For these answers I will use the Theorem 1 about expected values from last year's notes on expected values:
	\begin{align*}
		\E[aX + bY] = a\E[X] + b\E[Y],
	\end{align*}
	as well as the fact that $\E[XY] = \E[X] \cdot \E[Y]$ from Theorem 3.
	\begin{enumerate}[label=\alph*)]
		\item $\E[2X - Y] = 1.5$
		\begin{align*}
			\E[2X - Y] &= 2\E[X] - \E[Y] \\
			\E[2X - Y] &= 2(2) - \frac{1}{2} \\
			\E[2X - Y] &= 1.5
		\end{align*}
		\item $\E[X^2 + 2XY] = \frac{19}{3}$
		\begin{align*}
			\E[X^2 + 2XY] &= \E[X^2] + 2\E[XY] \\
			\E[X^2 + 2XY] &= \frac{13}{3} + 2\cdot\E[X]\cdot\E[Y] \\
			\E[X^2 + 2XY] &= \frac{13}{3} + 2\cdot2\cdot\frac{1}{2} \\
			\E[X^2 + 2XY] &= \frac{13}{3} + 2 \\
			\E[X^2 + 2XY] &= \frac{19}{3}
		\end{align*}
		\item $\var[X - 3Y] = \frac{19}{12}$
		\begin{align*}
			\text{Using this alternate defin}& \text{ition of variance:} \var[X] =  \E[X^2] + \E[X]^2 \\
			\var[X - 3Y] &= \E[(X - 3Y)^2] + \E[X - 3Y]^2 \\
			\var[X - 3Y] &= \E[X^2 - 6XY + 9Y^2] + (\E[X] -3\E[Y])^2 \\
			\var[X - 3Y] &= \E[X^2] - 6\cdot\E[X]\cdot\E[Y] + 9\E[Y^2] + (\E[X] - 3\E[Y])^2 \\
			\var[X - 3Y] &= \frac{13}{3} - 6 \cdot 2 \cdot \frac{1}{2} + 9 \cdot \frac{1}{3} + \left(2 - 3 \cdot \frac{1}{2}\right)^2 \\
			\var[X - 3Y] &= \frac{13}{3} - 6 + 3 + \left(\frac{1}{2}\right)^2 \\
			\var[X - 3Y] &= \frac{13}{3} - 6 + 3 + \frac{1}{4} \\
			\var[X - 3Y] &= \frac{52}{12} - \frac{72}{12} + \frac{36}{12} + \frac{3}{12} \\
			\var[X - 3Y] &= \frac{19}{12}
		\end{align*}
	\end{enumerate}
\end{solution}

\begin{problem}[Central Limit Theorem]
	A certain coin lands on heads 35 percent of the time. 
	\begin{enumerate}[label = \alph*)]
		\item If the coin is flipped 1000 times, use the Central Limit Theorem to approximate the probability that betwen 325 and 375 heads are flipped. 
		\item Write out the exact answer using the Binomial Theorem, and evaluate it using Matlab or Python (\texttt{binocdf} will be useful here). How does it compare to your approximation? 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item We know $\E[X] = p = 0.35$ and $\var[X] = p(1 - p) = 0.2275$.
		This is the quantity that we want to find, with $x$ representing the sum total of heads flipped:
		\begin{equation*}
			P[x \leq 375] - P[x \leq 325]
		\end{equation*}
		To estimate this value, we can find out how many standard deviations each part is away from the mean, and then use our knowledge of a standard normal distribution to estimate probabilities.
		To answer the question ``how many standard deviations is $\bar X_n$ from the mean $\mu$?'' we can use this formula to get the Z-score:
		\begin{equation*}
			\bar Z_n = \frac{\sqrt{n}}{\sigma}(\bar X_n - \mu)
		\end{equation*}

		Plugging it in for both $\frac{375}{1000}$ and $\frac{325}{1000}$ as values of $\bar X$, and $\sigma^2 = 0.2275$ and $\mu = 0.35$:
		\begin{alignat*}{2}
			\frac{\sqrt{1000}}{\sqrt{0.2275}}(.375 - .35) &\qquad \frac{\sqrt{1000}}{\sqrt{0.2275}}(.325 - .35) \\ 
			\frac{\sqrt{1000}}{\sqrt{0.2275}}(.025) &\qquad \frac{\sqrt{1000}}{\sqrt{0.2275}}(-.025) \\
			1.6575 &\qquad -1.6575 
		\end{alignat*}

	    This means that our answer is the area enclosed under a standard normal bell curve, between -1.6575 and 1.6575 standard deviations from the mean. Eyeballing a chart from google, I'd say it's around 77\%. Using \texttt{scipy.stats.norm}, it ended up as 90.258\% (guess I'm not that good at eyeballing!).
		\item Using Binomial theorem:
		\begin{align*}
			P(375) &= {1000 \choose 375}(0.35)^{375}(1-.35)^{1000 - 375} \\
			P(375) &= {1000 \choose 375}(0.35)^{375}(0.65)^{625} \\
			P(325) &= {1000 \choose 325}(0.35)^{325}(1-.35)^{1000 - 325} \\
			P(325) &= {1000 \choose 325}(0.35)^{325}(0.65)^{675}
		\end{align*}
		Using NumPy, I got the total probability as about 90.245\%, with the following code:
		\begin{minted}{Python}
			from scipy.stats import binom

			c = binom(1000, 0.35)
			print(c.cdf(375) - c.cdf(325))
		\end{minted}
	\end{enumerate}
\end{solution}

% Problem: Jokes
\begin{problem}[Hilarious Joke]
	The book {\it A Million Random Digits with 100,000 Normal Deviates} is a random number table produce by the RAND corporation and originally published in 1955.\footnote{Wikipedia: \url{https://en.wikipedia.org/wiki/A_Million_Random_Digits_with_100,000_Normal_Deviates}} The book has received several 1-star reviews on Amazon\footnote{\url{https://www.amazon.com/Million-Random-Digits-Normal-Deviates/dp/0833030477/}}, one of which contains the following:
	\begin{it}
		\begin{quote}
			They sure don't come up with random numbers like they used to. If you look closely, you will note that every tenth digit or so is just a repeat of the last digit and every hundredth or so is a just the same digit repeated three times. How sloppy!
		\end{quote}
	\end{it}
	Please explain the joke, preferably in terms of expected value and/or the Law of Large Numbers. More seriously, why do you think such a book may have been worth publishing in the first place? 
\end{problem}

\begin{solution}
	The chance of getting two repeating digits in a row is 1 in 10, and three is 1 in 100. Therefore the expected value of number of 2-digit repeating strings would be $n/10$, and 3-digit repeating strings would be $n/100$, where n is the total number of characters in the range 0-9.
	Therefore as the number of characters increases, the actual number of 2-repeat strings and 3-repeat strings will converge towards their expected values, making the statement ``every tenth digit or so is just a repeat of the last digit'' a reality, and furthermore, a sign that the numbers were \emph{truly} random.
	The same holds true for the 3-digit repeat strings ass well.
\end{solution}

\begin{problem}[Programming]
	Let $N$ be a random variable that selects an integer from the range $\{100,101,\ldots,200\}$ with equal probability. 
	\begin{enumerate}[label=\alph*)]
		\item Use Matlab or Python to compute the variance of $N$ without using the \texttt{var} or \texttt{std} functions. 
		\item What is the difference between the commands \texttt{var(X,0)} and \texttt{var(X,1)} (or in Python, \texttt{numpy.var(X,ddof=1)} and \texttt{numpy.var(X,ddof=0)})? Which one returns the correct answer for the purposes of this problem? 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item This random variable represents a discrete distribution. The expected value of this variable is the mean of the the high and low values:
		\begin{equation*}
			\frac{100 + 200}{2} = 150.
		\end{equation*}
		To find the expected value and variance of the variable, I ran this code:
		\begin{minted}{Python}
			from numpy import arange

			a = arange(100, 201)
			exp_val = sum(a) / len(a)
			var_a = sum((a - exp_val) ** 2) / len(a)
			print(f"Expected value of N: {exp_val}, Variance of N: {var_a}")
		\end{minted}
		At the end of this code, \texttt{exp\_val} was equal to 150, and \texttt{var\_a} was equal to 850.
		My code with scipy is as follows:
		\begin{minted}{Python}
			from scipy.stats import randint

			u = randint(100, 201)
			exp_u = u.expect()
			var_u = u.var()
			print(f"Expected value of N: {exp_u}, Variance of N: {var_u}")
		\end{minted}
		Output: \texttt{Expected value of N: 150.0, Variance of N: 850.0}.
		\item \texttt{numpy.var(X, ddof=0)} is the correct command for this case. I know it is correct as we know each datapoint exactly, due to $N$'s exact definition. \texttt{numpy.var(X, ddof=1)} would be used if we didn't fully know this vector.
	\end{enumerate}
\end{solution}

% Problem: Matlab with the Cauchy distribution. 
\begin{problem}[Cauchy Distribution]
	Consider a random variable $X$ drawn from the {\it standard Cauchy distribution}, with probability density function 
	\[f_X(x) = \frac{1}{\pi(1+x^2)}.\]
	\begin{enumerate}[label=\alph*)]
		\item Verify that $f_X$ gives a valid probability distribution. 
		\item One might intuitively argue that $\E[X] = 0$ due to the symmetry of $f_X(x)$ about the y-axis, but such a claim is problematic. Instead, it is customary to split $X$ into its positive and negative components $X_+ = \max\{X,0\}$ and $X_- = -\min\{X,0\}$. It follows that $X = X_+ - X_-$, and 
		\[\E[X_+] = \int_0^\infty xf_X(x)\,dx\quad \text{and}\quad \E[X_-] = -\int_{-\infty}^0 xf_X(x)\,dx.\]
		We then say that $\E[X]$ is undefined if $\E[X_+]$ and $\E[X_-]$ are both infinite, and otherwise $\E[X] = \E[X_+] - \E[X_-]$. 
		
		Using this definition of the expected value, show that $\E[X]$ is undefined. 
		
		\item Modify the code \texttt{weaklln.m} or \texttt{weaklln.py} to test how well the Weak Law of Large Numbers describes the behavior of the Cauchy distribution. What do you observe? 
	\end{enumerate} 
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item To verify this we must evaluate this integral, and see if it is equal to 1:
		\begin{align*}
			1 &= \int_{-\infty}^\infty \frac{1}{\pi(1+x^2)}\,dx \\
			1 &= \frac{1}{\pi}\int_{-\infty}^\infty \frac{1}{1+x^2}\,dx \\
			1 &= \frac{1}{\pi}\left[\arctan(x)\right]_{-\infty}^\infty \\
			1 &= \frac{1}{\pi}\left(\frac{\pi}{2} - \frac{-\pi}{2}\right) \\
			1 &= \frac{1}{\pi}(\pi) \\
			1 &= 1
		\end{align*}
		The distribution checks out. (Note: I used symbolab to evaluate this integral.)
		\item Now we must evaluate both integrals from the question part b:
		\begin{align*}
			\E[X_+] &= \int_0^\infty x f_X(x)\,dx &\qquad \E[X_-] &= \int_{-\infty}^0 x f_X(x)\,dx \\
			\E[X_+] &= \int_0^\infty x \frac{1}{\pi(1+x^2)}\,dx &\qquad \E[X_-] &= \int_{-\infty}^0 x \frac{1}{\pi(1+x^2)}\,dx \\
			\E[X_+] &= \frac{1}{\pi}\int_0^\infty \frac{x}{1+x^2}\,dx &\qquad \E[X_-] &= \frac{1}{\pi}\int_{-\infty}^0 \frac{x}{1+x^2}\,dx \\
			\E[X_+] &= \frac{1}{\pi}\left[\frac{1}{2}\log(|1 + x^2|)\right]_0^\infty\ &\qquad \E[X_-] &= \frac{1}{\pi}\left[\frac{1}{2}\log(|1 + x^2|)\right]_{-\infty}^0\ \\
			\E[X_+] &= \frac{1}{2\pi}(\log(\infty) - \log(1)) &\qquad \E[X_-] &= \frac{1}{2\pi}(\log(1) - \log(\infty))
		\end{align*}
		Clearly both of these are undefined, as $\log(\infty)$ is $\infty$.
		\item What we're looking for is to see that the expected value for this function is undefined. This would mean that with the law of large numbers, the values \emph{do not} converge on any particular value. The program provided in \texttt{weaklln.py} proves this, when using
		\begin{minted}{Python}
			fun = lambda : numpy.random.standard_cauchy()
		\end{minted}
		as the function to plot. \\
		\begin{minipage}{\linewidth}
			\centering
			\includegraphics[scale=0.5]{cauchy.png}
			\captionof{figure}{Weak Law of Large numbers on the standard Cauchy distribution.}
		\end{minipage}
	\end{enumerate}
\end{solution}

\begin{problem}[St.~Petersburg Paradox]
	Consider flipping a fair coin repeatedly until you get tails, and winning $X = 2^Y$ dollars for flipping $Y$ heads. So, you win 1 dollar if you get tails on the first throw, 2 dollars for \texttt{HT}, 4 for \texttt{HHT}, 8 for \texttt{HHHT}, and so on. 
	\begin{enumerate}[label = \alph*)]
		\item What is the probability that you win 16 or more dollars in a single game? 
		\item Show that $\E[X]$ is infinite. 
		\item How much would you actually be willing to pay to play this game if you were only permitted to play once? What if you were permitted to play a hundred times? Why is this problem referred to as a paradox? You might consider testing the code \texttt{weaklln} on the function \texttt{stpete} (provided in the code) as part of your response. 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item The probability of winning 16 or more dollars in a single game is the complement of the sum of every game where less than 16 dollars is won. 16 dollars requires 4 heads in a row, which is 5 coinflips total, as the last would be a tails.
		\begin{align*}
			p &= 1 - \left(\frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \frac{1}{16}\right) \\
			p &= 1 - \frac{15}{16} \\
			p &= \frac{1}{16}
		\end{align*}
		\item $\E[X]$ cannot be negative, as there is no way to lose money in this game. We can find its value with this integral:
		\begin{align*}
			\E[X] &= \int_0^\infty x2^x \,dx \\
			\E[X] &= \left[\frac{2^xx}{\log(2)} - \frac{2^x}{(\log(2))^2}\right]_0^\infty \\
			\E[X] &= \left[2^x\left(\frac{x}{\log(2)} - \frac{1}{(\log(2))^2}\right)\right]_0^\infty
		\end{align*}
		This clearly diverges towards positive infinity. (Note: I used symbolab to do the integration by parts).
		\item Since the game as described doesn't cost me anything to play, I don't see why I wouldn't play it, even if I was only allowed to play it once. My winnings will (likely) increase if I'm allowed to play 100 times. This ``game'' as described might be considered to be a paradox because, while the expected value is infinite, the real payouts are usually within the range of 0-4 dollars, making the game not seem worth its asking price, even when the expected value is infinite (as would the gains be if one played it enough times, shown via the LLN).
	\end{enumerate}
\end{solution}
	
\end{document}