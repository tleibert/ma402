import numpy as np
import scipy.special
import math

ch = 0.6

# chance of getting x hits in n trials in a binomial distribution
# with a chance of success specified by c
p = lambda n, x: scipy.special.binom(n, x) * ch ** x * (1 - ch) ** (n - x)

# cumulative chance of getting x hits thru n hits in n trials
c = lambda n, x: sum(p(n, i) for i in range(x, n + 1))

print(f"Probability of 2 or 3 tasters getting it right: {c(3, 2)}")

for i in range(3, 101, 2):
    x_maj = math.ceil(i / 2)
    print(f"c({i}, {x_maj}): {c(i, x_maj)}")