# weaklln.py
# Demonstrates the weak Law of Large Numbers on various functions
# %% imports
import numpy
import matplotlib.pyplot as plt


def stpete(): #Function for the St Petersburg paradox
    y = 1.0
    while numpy.random.randint(0,2) == 1:
        y = 2*y
    
    return y 

# %% data -- cell to get ipython to spin up
plt.figure()

numSamples = 1000
numExperiments = 25

# Function to test
# fun = lambda  : numpy.sqrt(numpy.random.random()) # F(t) = t^2, or f(t) = 2t
fun = lambda : stpete()
#fun = lambda : numpy.tan(numpy.pi*(numpy.random.random()-0.5))
# define fun as our function from problem 9
# fun = lambda : numpy.random.standard_cauchy()

# Plot the expected value, if known
expVal = 2.0/3
plt.hlines(expVal,0,numSamples,linestyles = 'dashed')

# Set the ylim, if desired
plt.ylim((0,1))


# Run the experiments!
runningAvg = numpy.zeros(numSamples)

for i in range(numExperiments):
    S = 0
    for j in range(numSamples):
        S += fun()
        runningAvg[j] = S/(j+1.0)

    plt.plot(runningAvg)
    

plt.ylabel("Running Average",size=20)
plt.xlabel("n",size=20)
plt.title("Weak Law of Large Numbers",size=18)
plt.show()
plt.savefig("stpete.png", transparent=False, edgecolor="w", facecolor="w")

# for _ in range(100):
#     print(stpete())
# %%
