% weaklln.m
% Demonstrates the weak Law of Large Numbers
figure; hold on

% ------ FUNCTION TO TEST -------- %
fun = @()(rand<0.5);          % Fair coin flip
%fun = @()sqrt(rand);        % F(t) = t^2, or f(t) = 2t
%fun = @stpete;              % St Petersburg Paradox function
%fun = @()tan(pi*(rand-.5)); % Cauchy distribution

% --- The expected value, if known --- %
expVal = 0.5; 
% yline(expVal,'k--','Linewidth',2);

% --- Consider limiting the y window --- %
% ---  if there are extreme outliers --- %
ylim([0,1]);


% --- Number of samples per experiment --- %
% ------ and number of experiments ------- %
numSamples = 1000;
numExperiments = 50; 
 

% --- Run the experiments --- %
runningAvg = zeros(1,numSamples);

for i = 1:numExperiments
    S = 0; 
    for j = 1:numSamples
        S = S + fun();
        runningAvg(j) = S/j; 
    end
    plot(runningAvg); 
end

% --- Plot labels --- %
ylabel('Running Average','Fontsize',18); 
xlabel('n','Fontsize',18); 
title('Weak Law of Large Numbers','Fontsize',18);

 
function y = stpete()
    % Generates a random variable according to 
    % the distribution of the St Petersburg Paradox. 
    % Returns y = 2^n, where n is the number of successive heads flipped
    % before the first tails on a fair coin. 
    
    y = 1; 
    while randi(2) == 1
        y = y*2;
    end
end

