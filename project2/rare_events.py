# %% imports
import numpy as np
import matplotlib.pyplot as plt

# %% definitions

# random variable X
def run_experiment(num_trials, dimension):
    """
    Samples the random variable X num_trials times 
    in the given dimension.
    Returns the sample.
    """
    numbers = np.random.uniform(-1, 1, (num_trials, 2))
    selector = (np.linalg.norm(numbers, axis=1) <= 1)
    X = (2 ** dimension) * selector[:,np.newaxis]
    return X

# %% part a
num_trials = [10, 50, 500]
res = [run_experiment(trials, 2) for trials in num_trials]
exp_vals = [np.mean(data) for data in res]
for i in range(len(num_trials)):
    print(f"Mean/Approximate Expected Value from {num_trials[i]} trials: {exp_vals[i]}")

# %% optional graph for part a
graph = False
if graph:
    moar = np.random.uniform(-1, 1, (500, 2))
    sel = np.linalg.norm(moar, axis=1) <= 1
    inside = moar[sel]
    plt.scatter(inside[:,0], inside[:,1], s=4, c="b", label="norm $\leq$ 1")
    outside = moar[~sel]
    plt.scatter(outside[:,0], outside[:,1], s=4, c="r", label="norm > 1")
    plt.yticks(np.linspace(-1, 1, 5))
    plt.ylabel("y values", fontsize=14)
    plt.xlabel("x values", fontsize=14)
    plt.legend()
    plt.title("500 uniform dots, separated by norm", fontsize=14)
    plt.gca().set_aspect('equal')
    plt.savefig("circle")
# %% part c
# X bar is the mean of many samples from X.

# get 500 samples
data = run_experiment(500, 2)
std_dev = np.std(data)
std_err = std_dev / np.sqrt(len(data))

print(f"Sample mean: {np.mean(data)}")
print(f"Standard Deviation in sample: {std_dev:3g}")
print(f"Standard Error in sample: {std_err}")

# %% part d
from scipy.stats import norm
prob = norm.cdf(1.96) - norm.cdf(-1.96)
print(prob)

# %% part e
SAMPLE_SIZE = 1000000
LATEX = True
# generate data
for d in range(2, 17):
    data = run_experiment(SAMPLE_SIZE, d)
    m = np.mean(data)
    s = np.std(data) / np.sqrt(SAMPLE_SIZE)
    if LATEX:
        print(f"{d} & {np.sum(data != 0)} & {m:.6} & {s:.6} & {m:.6} $\pm$ {s:.6}\\\\")
    else:
        print(f"""Dimension: {d}
Number inside unit ball: {np.sum(data != 0)}
Mean of sample: {m}
Standard deviation of sample: {np.std(data)}
Standard error of sample: {s}
""")


# %%
