# %% imports
import numpy as np
np.set_printoptions(linewidth=1000)
# %% generate one set data
NUM_SIMULATIONS = 10000
DO_META_SIM = False

def gen_data():
    """
    Generate time data for each swimmer, 10000 times
    """
    # generate a time value using the distribution from the assignment
    t = lambda i: np.random.normal(100 + i/10, 1)

    # list of 10000 trials, with times for each racer 1-8
    base = np.tile(np.arange(1, 9), (NUM_SIMULATIONS, 1))
    return t(base)

def get_winshare(data_array):
    """
    Find the share of times each swimmer won
    """
    winners = np.argmin(data_array, axis=1)
    
    return np.bincount(winners) / len(winners)

win_share = get_winshare(gen_data())
print(f"Share of times Swimmer 1 won: {win_share[0]}")
print(f"Share of times Swimmer 8 won: {win_share[7]}")

# %% average out win percentages, over 1000 simulations
if (DO_META_SIM):
    NUM_META_SIM = 1000
    sums = np.zeros(8)
    for _ in range(NUM_META_SIM):
        sums += get_winshare()
    print("Meta simulation results:")
    print(sums / NUM_META_SIM)

# %% monte carlo for NCSU's expected score
# NCSU has the odd-numbered swimmers
data = gen_data()

# for our monte carlo simulation, we'll assign scores to each
# of the simulated races, and see the total scores per student
for row in data:
    scores = [0, 0, 0, 1, 2, 3, 4, 9]
    for score in scores:
        minidx = np.argmax(row)
        row[minidx] = score

score_per_swimmer = np.average(data, axis=0)

# sum up the odd numbered swimmer's expected scores
ncsu_score = 0.0
for idx in range(0, 8, 2):
    ncsu_score += score_per_swimmer[idx]

print(f"NCSU expected score: {ncsu_score}")
