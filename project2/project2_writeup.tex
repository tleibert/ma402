\documentclass{article}[10pt]
\usepackage{enumitem}
\usepackage{amsmath,amsfonts}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{minted}
\usepackage{caption}

\setlength{\parindent}{0pt}
\newcommand{\1}{\mathds{1}}
\newcommand{\E}{\mathbb{E}}

\begin{document}
	\title{Project \#2}
	\author{Trevor Leibert}	
	\date{\today}

	\maketitle
	\begin{center}
		I had no collaborators in the making of any part of this project.
	\end{center}
	\subsection*{Writeup (4 pts)}
	In general, your writeup for each part should include the following: 
	\begin{itemize}
		\item A {\it short} introduction explaning the nature and purpose of the assignment. 
		\item A brief description of your code or algorithms, if necessary. 
		\item A section for your plots/tables/figures, appropriately labeled and with further description if necessary. 
		\item A paragraph for your conclusions. What valuable life lessons did you draw from the assignment?
	\end{itemize}
	The aim is that a student who knows a little about the relevant material, but has not seen or worked on this assignment specifically, should be able to read your writeup and get a general idea of what you did and why. 
	
	Files submitted should include the following: 
	\begin{itemize}
		\item Your report (PDF/docx), which includes all relevant plots/tables/figures.
		\item Any code (.m/.py/.ipynb) used for your experiments.
	\end{itemize}

	\pagebreak
	\section*{Part 1 Writeup}
	\subsection*{Introduction}
		This writeup demonstrates the utility of the Law of Large Numbers and Monte Carlo techniques, especially when combined with a programming environment such as NumPy or Matlab. In this example, we simulate the results of a swim meet, where each swimmer's time is represented by an independent random Gaussian variable. The means of each distribution depend on the swimmer's number. Swimmer number one has the lowest mean, followed by two, up to eight. We can estimate their win percentages by looking at the average results over a large number of trials--an application of the loose law of large numbers.
	
	\subsection*{Python Code}
	My code can be found in the file \texttt{race\_simulation.py}. It can be run with just numpy installed via Python 3 on the command line. It, along with my other code, has ``cell'' comments that enable more matlab-like execution when using VSCode with the Python extension pack installed.
	\begin{minted}{Python}
# %% imports
import numpy as np
np.set_printoptions(linewidth=1000)
# %% generate one set data
NUM_SIMULATIONS = 10000
DO_META_SIM = False

def gen_data():
    """
    Generate time data for each swimmer, 10000 times
    """
    # generate a time value using the distribution from the assignment
    t = lambda i: np.random.normal(100 + i/10, 1)

    # list of 10000 trials, with times for each racer 1-8
    base = np.tile(np.arange(1, 9), (NUM_SIMULATIONS, 1))
    return t(base)

def get_winshare(data_array):
    """
    Find the share of times each swimmer won
    """
    winners = np.argmin(data_array, axis=1)
    
    return np.bincount(winners) / len(winners)

win_share = get_winshare(gen_data())
print(f"Share of times Swimmer 1 won: {win_share[0]}")
print(f"Share of times Swimmer 8 won: {win_share[7]}")

	\end{minted}
	The scoring for the bonus question was performed with the following algorithm:
	\begin{minted}{Python}
# %% monte carlo for NCSU's expected score
# NCSU has the odd-numbered swimmers
data = gen_data()

# for our monte carlo simulation, we'll assign scores to each
# of the simulated races, and see the total scores per student
for row in data:
    scores = [0, 0, 0, 1, 2, 3, 4, 9]
    for score in scores:
        minidx = np.argmax(row)
        row[minidx] = score
	\end{minted}
	This algorithm uses (maybe abuses) the fact that in this scenario the scores are all much lower than the times that are being scored to simply score each time in-place.\\\\
	Example outputs from both parts: \\
	\texttt{Share of times Swimmer 1 won: 0.2064\\
	Share of times Swimmer 8 won: 0.0637\\
	NCSU expected score: 10.0852}

	\subsection*{Results and Analysis}
	With the simulated race run 10,000 times, we can take the fraction of times that each racer won as an approximation of their real chances of winning the race.
	In my simulation, swimmer 1 won about 20\% of the time, while swimmer 8 won around 6.5\% of the time.
	I got this figure from running repeatedly and ``eyeballing'' the results. I then collected the results from 1,000 runs of this function, and averaged them out, so I could have a better result than just ``eyeballing.'' Swimmer 1 won about 20.3\% of the time, with swimmer 8 winning about 6.45\% of the time.\\
	The assumption that these variables are independent is not a perfect one by any means. With all of the swimmers in the same pool, their splashes and other disturbances to the water will influence eachothers' speed. The training for members across teams might also cause a certain group out of the 8 to perform differently than another group.\\
	This integral is problematic as there are eight variables to contend with. While double and triple integrals are fine and good in Calculus III, an 8-d integral would not be so fun.\\
	(Bonus) The Monte Carlo simulation code routinely produced results in the range of 9.5 to 10.2, from my testing. I'm not quite sure how to set up a confidence interval for it.
	
	\pagebreak
	
	\section*{Part 2 Writeup}
	\subsection*{Introduction}
	This part showcases the use of Monte Carlo techniques when estimating volume enclosed inside a multidimensional ``ball,'' which is defined as the set of points with 2-norm bounded by 1 for a given dimension.
	For two and three dimensions, this volume can be somewhat easily calculated with integration.
	However, integrals become quite tedious and computationally expensive as the number of dimensions/variables involved increases.
	We can get a good approximation of these variables with Monte Carlo, with a (comparatively) simpler implementation and much less computation.
	
	\subsection*{Python Code}
	The full Python code for this section can be found in the file \texttt{rare\_events.py}. All data was generated using this function:
	\begin{minted}{Python}
def run_experiment(num_trials, dimension):
	"""
	Samples the random variable X num_trials times 
	in the given dimension.
	Returns the sample.
	"""
	numbers = np.random.uniform(-1, 1, (num_trials, 2))
	selector = (np.linalg.norm(numbers, axis=1) <= 1)
	X = (2 ** dimension) * selector[:,np.newaxis]
	return X
	\end{minted}
	The function \texttt{run\_experiment} samples the random variable $X$ defined as 
	\[
		X = 2^d\1({\bf x}),
	\] where 
	\[
		\1({\bf x}) = \begin{cases}
			1 & \|{\bf x}\|_2 \leq 1, \\
			0 & \text{otherwise}. 
		\end{cases},
	\] giving back a sample with \texttt{num\_trials} entries from the given dimension.
	\pagebreak

	The following addition to the code was used to run the experiment for part (e):
	\begin{minted}{Python}
# %% part e
SAMPLE_SIZE = 1000000
# generate data
for d in range(2, 17):
	data = run_experiment(SAMPLE_SIZE, d)
	print(f"""Dimension: {d}
Number inside unit ball: {np.sum(data != 0)}
Mean of sample: {np.mean(data)}
Standard deviation of sample: {np.std(data)}
Standard error of sample: {np.std(data) / np.sqrt(SAMPLE_SIZE)}
""")
	\end{minted}

	\pagebreak
	\subsection*{Results}
	(a) Example output approximating the value of $\E[X]$ with sample sizes 10, 50, and 500:\\
	\texttt{
		Mean/Approximate Expected Value from 10 trials: 2.8\\
		Mean/Approximate Expected Value from 50 trials: 3.2\\
		Mean/Approximate Expected Value from 500 trials: 3.136
	}\\\\
	(c) Example output approximating the mean and standard deviation:\\
	\texttt{
		Sample mean: 3.144\\
		Standard Deviation in sample: 1.64051\\
		Standard Error in sample: 0.07336571406317804
	}\\
	In the requested form of $\overline{\mu}\pm 1.96s$:
	\[
		3.144 \pm 0.143797
	\]
	The standard error was calculated by dividing the standard deviation by the square root of the sample size.
	\\
	\\
	(e) The results from one run of the aforementioned code for part e are collected in the table below:
	\begin{center}
		\begin{tabular}{c|r|r|r|r}
			$d$ & \# inside(out of 1,000,000) & sample mean & std err & interval \\
			\hline
			2 & 785692 & 3.14277 & 0.00164137 & 3.14277 $\pm$ 0.00164137\\
			3 & 785984 & 6.28787 & 0.0032811 & 6.28787 $\pm$ 0.0032811\\
			4 & 785066 & 12.5611 & 0.00657242 & 12.5611 $\pm$ 0.00657242\\
			5 & 785431 & 25.1338 & 0.0131367 & 25.1338 $\pm$ 0.0131367\\
			6 & 784876 & 50.2321 & 0.0262981 & 50.2321 $\pm$ 0.0262981\\
			7 & 784964 & 100.475 & 0.0525885 & 100.475 $\pm$ 0.0525885\\
			8 & 785136 & 200.995 & 0.105146 & 200.995 $\pm$ 0.105146\\
			9 & 786255 & 402.563 & 0.209894 & 402.563 $\pm$ 0.209894\\
			10 & 785525 & 804.378 & 0.420309 & 804.378 $\pm$ 0.420309\\
			11 & 785171 & 1608.03 & 0.841121 & 1608.03 $\pm$ 0.841121\\
			12 & 784614 & 3213.78 & 1.68382 & 3213.78 $\pm$ 1.68382\\
			13 & 785069 & 6431.29 & 3.36506 & 6431.29 $\pm$ 3.36506\\
			14 & 784901 & 12859.8 & 6.73204 & 12859.8 $\pm$ 6.73204\\
			15 & 784700 & 25713.0 & 13.4686 & 25713.0 $\pm$ 13.4686\\
			16 & 784863 & 51436.8 & 26.9299 & 51436.8 $\pm$ 26.9299\\
			\hline
		\end{tabular}
	\end{center}
	\subsection*{Analysis}
	(b) $\E[X]$ should be equal to the area of the circle as it represents the fraction of points generated that fall inside the circle. The points are generated in a square/rectangle pattern with $x$ and $y$ ranging from -1 to 1. If we multiply the area of this square by the fraction of its area that is enclosed by the ball, we get the area of the ball. This effect is demonstrated with the below figure:
	\begin{minipage}{\linewidth}
		\centering
		\includegraphics[scale=0.5]{circle.png}
		\captionof{figure}{500 i.i.d. uniform random points, with those inside the bound in blue, and those outside in red.}
	\end{minipage}\\\\
	(c) The area of the unit circle is obviously $\pi$. This interval of $3.144 \pm 0.143797$ contains $\pi$. In fact, the sample mean is \emph{very} close to $\pi$, within 0.0766\%.\\\\
	(d) The probability of the true value for the area circle being contained in the interval can be found through use of the central limit theorem, which states that as many independent random variables are added, the mean will tend towards a normal distribution.
	Therefore we can use the CDF of the normal distribution to find out the exact probability that the sum is enclosed in this interval. This can be done in python with the following code:
	\begin{minted}{Python}
from scipy.stats import norm
prob = norm.cdf(1.96) - norm.cdf(-1.96)
print(prob)
	\end{minted}
	The value returned by this operation is 0.95.
	Therefore, this interval is a 95\% confidence interval for the value of the area of the circle.
	So we can say that there is a 95\% chance that the value for the area of the circle will be enclosed inside of this range, whenever we run the code that generates these values.\\\\
	(f) As the number of dimensions increases, the standard error of the mean increases.
	It is increasing exponentially, doubling every time the number of dimensions goes up. This means that our basic Monte Carlo techniques will require \emph{vastly} more trials for each increase in dimension.
	This will quickly become infeasible, due to the standard error being the standard deviation of the sample divided by the \emph{square root} of the sample size.
	The sample size will increase by a \emph{massive} factor for every increase to dimension if we want to keep the confidence interval small.
	\pagebreak


	\section*{Part 3 Writeup}
	\subsection*{Introduction}
	The purpose of this project is to estimate the area enclosed inside of a complex polar curve, whose integral would be very painful, if not impossible, to do by hand. This curve is given by the equation
	\[
		r(\theta) = \left(\frac{100}{100 + (\theta -\pi/2)^8}\right)\cdot \left(2 - \sin(7\theta) + \cos(30\cdot\theta)/2\right), \quad \theta \in [-\pi/2, 3\pi/2].
	\]
	and generates the leaf seen below:
	\begin{figure}[ht]
		\begin{center}
			\includegraphics[width=8cm]{leaf.png}
		\end{center}
		\caption{Plot of $r(\theta)$}\label{fig:leaf}
	\end{figure}

	\subsection*{Python Code}
	The full code for this part can be found in the file \texttt{leaf\_area.py}.
	Implementation of $r(\theta)$ in Python is straightforward with direct imports:

	\begin{minted}{Python}
from numpy import pi, sin, cos

# %% generate data
# define r(o): o looks like theta
r = lambda o: (100 / (100 + (o-pi/2)**8)) * (2-sin(7*o) + cos(30*o)/2)
	\end{minted}
\pagebreak
This function was applied to estimate the expected value of the function for part (b):
	\begin{minted}{Python}
# %% part b
for num in [10, 50, 500]:
T = np.random.uniform(-pi / 2, 3 * pi / 2, num)
X = pi * r(T) ** 2
print(f"""Size of sample: {num}
Mean of sample: {np.mean(X)}
Standard deviation of sample: {np.std(X)}
Standard error of sample: {np.std(X) / np.sqrt(len(X))}
""")
	\end{minted}
And to establish the interval for part (c):
	\begin{minted}{Python}
# %% part c
NUM_SAMPLES = 500
T = np.random.uniform(-pi / 2, 3 * pi / 2, NUM_SAMPLES)
Xs = pi * r(T) ** 2
Xbar = np.mean(Xs)
std_dev = np.std(Xs)
std_err = std_dev / np.sqrt(NUM_SAMPLES)
print(f""""Size of sample: {NUM_SAMPLES}
Sample mean: {Xbar}
Sample standard deviation: {std_dev}
Sample standard error: {std_err}
""")
	\end{minted}
Adding the control variate for part (e):
	\begin{minted}{Python}
# %% part e
# define control variate
NUM_SAMPLES = 500
rbar = lambda o: 1 + sin(o) - sin(7*o)/2
T = np.random.uniform(-pi / 2, 3 * pi / 2, NUM_SAMPLES)
X = pi * r(T) ** 2
Y = pi * rbar(T) ** 2
# integrate rbar to find the real expected value
EY = 5.10509
a = -1
Z = X + a * (Y - EY)
print(f"""Size of sample: {NUM_SAMPLES}
Sample mean: {np.mean(Z)}
Sample standard deviation: {np.std(Z)}
Sample standard error: {np.std(Z) / np.sqrt(NUM_SAMPLES)}
Range: {np.mean(Z):.7} +- {1.96 * np.std(Z) / np.sqrt(NUM_SAMPLES):.7}
""")
	\end{minted}
	\subsection*{Results}
	(b) The results of each trial for part (b) are below:
	\begin{center}
		\begin{tabular}{c | c}
			Number of Trials & Estimate of $\E[X]$ \\
			\hline
			10 & 11.949021\\ 
			50 &  8.149654\\
			500 & 7.806539\\
			\hline
		\end{tabular}
	\end{center}
	(c) Range of $\mu \pm 1.96s$:
	\[
		7.054781 \pm 0.796455
	\]
	(e) Range of $\mu \pm 1.96s$ \emph{with} control variate $\tilde r(\theta)$:
	\[
		7.210445 \pm 0.448933
	\]
	\subsection*{Analysis}
	(a) $\E[X]$ should be equal to the area of $L$ because of its relation of the expected value of any function of a random variable to the integral of said function evaluated from $-\infty$ to $\infty$. This can be adapted for use over a definite bound. From the lecture 8 notes:
	\begin{align*}
		\E[g(X)] &= \int_{-\infty}^\infty g(x)F_X(x) \, dx \\
		\text{When X follows a uniform distribu}&\text{tion, evaluated over a definite bound:} \\
		\E[g(X)] &= \int_{x_1}^{x_2} g(x) \,dx \\
		\text{Where:}\\
		X&\sim \mathcal{U}(x_1, x_2)
	\end{align*} 
	
	(c) The interval $7.054781 \pm 0.796455$ does contain the true value of the area of the leaf, 7.287285. The mean is within 3.19\% of the true value, which is well within one standard deviation from the mean.\\\\
	(e) When adding a control variate to decrease the errors in the estimate, the following function was used as the variate:
	\[
		\tilde r(\theta) = 1 + \sin(\theta) - \sin(7\theta)/2
	\]
	The random variable $Y$ was defined as:
	\[
		Y = \pi\tilde r(T)^2,
	\]
	with the same definition for $T$ as earlier.
	By evaluating the following integral (using Wolfram Alpha) we can get the area of this shape, which is by definition $\E[Y]$:
	\[
		\E[Y] = \int_{\pi/2}^{3\pi/2} \frac{\tilde r(\theta)^2}{2}\,d\theta \approx 5.10509.
	\]
	Using this new function and its known expected value, we can sample $Z$, where $Z = X + \alpha(Y - \E[Y])$.
	This data was collected with $\alpha$ = -1.
	We are effectively using the errors in Y, given by the expression $(Y - \E[Y])$ to cancel parts of the errors in $X$.
	The range given by this experiment, $7.210445 \pm 0.448933$, is narrower than the one in part (c), almost by a factor of two.
	This means that $\tilde r(\theta)$ is a good approximation of $r(\theta)$, and there is optimization that can be done to find the value of $\alpha$ that produces the smallest standard error.
	This method of using control variates could be useful in controlling errors for high-dimension problems like those found in Part 2.

	\begin{figure}[ht]
		\begin{center}
			\includegraphics[width=8cm]{leaf2.png}
		\end{center}
		\caption{The control variate $\tilde r(\theta)$ plotted over the original function $r(\theta)$.} \label{fig:leaf2}
	\end{figure}
	(d) We can show that $\E[Z] = \E[X]$, with a little help from Example Project 2:
	\begin{align*}
		\E[Z] &= \E[X + \alpha(Y - \E[Y])] \\
		\text{X and Y}&\text{ are independent...} \\
		\E[Z] &= \E[X] + \alpha\E[Y - \E[Y]] \\
		\E[Z] &= \E[X] + \alpha(\E[Y] - \E[\E[Y]]) \\
		\E[Y]\text{ is }&\text{constant, as it is the area of the shape.} \\
		\E[Z] &= \E[X] + \alpha(\E[Y] - \E[Y]) \\
		\E[Z] &= \E[X] + 0 \\
		\E[Z] &= \E[X]
	\end{align*}
	\pagebreak

\end{document}