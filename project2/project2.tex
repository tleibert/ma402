\documentclass{article}[10pt]
\usepackage{enumitem}
\usepackage{amsmath,amsfonts}
\usepackage{graphicx}
\usepackage{dsfont}
\setlength{\parindent}{0pt}
\newcommand{\1}{\mathds{1}}


\begin{document}
	\title{Project \#2}
	\author{MA 402 \\ Mathematics of Scientific Computing}	
	\date{Due: Wed, March 10}

	\maketitle
	
	\subsection*{Writeup (4 pts)}
	In general, your writeup for each part should include the following: 
	\begin{itemize}
		\item A {\it short} introduction explaning the nature and purpose of the assignment. 
		\item A brief description of your code or algorithms, if necessary. 
		\item A section for your plots/tables/figures, appropriately labeled and with further description if necessary. 
		\item A paragraph for your conclusions. What valuable life lessons did you draw from the assignment?
	\end{itemize}
	The aim is that a student who knows a little about the relevant material, but has not seen or worked on this assignment specifically, should be able to read your writeup and get a general idea of what you did and why. 
	
	Files submitted should include the following: 
	\begin{itemize}
		\item Your report (PDF/docx), which includes all relevant plots/tables/figures.
		\item Any code (.m/.py/.ipynb) used for your experiments.
	\end{itemize}

	

	\pagebreak
	
	\subsection*{Part 1: Race Simulation (3 pts)}
	Four NCSU swimmers are competing in a race against four Indiana swimmers. Their times can be modeled by the independent random Gaussian variables 
	\begin{align*}
		X_i &\sim \mathcal{N}(\mu_i, 1), \\
		\mu_i &= 100 + i/10,
	\end{align*}
	for $1\leq i \leq 8$. 
	\begin{enumerate}[label=\alph*)]
		\item (2 pts) Run 10000 simulations. What is the approximate probability that Swimmer 1 wins the race? What is the approximate probability that Swimmer 8 wins the race? 
		\item (0.5 pts) Why might the assumption that the variables are independent be problematic? 
		\item (0.5 pts) Suppose that odd-numbered swimmers are from NCSU and even-numbered swimmers are from Indiana, and that the dual meet uses 9-4-3-2-1 scoring. You would like to know NCSU's expected total score for the event. In theory, this could be done by setting up and solving an integral. Why is this option unattractive? 
		\item (Bonus) Use Monte Carlo to estimate NCSU's expected score for the event. 
	\end{enumerate}
	
	
	\pagebreak
	
	
	\subsection*{Part 2: Rare Events (8 pts)}
	Define the unit $d$-ball as the set of points in $\mathbb{R}^d$ with 2-norm bounded by 1. It is known that the volume of the unit ball in $d$ dimensions is given by the formula 
	\[
		V_d = \frac{\pi^{d/2}}{\Gamma(d/2 + 1)},
	\]
	where $\Gamma$ represents the gamma function (\texttt{gamma} in Matlab). In this experiment, you will use Monte Carlo methods to estimate the volume of the ball in various dimensions. 
	
	\begin{enumerate}[label=\alph*)]
		\item (1 pt) Start with the unit circle: generate random vectors ${\bf x}\in \mathbb{R}^2$ whose entries are i.i.d.~samples from $\mathcal{U}(-1,1)$. Define the random variable 
		\[
			X = 2^d\1({\bf x}),
		\] where 
		\[
			\1({\bf x}) = \begin{cases}
				1 & \|{\bf x}\|_2 \leq 1, \\
				0 & \text{otherwise}. 
			\end{cases}
		\]
		Estimate $\mathbb{E}[X]$ using $10$, $50$, and $500$ trials. 
		\item (1 pt) Explain: Why should $\mathbb{E}[X]$ be equal to the area of the circle? 
		\item (2 pts) Define the random variable $\overline{X}_N = \frac{1}{N}\sum_{i=1}^N X_i$, where $X_1,\ldots,X_N$ are i.i.d.~samples from the same distribution as $X$. With $N=500$, use the results of your trials to estimate
		\begin{itemize}[label={--}]
			\item  $\sigma[X]$, the standard deviation of $X$. You can do this using the function \texttt{std} or \texttt{numpy.std}.  
			\item  $\sigma[\overline{X}_{500}]$, using your estimate of $\sigma[X]$, the relation between $\overline{X}_{500}$ and $X$, and the properties of the variance. This estimate is commonly known as the {\bf standard error}. 
		\end{itemize}
		Report your estimate of the area of the circle in the form $\overline{\mu} \pm 1.96s$, where $\overline{\mu}$ is your estimate of $\mathbb{E}[X]$ and $s$ is the standard error. How do your results compare to the known area of the circle? 
		\item (1 pt) What is the approximate probability that the interval $\overline{\mu}\pm 1.96s$ will contain the true area of the circle? What theorem allows you to draw this conclusion? 
		\item \label{regular} (2 pts) Repeat the experiment for $2\leq d\leq 16$ with $10^6$ trials for each dimension. For each value of $d$, report the total number of points ${\bf x}$ that landed within the unit ball, as well as the corresponding estimate of the volume $\overline{\mu}\pm 1.96s$. 
		\item (1 pt) Comment on the behavior of the results. What starts happening as the number of dimensions increases? Why is this a problem for our standard Monte Carlo techniques? 
	\end{enumerate}
	
	\pagebreak
		
	\subsection*{Part 3: Control Variates (5 pts)}
	Consider the maple leaf\footnote{\texttt{https://math.stackexchange.com/questions/1587947/the-plot-of-a-leaf}} generated by the polar curve
	\[
		r(\theta) = \left(\frac{100}{100 + (\theta -\pi/2)^8}\right)\cdot \left(2 - \sin(7\theta) + \cos(30\cdot\theta)/2\right), \quad \theta \in [-\pi/2, 3\pi/2].
	\]
	The area of this leaf (Figure \ref{fig:leaf}) is approximately 7.287285. Here you will use Monte Carlo techniques to estimate the integral 
	\[
		\text{area}(L) = \int_{\theta=-\pi/2}^{3\pi/2}\frac{r(\theta)^2}{2}\,d\theta.
	\]
	This integral is equal to the area of $L$ (the set of points inside the leaf) since it uses the formula for the area enclosed by a polar curve. 
			\begin{figure}[ht]
				\begin{center}
					\includegraphics[width=8cm]{leaf.png}
				\end{center}
				\caption{A pretty leaf.}\label{fig:leaf}
			\end{figure}
	
	\begin{enumerate}[label=\alph*)]
		\item (1 pt) Let $T\sim \mathcal{U}(-\pi/2, 3\pi/2)$ and define $X = \pi r(T)^2$. Explain: Why should $\mathbb{E}[X]$ be equal to the area of $L$? 
	
		\item (1 pt) Generate random points $T_1,\ldots, T_N$ and estimate $\mathbb{E}[X]$ for $N = 10,50,500$. 
		
		\item (1.5 pts) In the same manner as step (c) of the previous part, use $N = 500$ and estimate $\sigma[X]$ and $\sigma[\overline{X}_{500}]$. Report your estimate in the form $\overline{\mu}\pm 1.96s$ as before. How do your results compare to the known area of $L$? 
		
		\item (0.5 pts) Now try to improve the estimate by introducing a control variate. The shape enclosed by the polar function $\tilde{r}(\theta) = 1 + \sin(\theta) - \sin(7\theta)/2$ has an area\footnote{$\tilde{r}(\theta)$ is sometimes negative, so take this notion of area with a grain of salt.} of $13\pi/8$ and appears to approximate the shape of the leaf decently well. Let $Y = \pi\tilde{r}(T)^2$, and let $Z = X + \alpha(Y - \mathbb{E}[Y])$, where $\alpha = -1$ and $\mathbb{E}[Y]$ is the area of the approximating shape. Why is the equality $\mathbb{E}[Z] = \mathbb{E}[X]$ true? 
		
		\item (1 pt) Estimate $\mathbb{E}[Z]$ using 500 trials. Also estimate $\sigma[\overline{Z}_{500}]$. How do your results compare to your other estimates? 
	
		\item (Bonus) What value of $\alpha$ tends to give you the smallest standard error? 

		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=8cm]{leaf2.png}
			\end{center}
			\caption{A simpler shape that approximates the shape of the leaf.} \label{fig:leaf2}
		\end{figure}
		
	
	\end{enumerate}
			
			

	

\end{document}