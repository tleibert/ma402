# %% imports
import numpy as np
from numpy import pi, sin, cos
import matplotlib.pyplot as plt

# %% generate data
# define r: python unicode for the win
r = lambda o: (100 / (100 + (o - pi/2) ** 8)) * (2 - sin(7 * o) + cos(30 * o)/2)

# %% part b
for num in [10, 50, 500]:
    T = np.random.uniform(-pi / 2, 3 * pi / 2, num)
    X = pi * r(T) ** 2
    print(f"""Size of sample: {num}
Mean of sample: {np.mean(X)}
Standard deviation of sample: {np.std(X)}
Standard error of sample: {np.std(X) / np.sqrt(len(X))}
""")

# %% part c
NUM_SAMPLES = 500
T = np.random.uniform(-pi / 2, 3 * pi / 2, NUM_SAMPLES)
Xs = pi * r(T) ** 2
Xbar = np.mean(Xs)
std_dev = np.std(Xs)
std_err = std_dev / np.sqrt(NUM_SAMPLES)
print(f""""Size of sample: {NUM_SAMPLES}
Sample mean: {Xbar}
Sample standard deviation: {std_dev}
Sample standard error: {std_err}
""")

# %% part e
# define control variate
NUM_SAMPLES = 500
rbar = lambda o: 1 + sin(o) - sin(7*o)/2
T = np.random.uniform(-pi / 2, 3 * pi / 2, NUM_SAMPLES)
X = pi * r(T) ** 2
Y = pi * rbar(T) ** 2
# integrate rbar to find the real expected value
EY = 5.10509
a = -1
Z = X + a * (Y - EY)
print(f"""Size of sample: {NUM_SAMPLES}
Sample mean: {np.mean(Z)}
Sample standard deviation: {np.std(Z)}
Sample standard error: {np.std(Z) / np.sqrt(NUM_SAMPLES)}
Range: {np.mean(Z):.7} +- {1.96 * np.std(Z) / np.sqrt(NUM_SAMPLES):.7}
""")

# %% graph to check
graph = False
if graph:
    o = np.linspace(-pi/2, 3*pi/2, 5000)
    plt.polar(o, r(o))
# %%
