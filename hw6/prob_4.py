# %% imports
import numpy as np

# %% implement cluster algorithm
def cluster_iter(points, means):
    idxlist = [
        np.argmin([np.linalg.norm(point - mean, 2) for mean in means])
        for point in points
    ]
    list1 = []
    list2 = []
    for idx, point in enumerate(points):
        if idxlist[idx] == 0:
            list1.append(point)
        else:
            list2.append(point)
    l1 = np.array(list1)
    l2 = np.array(list2)

    return np.array([np.mean(l1, axis=0), np.mean(l2, axis=0)]), np.array(idxlist)


# %% set up data
xs = np.array([[0, 0],
               [0, 1],
               [2, 1],
               [2, 0]])

means = np.array([[-1, -1],
                  [1, 2]])

print("Means when starting:")
print("Note vectors are VERTICAL here")
print(means.T)
# %% cluster time
means, assignments = cluster_iter(xs, means)
print("Means after one iteration:")
print(means.T)
print("Assignments after one iteration:")
print(assignments)

# %% iteration 2
means, assignments = cluster_iter(xs, means)
print("Means after two iterations:")
print(means.T)
print("Assignments after two iterations:")
print(assignments)

# %% iteration 3 -- it seems to have converged
means, assignments = cluster_iter(xs, means)
print("Means after three iterations:")
print(means.T)
print("Assignments after three iterations:")
print(assignments)
# %%
