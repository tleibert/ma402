# %% imports
from scipy.io import loadmat
import numpy as np
from scipy.cluster.vq import kmeans2

# %% load data
jokeData = loadmat("jokeData.mat")
J = jokeData["J"]
jokeTexts = jokeData["jokeTexts"]
# %% define related jokes
# I picked jokes that had something to do with cows
cow_jokes = np.array([22, 23, 25, 34])
cow_jokes -= 1  # adjust jokes for indexing

cow_joke_scores = J[:, cow_jokes]

K_cow = np.cov(cow_joke_scores, bias=True)
u, s, vt = np.linalg.svd(K_cow)
print("Largest three singluar values of K for cow-themed jokes:")
print(s[:3])

# %%
unrelated_jokes = np.array([0, 4, 12, 29])  # four "random" jokes
unrelated_joke_scores = J[:, unrelated_jokes]

K_unrelated = np.cov(unrelated_joke_scores, bias=True)
u, s, vt = np.linalg.svd(K_unrelated)
print()
print("Largest three singular values of K for unrelated jokes:")
print(s[:3])

# %%
