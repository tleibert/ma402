# %% imports
import numpy as np

# %% setup
x1 = [1, 2, 3, 4, 5, 6]
x2 = [1, 1, 1, 2, 2, 3]

# %% part a
print(f"E[X1]: {np.mean(x1)}")
print(f"E[X2]: {np.mean(x2)}")
print(f"Var[X1]: {np.var(x1)}")
print(f"Var[X2]: {np.var(x2)}")
# %% part b
k = np.cov(x1, x2, bias=True)  # bias=True calculates population covariance
print(f"Cov(X1, X2): {k[0][1]}")

# %% part c
x = [x1, x2]
print(f"Covariance matrix:\n{np.cov(x, bias=True)}")

# %% part d
# cov_mat is the same result as np.cov(x)
u, s, vt = np.linalg.svd(k)
v = vt.T
# since cov_mat is symmetric, u and v are the same matrix
print("u\n", u, "\n\nv\n", v)

# %% part e
c = np.corrcoef(x1, x2)
print("Correlation matrix:\n", c)

# %%
