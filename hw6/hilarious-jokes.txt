A dog walks into Western Union and asks the clerk to send a telegram. He fills out a form on which he writes down the telegram he wishes to send: "Bow wow wow, Bow wow wow."  The clerk says, "You can add another 'Bow wow' for the same price."  The dog responded, "Now wouldn't that sound a little silly?"
Q: If a person who speaks three languages is called "tri-lingual," and a person who speaks two languages is called "bi-lingual," what do call a person who only speaks one language?  A: American!
Q: What's orange and sounds like a parrot? A: A carrot
Q: What is the Australian word for a boomerang that won't come back?   A: A stick
A horse walks into a bar. Bartender says: "So, why the long face?"
An engineer, a physicist and a mathematician are sleeping in a room. There is a fire in the room. The engineer wakes up, sees the fire, picks up the bucket of water and douses the fire and goes back to sleep.   Again there is fire in the room. This time, the physicist wakes up, notices the bucket, fills it with water, calculates the optimal trajectory and douses the fire in minimum amount of water and goes back to sleep.  Again there is fire. This time the mathematician wakes up.  He looks at the fire, looks at the bucket and the water and exclaims, "A solution exists" and goes back to sleep.
Employer to applicant: "In this job we need someone who is responsible."  Applicant: "I'm the one you want. On my last job, every time anything went wrong, they said I was responsible."
Why are there so many Smiths in the phone book? Because they all have phones.
How many teddybears does it take to change a lightbulb?  It takes only one teddybear, but it takes a whole lot of lightbulbs.
A neutron walks into a bar and orders a drink. "How much do I owe you?" the neutron asks.  The bartender replies, "for you, no charge."
Two atoms are walking down the street when one  atom says to the other  "Oh, my! I've lost an electron!"  The second atom says"Are you sure"  The first replies "I'm positive!"
Two attorneys went into a diner and ordered two drinks.  Then they produced  sandwiches from their briefcases and started to eat.  The owner became quite concerned and marched over and told them, "You can't eat your own sandwiches in here!"  The attorneys looked at each other, shrugged their shoulders and then exchanged sandwiches.
Nurse: Doctor, Doctor, there's an invisible man in the waiting room! Doctor: Well, go in there and tell him that I can't see him!
Sherlock Holmes and Dr. Watson go on a camping trip, set up their tent, and fall asleep. Some hours later, Holmes wakes his faithful friend. "Watson, look up at the sky and tell me what you see." Watson replies, "I see millions of stars." "What does that tell you?" Watson ponders for a minute. "Astronomically speaking, it tells me that there are millions of galaxies and potentially billions of planets. Astrologically, it tells me that Saturn is in Leo. Timewise, it appears to be approximately a quarter past three. Meteorologically, it seems we will have a beautiful day tomorrow. What does it tell you?" Holmes is silent for a moment, then speaks. "Watson, you idiot, someone has stolen our tent."
A man walks into a bar and says "ouch!"
Q: What's the best thing about Switzerland? A: I don't know, but the flag is a big plus!
Q: Why did the toadstool go to the party? A: Because it was a fun guy! Q: Why did it leave? A: Because there wasn't mushroom! 
Q: How did the elephant get up in the oak tree? A: It sat on an acorn and waited. 
Q: Why did the elephant paint its toenails red? A: To hide in the cherry tree. Q: Have you ever seen an elephant in a cherry tree? A: ...so it works! 
A mathematician, physicist, and engineer are asked to measure the volume of a red rubber ball. The mathematician measures the circumference with a string, then computes the volume. The physicist puts the ball in a beaker of water and measures the displacement. The engineer weighs the ball, then looks up the conversion formula in a Red Rubber Ball table. 
Q: How do you get an elephant in a refrigerator? A: Open the door, put in the elephant, and close the door. 
Milk production at a dairy farm was low, so the farmer wrote to the local university asking for help. A physicist comes to the farm for an intensive on-site investigation, and returns to the univeristy to evaluate the data. The physicist soon returns to the farm, saying to the farmer "I have the solution, but it works only in the case of spherical cows in a vacuum".
Q: What do you call a cow with only three legs? A: Tri-tip
Patron: Waiter, what's this fly doing in my soup?! Waiter: It seems to be doing the backstroke, sir. 
Did you hear about the cow that won an academic award? It was outstanding in its field. 
Q: How do you catch a unique bird? A: Unique up on it. 
There once was a man/from cork, who got limericks/and haikus confused
There once was a man from Japan/whose poetry never would scan/When someone asked why/he replied, with a sigh/"It's because I always try to fit as many words into the last line as I possibly can."
There once was a man from Peru/who dreamed he was eating his shoe/He woke up in the night/with a terrible fright/and found it was perfectly true! 
Q: What's red and smells like blue paint? A: Red paint. 
Q: Why was 6 afraid of 7? A: Because 7 8 9! 
Q: What do an eagle and a mole have in common? A: They both live underground. Apart from the eagle. 
A duck walks into a bar. Animal control is promptly called and the duck is released in a nearby park. 
Two cows are in a field. Suddenly, from behind a bush, a rabbit leaps out and runs away. One cow looks around a bit, eats some grass, and then wanders off. 
What's the difference between a piano and a fish? A: You can tune a piano but you can't tuna fish!
Haikus are easy/But sometimes they don't make sense/Refrigerator
Q: What did the duck do when it flew upside down? A: It quacked up! 
Radio Yerevan was asked: "Is it correct that Grigori Grigorievich Grigoriev won a luxury car at the All-Union Championship in Moscow?"/ Radio Yerevan answered: "In principle, yes. But first of all it was not Grigori Grigorievich Grigoriev, but Vassili Vassilievich Vassiliev; second, it was not at the All-Union Championship in Moscow, but at a Collective Farm Sports Festival in Smolensk; third, it was not a car, but a bicycle; and fourth he didn't win it, but rather it was stolen from him."
Joe: What's wrong, Mort? Mort: Last night, I dreamed I was eating a giant marshmallow? Joe: So? Mort: So when I woke up this morning, my pillow was gone! 
Q: What's the difference between a duck? A: One leg is both the same.
The three most common issues in Matlab are vectorization and off-by-one errors. 
Q: Why do ducks have flat feet? A: For stamping out forest fires. Q: Why do elephants have flat feet? A: For stamping out flaming ducks. 