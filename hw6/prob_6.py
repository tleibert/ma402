# %% imports
from numpy.core.fromnumeric import size
from scipy.io import loadmat
from scipy.cluster.vq import kmeans2
import numpy as np
import matplotlib.pyplot as plt

# %% setup
hw6 = loadmat("hw6.mat")
x = hw6["X"]
x_shape = np.shape(x)
flat_x = x.flatten()

# %% part a -- group data into 4 clusters
centroids, idxs = kmeans2(flat_x, 4)
compressed_x = np.reshape(centroids[idxs], x_shape)

# %% part b -- plot histogram
fig, ax = plt.subplots()
ax.hist(flat_x, 20)
for centroid in centroids:
    if centroid == centroids[0]:
        ax.axvline(centroid, color="r", linestyle="--", label="cluster center")
    else:
        ax.axvline(centroid, color="r", linestyle="--")

ax.set_xlabel("Color value (grouped by 0.05)", size=12)
ax.set_ylabel("Number of occurances", size=12)
ax.legend()
ax.set_title("Color values and their K-means determined clusters", size=15)

save = False
if save:
    plt.savefig("prob_6_hist.png", dpi=300)
# %% part c -- show figures
fig, axs = plt.subplots(1, 2, figsize=(8, 3))

captions = ["Original image", "Compressed image"]
for idx, image in enumerate([x, compressed_x]):
    axs[idx].imshow(image, cmap="gray")
    axs[idx].set_xticks([])
    axs[idx].set_yticks([])
    axs[idx].set_title(captions[idx])

save = False
if save:
    plt.savefig("prob_6_clown.png", dpi=300)
# %%
