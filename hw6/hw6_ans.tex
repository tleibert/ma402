\documentclass{article}[10pt]

% PACKAGES AND DEFINITIONS
\usepackage{amsmath,amsthm,amssymb}
\usepackage{graphicx,enumitem,multicol}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{dsfont}
\usepackage{float,graphicx}

\theoremstyle{definition}
\newtheorem{problem}{Problem}
\specialcomment{solution}{{\bf Solution: }}{}
\setlength{\parindent}{0pt}

\newcommand{\x}{{\bf x}}
\newcommand{\y}{{\bf y}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\1}{\mathds{1}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\unif}{\mathcal{U}}
\newcommand{\inv}{^{-1}}
\newcommand{\ts}{^{T}}
\newcommand{\pinv}{^{\dagger}}

\DeclareMathOperator*{\var}{Var}
\DeclareMathOperator*{\prob}{Pr}
\DeclareMathOperator*{\Ber}{Ber}

%\excludecomment{solution}

\begin{document}
	
	
\title{Homework \#6}
\author{Trevor Leibert}	
\date{\today}

\maketitle

{\bf UPDATE} (4/14/21): Choose ONE of Problems 5 or 6 to solve. You may do both if you wish, but only one is necessary. 


\begin{problem}[Covariance Matrix I]
	Roll a standard six-sided die, and let $X_1$ be the number rolled. Define $X_2$ by 
	\[
		X_2 = \begin{cases}
			1 & X_1 \in \{1,2,3\}, \\
			2 & X_1 \in \{4,5\}, \\
			3 & X_1 = 6. 
		\end{cases}
	\]
	\begin{enumerate}[label=\alph*)]
		\item Compute $\mathbb{E}[X_1]$, $\mathbb{E}[X_2]$, $\text{Var}[X_1]$, and $\text{Var}[X_2]$. 
		\item Compute $\text{Cov}(X_1,X_2)$. 
		\item If ${\bf X} = [X_1; X_2]$, what is the covariance matrix $K_{{\bf XX}}$? 
		\item What are the singular vectors of $K_{{\bf XX}}$? What do these vectors represent in terms of $X_1$ and $X_2$? 
		\item What is the correlation matrix determined by ${\bf X}$? [This is a matrix $C$ where $C(i,j) = \rho(X_i,X_j)$.]
	\end{enumerate}
\end{problem}

\begin{solution}
	I did all the math using Python/NumPy: my work is in the file \texttt{prob\_1.py}. Note: all fractions are my best guess at what the decimals returned by the code represent, though I'm pretty confident in them.
	\begin{enumerate}[label=\alph*)]
		\item \begin{align*}
			\E[X_1] &= 3.5 \\
			\E[X_2] &= \frac{5}{3} \\
			\text{Var}[X_1] &= 2.9167 = \frac{35}{12}\\
			\text{Var}[X_2] &= 0.5556 = \frac{5}{9}
		\end{align*}
		\item
		\[
			\text{Cov}(X_1, X_2) = 1.16667 \approx \frac{7}{6}
		\]
		\item \[
			K = \begin{bmatrix}
				2.91667 & 1.16667 \\ 1.16667 & 0.55556
			\end{bmatrix}
		\]
		\item Note that because {\bf X} is symmetric, {\bf U} and {\bf V} are the same\[
			{\bf U}_1 = \begin{bmatrix}
				-0.92501 \\ -0.37995
			\end{bmatrix} \,\,\, {\bf U}_2 = \begin{bmatrix}
				-0.37995 \\ 0.92501
			\end{bmatrix}
		\]
		I don't really have any idea what these vectors represent in terms of $X_1$ or $X_2$. It might have something to do with maximizing the variance?
		\item \[ C =
			\begin{bmatrix}
				1 & 0.91652 \\ 0.91652 & 1
			\end{bmatrix}
		\]
	\end{enumerate}
\end{solution}

\pagebreak

\begin{problem}[Covariance Matrix II]
	You have 100 dollars to invest in stocks in one of 2 companies: the Safety Company and the Danger Company. In terms of the percentage rate of return, each company can be modeled as a normal random variable: $S\sim \mathcal{N}(4,4)$ and $D\sim \mathcal{N}(4,9)$ (i.e. expected values 4 and respective variances 4 and 9). Since these two companies have the same expected rate of return, you would like to spread out your money in order to minimize the variance. 
	\begin{enumerate}[label=\alph*)]
		\item Suppose $S$ and $D$ are independent. How much money should you put in each company? 
		\item What will the resulting expected value and standard deviation be, in dollars? 
		\item Suppose $\text{Cov}(S,D) = -4$. How much money should you put in each company? 
		\item What will the resulting expected value and standard deviation be, in dollars? 
		\item What lessons can you draw from this exercise? 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item Because we know the variables are independent, their combined variance is the simple sum of the two.
		We can describe the total variance as follows, where $C$ represents the combination of the two variables, where $0 \leq x \leq 100$ represents the amount invested in the Safety Company:
		\begin{align*}
			\text{Var}[C] &= (x)^2 \cdot \text{Var}[S] + (100 - x)^2 \cdot \text{Var}[D] \\
			\text{Var}[C] &= x^2 \cdot 4 + (x^2 -200x +10000) \cdot 9 \\
			\text{Var}[C] &= 13x^2 - 1800x + 90000  \\
		\end{align*}
		The minimum variance can be found by finding the minimum value of this function on the interval $0 \leq x \leq 100$. We'll find the absolute minimum first:
		\begin{align*}
			0 &= \frac{d}{dx}\, f(x) \\
			0 &= \frac{d}{dx}\, 13x^2 - 1800x + 90000 \\
			0 &= 26x - 1800\\
			26x &= 1800 \\
			x &= \frac{1800}{26} \\
			x &= 69.2308
		\end{align*}
		This means we invest \$69.23 into the Safety Company and \$30.77 into the Danger Company.
		\item The expected value would just be $4 \cdot 100 = 400$. The standard deviation would be the square root of the variance. We can obtain the variance by plugging $x$ back into the simplified equation for variance from earlier:
		\begin{align*}
			\text{Var}[C] &= 13x^2 - 1800x + 90000  \\
			\text{Var}[C] &= 13(69.2308)^2 - 1800(69.2308) + 90000 \\
			\text{Var}[C] &= 27692.30
		\end{align*}
		The standard deviation is $\sqrt{27692.30}$ or 166.41.
		\item In this case, the combined variance will be as follows:
		\begin{align*}
			\text{Var}[C] &= (x^2) \cdot \text{Var}[S] + (x) \cdot (100 - x) \cdot \text{Cov}[S,D] + (100 - x)^2 \cdot \text{Var}[D] \\
			\text{Var}[C] &= x^2 \cdot 4+ x \cdot (100 - x) \cdot -4 + (x^2 -200x +10000) \cdot 9 \\
			\text{Var}[C] &= 17x^2 - 2200x + 90000
		\end{align*}
		Now we just need to minimize this function on the interval $0 \leq x \leq 100$. To do this we can find where its derivative is equal to zero:
		\begin{align*}
			0 &= \frac{d}{dx} \, f(x) \\
			0 &= \frac{d}{dx}  \, 17x^2 - 2200x + 90000 \\
			0 &= 34x - 2200 \\
			34x &= 2200 \\
			x &= \frac{2200}{34} \\
			x &= 64.7059 
		\end{align*}
		This means we want to invest \$64.71 into the Safety Company and \$35.29 into the Danger Company.
		\item The expected value will still be 400. The variance given by this value of x is 
		\begin{align*}
			\text{Var}[C] &= 17x^2 - 2200x + 90000\\
			\text{Var}[C] &= 17(50.625)^2 - 2200(50.625) + 90000 \\
			\text{Var}[C] &= 18823.53
		\end{align*}
		The standard deviation will be equal to the square root of the variance:
		\[
			\sqrt{18823.53} = 137.20
		\]
		\item This exercise should show that, while not intuitive, the variance of a combination of variables can be decreased by adding one with a higher variance. It also shows that the common wisdom to ``not put all your eggs in one basket'' can be mathematically supported.
	\end{enumerate}
\end{solution}

\pagebreak

\begin{problem}[Nearest Neighbors]
	Consider the three points in $\R^2$
	\[
		{\bf x}_1 = \begin{bmatrix}
			2\\0
		\end{bmatrix}, 
		\quad
		{\bf x}_2 = \begin{bmatrix}
			5\\3
		\end{bmatrix},
		\quad
		{\bf x}_3 = \begin{bmatrix}
			5\\1
		\end{bmatrix}. 
	\]
	\begin{enumerate}[label=\alph*)]
		\item Find the perpendicular bisector of each pair of points. Express your answer in the form $S = \{{\bf a} + \alpha {\bf z}: \alpha \in \R\}$, where you have to determine the vectors ${\bf a}$ and ${\bf z}$. 
		\item Find the unique point in $\R^2$ that is equidistant from all three points ${\bf x}_1, {\bf x}_2, {\bf x}_3$. 
	\end{enumerate}
\end{problem}

\begin{solution}
	My work is in the file \texttt{prob\_3.py.} I went to the liberty of plotting the three points, the midpoints between them, and the bisecting lines:
	\begin{figure}[H]
		\centering
		\includegraphics[width=12cm]{lines.png}
		\caption{Blue points are the given $x$ points. Red points are the midpoints between each combination of $x$ point. Red lines are the bisecting lines between each pair of $x$ points. The green dot is the point equidistant from the three $x$ points.}
	\end{figure}
	\begin{enumerate}[label=\alph*)]
		\item I got:
		\[
			{\bf a} = \begin{bmatrix}
				5 \\ 11 \\ 2
			\end{bmatrix} \,\,\,\, {\bf z} = \begin{bmatrix}
				-1 \\ -3 \\ 0
			\end{bmatrix}
		\]
		\item All three lines intersect at the point (3, 2), which is equidistant from the three $x$ points.
	\end{enumerate}
\end{solution}

\pagebreak

\begin{problem}[K-means Clustering]
	Consider the four points $S = \{{\bf x}_1,{\bf x}_2, {\bf x}_3, {\bf x}_4\}$, where
	\[{\bf x}_1 = \begin{bmatrix}
		0\\0
	\end{bmatrix}, {\bf x}_2 = \begin{bmatrix}
		0\\1
	\end{bmatrix}, {\bf x}_3 = \begin{bmatrix}
		2\\1
	\end{bmatrix}, {\bf x}_4 = \begin{bmatrix}
		2\\0
	\end{bmatrix}.\]
	Run the $k$-means clustering algorithm by hand, starting with the cluster ``means'' $\mu_1 = \begin{bmatrix}
		-1\\-1
	\end{bmatrix}$ and $\mu_2 = \begin{bmatrix}
		1\\2
	\end{bmatrix}$ and proceeding to assign each point ${\bf x}_i$ to the cluster belonging to the nearest $\mu$. 
	\begin{enumerate}[label=\alph*)]
	\item After each step, specify the location of the two cluster means $\mu_1$ and $\mu_2$ and note which points ${\bf x}_i$ are assigned to them. You do not have to draw a picture, although you may find it helpful. 
	\item How many steps does it take for the algorithm to converge?
	\end{enumerate}

\end{problem}

\begin{solution}
	So for my ``by hand'' work I implemented the cluster-and-adjust part of the algorithm in Python, and fed it the data one iteration at a time.
	My code is in the file \texttt{prob\_4.py}.
	\begin{enumerate}[label=\alph*)]
		\item Step 1:
		\[
			\mu_1 = \begin{bmatrix}
				0 \\ 0
			\end{bmatrix} \,\, \mu_2 = \begin{bmatrix}
				1.33333 \\ 0.66667
			\end{bmatrix}
		\]
		${\bf x}_1$ is assigned to $\mu_1$, while all other points are assigned to $\mu_2$\\\\
		Step 2:
		\[
			\mu_1 = \begin{bmatrix}
				0 \\ 0.5
			\end{bmatrix} \,\, \mu_2 = \begin{bmatrix}
				2 \\ 0.5
			\end{bmatrix}
		\]
		${\bf x}_1$ and ${\bf x}_2$ are assigned to $\mu_1$, while ${\bf x}_3$ and ${\bf x}_4$ are assigned to $\mu_2$. \\\\
		Step 3:
		\[
			\mu_1 = \begin{bmatrix}
				0 \\ 0.5
			\end{bmatrix} \,\, \mu_2 = \begin{bmatrix}
				2 \\ 0.5
			\end{bmatrix}
		\]
		The means didn't change at all, nor did the assignments. The algorithm has converged.
		\item The algorithm took 2 (3 if we're counting checking) steps to converge.
	\end{enumerate}
\end{solution}

\pagebreak

\begin{problem}[Gaussian Random Vector]
	Consider a set of $n=101$ points evenly spaced on the interval $[0,1]$. For $1\leq i \leq n$ generate vectors $X_i\sim\mathcal{N}(0,1)$ at each of these points $x_i$ for each of the following scenarios: 
	\begin{enumerate}[label=\alph*)]
		\item The samples are independent.
		\item The covariance matrix ${\bf K}$ is given by the function 
		\[K(x_i,x_j) = e^{-(x_i-x_j)^2/(2l^2)}\]
		with $l = 0.02$. 
		\item The covariance matrix is the same as before, but with $l=0.1$. The covariance matrix will not have full rank in this case, so use ${\bf K} + \delta{\bf I}_n$ for some very small value of $\delta$ instead. 
	\end{enumerate}
	Submit plots of your samples. What effect does the parameter $l$ have on the plot? What effect does $l$ have on the singular values of ${\bf K}$? How are these two answers related? 
\end{problem}


\pagebreak

\begin{problem}[Image Compression]
	In the file \texttt{hw6.mat} you will find a grayscale image of the creepy Matlab clown. 
	\begin{enumerate}[label=\alph*)]
		\item Treating each pixel of the image ${\bf X}$ as one data point, group the data into $k=4$ clusters. You may use Matlab's built-in \texttt{kmeans} if you wish. 
		\item Make a histogram of the pixel values, with vertical dashed red lines at each of the clusters. 
		\item Reset the value of each pixel to the mean value of the cluster it belongs to. Plot the original and modified images side by side. 
		\item In what way could it be more efficient to store the modified image than the original? 
	\end{enumerate}
\end{problem}


\begin{solution}
	My code is in the file \texttt{prob\_6.py}.
	\begin{enumerate}[label=\alph*)]
		\item \text{[}Answered in Python file.\text{]}
		\item Below is the histogram. I used 20 buckets in order to provide a clearer picture than the default 10.
		\begin{figure}[H]
			\centering
			\includegraphics[width=8cm]{prob_6_hist.png}
		\end{figure} 
		\item The ``compressed'' image still looks pretty good considering it only has 4 distinct colors in it:
		\begin{figure}[H]
			\centering
			\includegraphics[width=12cm]{prob_6_clown.png}
		\end{figure}
		\item This original image consists of 64,000 64-bit floating-point values. These values consume $64000 \cdot 8 = 512000$ bytes. The array that contains the modified image also takes up this much space, but since it only contains four distinct color values, we can very easily take advantage of a ``colormap'' to reduce the size of this image. 
		\\\\The colormap will have a mapping of an integer value to each distinct color in the image. Instead of having to store the color value of each pixel, we store which number color it uses, and use the map to reconstruct the image. Since there are only four distinct colors in this image, we only need two bits to store the color of one pixel.
		\\\\ Consequently, we can store four adjacent pixels' colors in one byte. This technique allows the image to be stored in $64
		000 / 4 = 16000$ bytes, with an additional $8 \cdot 4 = 32$ to store the colormap and maybe a few more to store dimension information, depending on how advanced we want our custom file format to be. The approximate 16000 bytes to store the compressed image is only 3\% of the original image's size.
	\end{enumerate}
\end{solution}

\pagebreak

\begin{problem}[Covariance Matrix III]
	Load the \texttt{jokeData.mat} data set of hilarious jokes. 
	\begin{enumerate}[label=\alph*)]
		\item Pick a group of 4-5 jokes that you think are related, such as math jokes, puns, poems, anti-jokes, or elephant jokes. Find the covariance matrix ${\bf K}$ of the ratings of those jokes, and report the largest three singular values of ${\bf K}$. 
		\item Do the same, but with 4-5 jokes that do not appear to be related at all. How do your results differ? 
	\end{enumerate}
\end{problem}

\begin{solution}
	I picked the four cow-themed jokes for my experiment: jokes 22, 23, 25, and 34. The three largest singular values from the covariance matrix for these scores are as follows:
	\[
		\begin{bmatrix}
			332.8485 \\ 73.9805 \\ 46.8416
		\end{bmatrix}
	\]
	For the unrelated jokes, I picked jokes 1, 5, 13, and 30. The three largest values from the covariance matrix for these jokes' scores are:
	\[
		\begin{bmatrix}
			138.6716 \\ 64.0416 \\ 57.4231
		\end{bmatrix}
	\]
	The first singular value for the related jokes is much larger than the first for the unrelated jokes. However, the singular values for the ``related'' jokes seem to fall off much faster than those for the ``unrelated'' jokes
\end{solution}

\end{document}