# %% imports
import numpy as np
import matplotlib.pyplot as plt

# %% setup
x1 = np.array([2, 0])
x2 = np.array([5, 3])
x3 = np.array([5, 1])

xcoords = np.array([p[0] for p in [x1, x2, x3]])
ycoords = np.array([p[1] for p in [x1, x2, x3]])
arr = np.array([xcoords, ycoords])

midpoint = lambda p1, p2: (p1 + p2) / 2
slope = lambda p1, p2: (p2[1] - p1[1]) / (p2[0] - p1[0])
inv_slope = lambda p1, p2: -1 / slope(p1, p2)

# %% plot the points
plt.scatter(arr[0, :], arr[1, :])
pairs = [(x1, x2), (x1, x3), (x2, x3)]
midpoints = np.array([midpoint(*pair) for pair in pairs]).T
plt.scatter(midpoints[0, :], midpoints[1, :], color="r")

slopes = np.array([inv_slope(*pair) for pair in pairs])
intercepts = midpoints[1, :] - (midpoints[0, :] * slopes)

bisect_1 = lambda x: slopes[0] * x + intercepts[0]
bisect_2 = lambda x: slopes[1] * x + intercepts[1]
bisect_3 = lambda x: slopes[2] * x + intercepts[2]
xvals = np.linspace(0, 6)
plt.plot(xvals, bisect_1(xvals), color="r", linestyle="--")
plt.plot(xvals, bisect_2(xvals), color="r", linestyle="--")
plt.plot(xvals, bisect_3(xvals), color="r", linestyle="--")
plt.axis("square")
plt.xlim((0, 6))
plt.ylim((-1, 5))
plt.title("Center points and bisecting lines")

plt.scatter(3, 2, color="c")
# plt.savefig("lines.png", dpi=300)
# plt.scatter(midpoint[0], midpoint[1])


S = np.array([intercepts, slopes]).T
print("S matrix composed of vectors a and z:")
print(S, "\n\n\n")
# %%
