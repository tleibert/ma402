"""
Author: Trevor Leibert
Assignment: MA402 Homework 1
Date: 1/31/2021
"""

total = 0
for i in range(1, 101):
    total += i ** 3
    
print(total)
