"""
Author: Trevor Leibert
Assignment: MA402 Homework 1
Date: 1/31/2021
"""

import numpy as np

array = np.arange(1, 101)
array = array ** 3
total = np.sum(array)
print(total)
