"""
Author: Trevor Leibert
Assignment: MA402 Homework 1
Date: 1/31/2021
"""
# %% imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

# %% data
days = [1, 2, 3, 4, 5]
cells = [16, 27, 45, 74, 122]

# %% equation
alpha_0 = np.double(9.7309)
alpha_1 = np.double(0.5071)

xvals = np.linspace(1, 5, 500)
yvals = alpha_0 * np.exp(alpha_1 * xvals)

# y = alpha_0 * e ^ (alpha_1 * t)
# solve for t:
#  t = ln(y/alpha_0) / alpha_1
# y = 100
t_100 = np.log(100 / alpha_0) / alpha_1
# t ~= 4.594486

# %% plots
plt.plot(xvals, yvals, color="r", linewidth=4, label="Data")
plt.plot(days, cells, marker="+", color="k", linestyle="None", markersize=20, label="Fit")
plt.vlines(t_100, np.min(yvals), np.max(yvals), color="g", linestyle="--", linewidth=4, label="Prediction")


plt.legend(fontsize=18)
plt.title("Exponential line fit", size=20)
plt.xlabel("t (days)", size=18)
plt.ylabel("y (cells)", size=18)
plt.gca().set_facecolor("w")
plt.savefig("problem11.png", transparent=False, edgecolor="w", facecolor="w")
plt.show()

# %%
