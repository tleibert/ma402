# %% imports
import numpy as np

# %% Problem 1
A = np.ones((2, 4))
B = np.ones((4, 2))
C = np.ones((4, 1))
D = np.ones((2, 1))

print(B @ A)
print(A @ B)
print(A @ B @ D)
# print(D @ B @ A)
print(A @ (B + C))
print(B + C)

# %%
