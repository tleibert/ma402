%% Establish A
A = [-3 3 7 -8;
     -8 1 0 2;
     5 -1 -1 0;];

%% a) rank of A
fprintf("Rank of A: %d\n", rank(A));

%% b) singular values of A
disp("Singular values of A:");
disp(svd(A));

%% c) 2-norm and F-norm
fprintf("2-Norm of A: %f\nFro-Norm of A: %f", norm(A, 2), norm(A, "fro"))

%% d) rank 1 approximation of a
[u, s, v] = svd(A);
approx = u(:,1) * s(1,1) * v(:,1)';
disp("Best rank-1 approximation to A:");
disp(approx);

test = true
if test
    fprintf("Error in best: 2-norm: %f, error in F-norm %f\n", norm(A - approx, 2), norm(A - approx, "fro"))
    approx2 = u(:,2) * s(2,2) * v(:,2)';
    fprintf("Error in other: 2-norm: %f, F-norm %f\n", norm(A - approx2, 2), norm(A - approx2, "fro"))
end
%% e) orthogonal projection matrix for R(A)
PI = v(:,1) * v(:,1)';
disp("Projection matrix")
disp(PI)
