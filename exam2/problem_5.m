%% Problem 5 Trevor Leibert
A = [1 1 1 1;
     1 2 4 8;
     1 3 9 27;
     1 4 16 64;];
b = [1;
     sqrt(2);
     pi;
     exp(8)];

x = A\b;
disp("X from alpha_0 to alpha_3")
disp(x)
disp("Condition number of A")
disp(cond(A))
