# %%
import numpy as np

# %% setup
NUM_TRIALS = 1000
TARGET = 195

a = np.random.randint(1, 9, (NUM_TRIALS, 45))

# %% a) confidence interval
sums = np.sum(a, axis=1)
# 
victories = sums >= 195
standard_dev = np.std(victories)
standard_err = standard_dev / np.sqrt(NUM_TRIALS)
mean = np.mean(victories)

latex = False
if latex:
    print(f"Interval: ${mean} \pm {1.96 * standard_err}$")
else:
    print(f"mu = {mean}, s = {standard_err}")
# %%
