import numpy as np


def isEven(num: float) -> bool:
    return num + np.sign(num) * np.spacing(num) / 2 == num

print(isEven(np.pi))
print(isEven(np.e))
print(isEven(np.sqrt(2 * np.pi)))
print(isEven(np.finfo(np.float64).eps))
