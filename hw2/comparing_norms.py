# %% imports
import numpy as np
import matplotlib.pyplot as plt

# %% generate data
NUM_TRIALS = 10000

data = []

for i in range(1, 11):
    l = []
    for _ in range(NUM_TRIALS):
        vec = np.random.randn(i, 1)
        norm_1 = np.linalg.norm(vec, 1)
        norm_2 = np.linalg.norm(vec, 2)
        l += [norm_1 / norm_2]
    data.append(l)

# pick largest and smallest ratios from each size
largest_and_smallest = [(max(value), min(value)) for value in data]
labels = ["Max of ratio (out of 10000 trials)", "Min of ratio (out of 10000 trials)"]

# %% plot
for i in range(2):
    plt.plot(
        range(1, 11),
        [e[i] for e in largest_and_smallest],
        label=labels[i],
        marker="+",
        mec="k",
        markersize=20,
        linewidth=4
    )

plt.title("Comparing Norms: $\|\|\\vecx\|\|_1$ vs. $\|\|\\vecx\|\|_2$", size=20)
plt.xlabel("n (number of dimensions in vector $\\vecx$)", size=18)
plt.ylabel("ratio of norms ($\\frac{\|\|\\vecx\|\|_1}{\|\|\\vecx\|\|_2}$)", size=18)
plt.legend(fontsize=11)
plt.tight_layout()
plt.savefig("comparing_norms.png", transparent=False, edgecolor="w", facecolor="w")
plt.show()
# %%
