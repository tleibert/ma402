\documentclass{article}[10pt]

% PACKAGES AND DEFINITIONS
\usepackage{amsmath,amsthm,amssymb}
\usepackage{graphicx,enumitem,multicol}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{dsfont}
\usepackage{minted}

\theoremstyle{definition}
\newtheorem{problem}{Problem}
\specialcomment{solution}{{\bf Solution: }}{}
\setlength{\parindent}{0pt}

\newcommand{\x}{{\bf x}}
\newcommand{\y}{{\bf y}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\1}{\mathds{1}}

% TOGGLE SOLUTION HERE
%\excludecomment{solution}

\begin{document}
	
	
\title{Homework \#2}
\author{Trevor Leibert}	
\date{\today}

\maketitle

\begin{problem}[Condition Numbers]
	Find the absolute and relative condition numbers of the following functions. If the answer depends on the value of $x$ at which the function is evaluated, for which values of $x$ is the function especially ill-conditioned? Are there any values of $x$ for which the function is unusually well-conditioned? 
	\begin{enumerate}[label=\alph*)]
		\item $f_1(x) = \sqrt{1-x^2}$ for $x\in (-1,1)$
		\item $f_2(x) = 1/(1+x)$ for $x\neq -1$
		\item $f_3(x) = \sqrt{1+x^2}$ 
	\end{enumerate}
\end{problem}

\begin{solution}
	\begin{enumerate}[label=\alph*)]
		\item Absolute condition number: $|\frac{x}{\sqrt{1 - x^2}}|$. Relative condition number: $|\frac{x^2}{1 - x^2}|$. For $x = 0$ this function has a condition number of 0 for both absolute and relative. Near $x = \pm 1$ the relative condition number goes to $+\infty$.
		\item Absolute condition number: $|\frac{1}{(1 + x)^2}|$. Relative condition number: $|\frac{x}{1 + x}|$. Near $x = -1$ the function is very poorly conditioned, and it gets better-conditioned the farther away it is from -1. The absolute condition number gets very small as the magnitude of x gets very large (positive or negative). The function has a very ``good'' relative condition number near $x = 0$.
		\item Absolute condition number: $|\frac{x}{\sqrt{1 + x^2}}|$. Relative condition number: $|\frac{x^2}{1 + x^2}|$. The function is well-conditioned near $x = 0$ and remains well-conditioned for every x value, as both functions approach 1 as the magnitude of x increases.
	\end{enumerate}
\end{solution}

\begin{problem}[Can You Even?]
	For a floating point number $x$ satisfying $\texttt{realmin}\leq \texttt{x} \leq \texttt{realmax}$, the function
	\begin{center}
		\texttt{f = @(x) ( x + sign(x)*eps(x)/2 == x )}
	\end{center} 
	will return \texttt{1} if $x$ is ``even'' (i.e. its final bit is zero) and \texttt{0} if $x$ is ``odd''. Use the rounding rules of IEEE arithmetic to justify this assertion. 
	
	Use the function $f$ (or the Python equivalent, with \texttt{numpy.finfo}) to determine the final bit in each of the IEEE double-precision representations of $\pi, e$, and $\sqrt{2\pi}$. Bonus: why does the function not work correctly for $x = \texttt{eps}(0)$? 
\end{problem}

\begin{solution}
	My Python function looked like this:
	\begin{minted}{Python}
		def isEven(num: float) -> bool:
    		return num + np.sign(num) * np.spacing(num) / 2 == num
	\end{minted}
		I got:
	\begin{itemize}
		\item $\pi$: even
		\item $e$: odd in Python, even in Matlab. $e$ in NumPy is stored as 2.718281828459045, while in Matlab it is stored as 2.718281828459046.
		\item $\sqrt{2\pi}$: odd
	\end{itemize}
	
	The function does not work properly for $x = \texttt{eps(0)}$ because it returns true, even though 0 is an even number, so the next number after it, \texttt{eps(0)}, must be odd, as numbers on the floating point number line alternate strictly even-odd. This error comes from the second part of the addition, where \texttt{eps(0)} is divided by 2, which ends up with an unrepresentable quantity that gets rounded to 0. Then it follows that $\texttt{eps(0)} + 0 = \texttt{eps(0)}$
\end{solution}

\begin{problem}[Condition Numbers II]
	If you type \texttt{cos((10\^{}10)/7)} into Matlab, about how many correct digits can you expect the answer to have? Use the relative condition number of \texttt{cos} to justify your answer, but note that $10^{10}$ will be computed exactly. 
\end{problem}

\begin{solution}
	The relative condition number of $\cos{x}$ is $x\tan(x)$. For $x = \frac{10^{10}}{7}$, this evaluates to $\kappa_{rel} \approx 2.596 \times 10^{10}$, making $\log_{10}({\kappa_{rel})} \approx 10.414$. This is pretty large. Since we know $\frac{10^{10}}{7}$ to about 16 digits due to it being stored as an approximation as a double, we will only know it to about 5-6 digits after it goes through \texttt{cos()}.
\end{solution}

\begin{problem}[Hilarious Joke]
	``I wanted to plot a circle in Matlab, but no matter how I tried it was always just a little bit off. Turns out, it was a rounding error.'' Please explain the joke. 
\end{problem}

\begin{solution}
	The joke is that in an environment such as Matlab, rounding error is a real concern when performing operations on floating point numbers, but a ``rounding'' error in a circle would make it appear funny.
\end{solution}

\begin{problem}[Error Tolerance]
	You have a perfect sphere and a piece of string. You wish to estimate the volume of the sphere to within about 1 percent of the true answer (i.e., a relative error of at most 0.01), but you can only use the string to measure the circumference. Come up with a function that relates the circumference $C$ to the volume $V$. What is its condition number? About how accurately must you measure the circumference? 
\end{problem}

\begin{solution}
	My equation is $V(C) = \frac{C^3}{6\pi^2}$. The relative condition number of this equation is 3. To find the tolerance for error, I found $\frac{1\%}{\kappa_{rel}}$, which evaluates to $.01/3 = 0.00333333333\dots$. This means we need to know the circumference to within 0.333333\dots\%, or $\frac{1}{3}\%$.
\end{solution}

\begin{problem}[Composite Functions]
	Let $f$ be a differentiable function with relative condition number $\kappa_1$ at $x$, where $x\neq 0$. Furthermore assume that $f(x)\neq 0$, and let $g$ be a differentiable function with relative condition number $\kappa_2$ at $f(x)$. Assuming that $g(f(x))\neq 0$ as well, show that the composite function $g\circ f$ defined by $g\circ f(x) = g(f(x))$ has relative condition number $\kappa_1\cdot \kappa_2$ at $x$. [Hint: Chain rule for derivatives.]
\end{problem}

\begin{solution}
	For lack of a better method I will enumerate my steps here.
	\begin{enumerate}
		\item Definition of the relative condition number for an \emph{arbitrary} function $f(x)$: $\kappa = |\frac{x \cdot f^\prime(x)}{f(x)}|$
		\item The function in this case is $g(f(x))$, which I will refer to as $h(x)$.
		\item The derivative of this function is $h^\prime(x) = g^\prime(f(x)) \cdot f^\prime(x)$, per the chain rule.
		\item Plugging this into the equation for the relative condition number, we get this: $\kappa_3 = |\frac{x \cdot h^\prime(x)}{h(x)}|$.
		\item Assume $\kappa_3 = \kappa_1 \cdot \kappa_2$. We can rewrite this using the definition of each $\kappa$: $|\frac{x \cdot h^\prime(x)}{h(x)}| = |\frac{x \cdot f^\prime(x)}{f(x)}|\cdot |\frac{f(x) \cdot g^\prime(f(x))}{g(f(x))}|$
		\item Use the properties of absolute values to pull the right side of the equation together, cancelling the $f(x)$ terms on the top and bottom: $|\frac{x \cdot h^\prime(x)}{h(x)}| = |\frac{x \cdot g^\prime(f(x)) \cdot f^\prime(x)}{g(f(x))}|$ 
		\item Expand the left side's values of $h^\prime(x)$ and $h(x)$: $|\frac{x \cdot g^\prime(f(x)) \cdot f^\prime(x)}{g(f(x))}| = |\frac{x \cdot g^\prime(f(x)) \cdot f^\prime(x)}{g(f(x))}|$
		\item Both sides are equal!
	\end{enumerate}
\end{solution}

\begin{problem}[Computing Norms]
	Let ${\bf x} = [2, 7, -4, -10]$. What are $\|\x\|_1$, $\|\x\|_2$, and $\|\x\|_\infty$? 
\end{problem}

\begin{solution}
	\begin{itemize}
		\item $\|\x\|_1$ = 23
		\item $\|\x\|_2$ = 13
		\item $\|\x\|_\infty$ = 10
	\end{itemize}
\end{solution}

\begin{problem}[Properties of a Norm]
	For $x \in \R$, consider the function $f(x) = \sqrt{1+ |x|}$. Which of the three basic properties of norms does $f(\cdot)$ satisfy?
\end{problem}

\begin{solution}
	\begin{enumerate}
		\item $f(\cdot)$ does not satisfy property 1, as the addition of 1 to $|x|$ in the function prevents this function from evaluating to zero when x is zero.
		\item $f(\cdot)$ also does not satisfy property 2, as well. For the simple example of $x = 3$, $f(x)$ evaluates to 2. If the second property were to hold true, $f(6)$ would have to evaluate to 4, but it evaluates to $sqrt(7)$.
		\item $f(\cdot)$ does satisfy property 3, the triangle inequality. I can show this informally by expanding the triangle inequality equation, which starts as $\sqrt{1 + |x + y|} \leq \sqrt{1 + |x|} + \sqrt{1 + |y|}$. After squaring both sides, this expands to $1 + |x + y| \leq 2 + |x| + |y| + \sqrt{(1 + |x|)(1 + |y|)}$. Through the properties of absolute values, I know that $|x + y| \leq |x| + |y|$ always, and I also know that $\sqrt{(1 + |x|)(1 + |y|)}$ is always positive for any $x$ and $y$. Ergo the triangle inequality holds true here, though I am lacking the mental tools for a complete proof.
	\end{enumerate}
\end{solution}

\begin{problem}[Trivia Scoring]
	You ask a group of friends to write down their best approximations to the following quantities: 
	\begin{itemize}
		\item The circumference of the Earth, in kilometers
		\item The number of liters in a hogshead 
		\item The average velocity of an unladen European swallow, in m/s
	\end{itemize}
	You would like to determine which of your friends made the best guesses overall. Come up with a method that uses vector norms to assign a numerical score to each friend's response. 
\end{problem}

\begin{solution}
	The scoring method will be to take the 2-norm of $\vec{A} - \vec{x}$, where $\vec{A}$ is the actual value for each number, and $\vec{x}$ is one set of guesses for each value. $\vec{A} = [40075, 245, 11]$ are the values that I will be holding as \emph{absolute}, with first being the circumference of the Earth in kilometers, second being the number of liters in a hogshead, and third being the average velocity of an unladen European swallow, in m/s. Scores closer to 0 are better. $s(\vec{x}) = \|\vec{A} - \vec{x}\|_2$
\end{solution}

\begin{problem}[Comparing Norms]
	How large can $\|{\bf x}\|_1$ get compared to $\|{\bf x}\|_2$? Write a Matlab program to generate random vectors ${\bf x} \in \mathbb{R}^n$ for $n = 1:10$. Run multiple trials for each value of $n$ and plot the largest and smallest ratios $\|{\bf x}\|_1/\|{\bf x}\|_2$ observed for each value of $n$. You may find the following commands helpful: 
	\begin{itemize}
		\item \texttt{x = randn(n,1)} generates a random n-dimensional vector
		\item \texttt{norm(x,p)} returns the p-norm of x. The equivalent in Python is \texttt{numpy.linalg.norm}. 
		\item \texttt{max(v)} and \texttt{min(v)} find the maximum and minimum elements in \texttt{v}
	\end{itemize}
	Offer a hypothesis about the relative magnitudes of the 1-norm and the 2-norm. 
\end{problem}

\begin{solution}
	The source code for this problem is in the file \texttt{comparing\_norms.py}. It seems that the maximum is bounded by a log function, or maybe a root of some kind, while the minimum seems to grow at a linear rate. My hypothesis is that there is some logarithmic function $f(n)$ of the magnitude 2-norm which will yield the magnitude of the 1-norm of the same vector. Something like $\|\vec{x}\|_1 = k \times log_?{\|\vec{x}\|_2}$.
	\begin{figure}[h!]
		\includegraphics{comparing_norms.png}
		\caption{minimum and maximum of $\|\vec{x}\|_1$ versus $\|\vec{x}\|_2$}
	\end{figure}
\end{solution}
	
\end{document}