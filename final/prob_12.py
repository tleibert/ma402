# %% imports
import numpy as np
import matplotlib.pyplot as plt

# %% part (a) - find x0 through x10
def x(n):
    """ fancy recursive function for X """
    if n == 0:
        return 1
    return np.sqrt(1 + x(n - 1))

xs = np.array([x(i) for i in range(11)])
print(f"Value of x10: {xs[10]:.5f}")
# %% part (b) 
phi = (1 + np.sqrt(5)) / 2
deltas = np.abs(xs - phi)
b = np.log(deltas)
A = np.array([np.ones(len(xs)), np.arange(len(xs))]).T
(lnalpha, beta) , _, _, _ = np.linalg.lstsq(A, b, rcond=None)
alpha = np.e ** lnalpha

f = lambda n: alpha * np.e ** (beta * n)
xvals = np.linspace(0, 10)
plt.scatter(np.arange(11), deltas, marker="+", s=60, label="$x_n$ values")
plt.plot(xvals, f(xvals), color="tab:orange", label="fitted line")
plt.legend()
plt.xlabel("$n$")
plt.ylabel("$|x_n - \phi|$")
plt.title("Examining the recurrence relation vs the best fit line")
# plt.savefig("prob_12.png", dpi=300)
# %% part (c)
eps = np.finfo(np.double).eps
n = (np.log(eps) - np.log(alpha)) / beta
print(f"{n=:.2f}")

# %%
