# %% imports
import numpy as np

# %% setup
x = np.arange(1, 7)
x1 = x ** 2
x2 = 36 - x ** 2
# %% part (b)
print(f"E[X1]: {np.mean(x1):.5f}, Var[X1]: {np.var(x1):.5f}")

# %% part (c)
print(f"E[X2]: {np.mean(x2):.5f}, Var[X2]: {np.var(x2):.5f}")

# %% part (d)
X = np.array([x1, x2])
cov_mat = np.cov(X, bias=True)
print("Covariance Matrix:")
print(cov_mat)

# %% part (e)
s = np.linalg.svd(cov_mat, compute_uv=False)
print("Singular Values of Covariance Matrix:")
print(s)
# %%
