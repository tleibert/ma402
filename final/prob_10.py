# %% imports
import numpy as np

# %% setup
NUM_TRIALS = 100000

data = np.random.randint(1, 7, (NUM_TRIALS, 20))
s = np.sum(data, axis=1)
results = s * (s % 10 <= 5) - s * (s % 10 > 5)
av = np.mean(results)
std_dev = np.std(results)
std_err = std_dev / np.sqrt(len(results))
print("Mean: ", av)
print("Std err: ", std_err)
# multiplying this by 1.96 will give the bounds on the confidence interval
print(f"95% confidence interval: {av:.4f} +- {1.96 * std_err:.4f}")

# %%
