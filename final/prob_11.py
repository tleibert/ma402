# %% imports
import numpy as np

# %% setup
A = np.array([[1, 2],
              [-1, -2],
              [1e-10, 0]])

b = np.array([1, 1, 1])

# %% (a) solve
x, _, _, _ = np.linalg.lstsq(A, b, rcond=None)
print(x)

# %%
