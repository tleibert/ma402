# %% imports
import numpy as np

# %% setup
def f(x):
    if (x > 0):
        return 1 / (x + np.sqrt(x ** 2 - 1))
    
    return x - np.sqrt(x ** 2 - 1)

x1 = 1e6
x2 = -6e7

# %% test
print(f"f(x1) = {f(x1)}")
print(f"f(x2) = {f(x2)}") 
# %%
