# %% imports
import numpy as np

# %% setup
f = lambda x1, x2: np.abs(x1 + 2 * x2) + np.abs(3 * x1 + 6 * x2)

# %% (1) disprove positive definiteness
a = np.array([-2, 1])
print("A: ", a)
print("f(A): ", f(*a))

# %% (2) test absolute scaling
b = np.array([2, 3])
print(f(*b) * 2)
print(f(*(b * 2)))

# %% (3) test triangle inequality
c = np.array([-2000, 100])
d = np.array([0, 2])
print(f(*(c+d)))
print(f(*c) + f(*d))


# %%
