# %% imports

import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.vq import kmeans2

# %% setup/ (a) and (b)
points = np.array([[1, 0],
                   [2, 2],
                   [5, 2.0]])

sol1_means, sol1_groups = kmeans2(points, 2, minit="points")
sol2_means, sol2_groups = kmeans2(points, [[3,1], [1,3]])

fig, axs = plt.subplots(1, 2)

means = [sol1_means, sol2_means]
groups = [sol1_groups, sol2_groups]
labels = ["Optimal Means", "Non-Optimal Means"]
titles = ["One optimal solution", "One non-optimal stable solution"]
for i in range(2):
    axs[i].scatter(points[:, 0], points[:, 1], label="Points in $S$")
    axs[i].axis("square")
    axs[i].set_xlim((0, 6))
    axs[i].set_ylim((-1, 5))
    axs[i].scatter(means[i][:, 0], means[i][:, 1], label=labels[i])
    axs[i].legend()
    axs[i].set_title(titles[i])
# plt.savefig("prob_3.png", dpi=300)

print("Optimal Means:")
print(sol1_means)
print("Stable Means:")
print(sol2_means)

# %% part (c)
def sum_dist(points, means, groups):
    """
    Returns the sum-of-squares distance for the given
    points, means, and groups
    """ 
    total = 0
    dist = lambda p1, p2: np.linalg.norm(p2 - p1, 2) ** 2

    for idx, el in enumerate(groups):
        total += dist(points[idx], means[el])

    return total

dist1 = sum_dist(points, sol1_means, sol1_groups)
dist2 = sum_dist(points, sol2_means, sol2_groups)
print("Total for solution 1: ", dist1)
print("Total for solution 2: ", dist2)
# %%
