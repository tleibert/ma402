# %% imports
from adjustText import (
    adjust_text,
)  # for making scatterplot point labels actually legible
from scipy.io import loadmat
from scipy.cluster.vq import kmeans2
import numpy as np
import matplotlib.pyplot as plt

# %% load data
jokeData = loadmat("jokeData.mat")
J = jokeData["J"]
jokeTexts = jokeData["jokeTexts"]
jokeNames = jokeData["jokes"]
# dictionary allowing for easy lookup of joke text by name
jokedict = {str(jokeNames[0][i][0]): str(jokeTexts[0][i][0]) for i in range(42)}
# %% setup
# transpose the jokes matrix so the jokes are rows and students are columns
JT = J.T
# number of dimensions to reduce to
K = 3
# number of groups to cluster into
GROUPS = 3

# %% build lower-rank approximation of J
u, s, vt = np.linalg.svd(JT)
plt.plot(s)
plt.xlabel("Singular value no.", size=12)
plt.ylabel("Value of singular value", size=12)
plt.title(r"Singular values of ${\bf J}^T$", size=18)
# plt.savefig("sing_vals.png", dpi=300)

ss = np.zeros(np.shape(JT))
np.fill_diagonal(ss, s)

J_K = u[:, :K] @ ss[:K, :K] @ vt[:K, :]
print(
    "Relative error in approximation matrix: ",
    np.linalg.norm(J_K - JT, 2) / np.linalg.norm(JT, 2),
)
# %% run kmeans

scores = u[:, :K] @ ss[:K, :K]
centroids, groups = kmeans2(J_K, GROUPS, minit="points")

# %% plot results
fig, ax = plt.subplots(figsize=(8, 8))
COLORS = "rgbycm"
for i in range(42):
    plt.scatter(scores[i, 0], scores[i, 1], color=COLORS[groups[i]])
    # plt.annotate(jokeNames[0,i][0], (scores[i,0],scores[i,1]))

# place and accumulate all text labels on the graph
texts = [
    plt.text(scores[i, 0], scores[i, 1], jokeNames[0, i][0], ha="center", va="center")
    for i in range(42)
]

plt.xlabel("First principal component", size=14)
plt.ylabel("Second principal component", size=14)
plt.title("Clustering jokes by score", size=20)
# make the plot texts actually legible by adjusting their positions
adjust_text(texts)
plt.savefig("joke_clusters.png", dpi=300)

# %%
