# %% imports
from scipy.io import loadmat
import numpy as np
import matplotlib.pyplot as plt

# %% (a) load data and convert to double precision
mnist38 = loadmat("mnist38.mat")
testX = mnist38["testX"].astype(float)
testY = mnist38["testY"][0]
trainX = mnist38["trainX"].astype(float)
trainY = mnist38["trainY"][0]

# %% (b) center data and reduce dimensionality
K = 2
# center data
testX -= np.mean(testX, axis=0)
trainX -= np.mean(trainX, axis=0)

# 
u, s, vt = np.linalg.svd(trainX, K)
v = vt.T

ss = np.zeros(np.shape(trainX))
np.fill_diagonal(ss, s)

# take advice from the email
V2 = v[:,:2]
vt_trainX = vt
trainX = trainX @ V2
# use same coordinate system as before
testX = testX @ V2

# %% (c) create numeric data Ztrain
Ztrain = 1 * (trainY == 3) - 1 * (trainY == 8)

# create A to fit model to
A = np.array([np.ones(len(trainX)), trainX[:, 0], trainX[:, 1]]).T
print("Entries in A are a column of 1's, and\nthe two columns in the reduced version of trainX")
print("First 5 rows of A:")
print(A[:5,:])

# fit model to A
x, _, _, _ = np.linalg.lstsq(A, Ztrain, rcond=None)
# lambda function that applies x to inputs x1 and x2
classifier = lambda x1, x2: x[0] + x[1] * x1 + x[2] * x2


# solve for boundary line
# 0 = alpha0 + alpha1 * x1 + alpha2 * x2
# -alpha2 * x2 = alpha0 + alpha1 * x1
# x2 = (alpha0 + alpha1 * x1) / -alpha2
bound_line = lambda x1: (x[0] + x[1] * x1) / -x[2]

# %% (d) plot color-coded data + boundary line
plt.scatter(trainX[trainY == 3][:,0], trainX[trainY == 3][:,1], label="3", s=1)
plt.scatter(trainX[trainY == 8][:,0], trainX[trainY == 8][:,1], label="8", s=1)
xvals = np.linspace(-1500, 1500, 20000)
plt.plot(xvals, bound_line(xvals), color="k", label="decision boundary")
plt.legend()
plt.xlabel("$x_{1}$", size=12)
plt.ylabel("$x_{2}$", size=12)
plt.title("Classifying digits with a linear model", size=16)
plt.savefig("digit_scatterplot.png", dpi=300)

# %% (e) classify training data
train_classify = np.sign([classifier(*row) for row in trainX])
train_classify = 3 * (train_classify == 1) + 8 * (train_classify == -1)
total_right_train = np.sum(train_classify == trainY)
total_threes = np.sum(trainY == 3)
total_eights = np.sum(trainY == 8)
three_as_eight = np.sum(np.logical_and(train_classify == 3, trainY == 8))
eight_as_three = np.sum(np.logical_and(train_classify == 8, trainY == 3))
print("TRAINING DATA:")
print(f"Accuracy Rate: {total_right_train / len(trainY) * 100:.2f}%")
print(f"Percentage of 3's incorrectly classified as 8's: {three_as_eight / total_threes * 100:.2f}%")
print(f"Percentage of 8's incorrectly classified as 3's: {eight_as_three / total_eights * 100:.2f}%")


# classify test data
classify_test = np.sign([classifier(*row) for row in testX])
classify_test = 3 * (classify_test == 1) + 8 * (classify_test == -1)
total_right_test = np.sum(classify_test == testY)
total_threes_test = np.sum(testY == 3)
total_eights_test = np.sum(testY == 8)
three_as_eight_test = np.sum(np.logical_and(classify_test == 3, testY == 8))
eight_as_three_test = np.sum(np.logical_and(classify_test == 8, testY == 3))
print("\n\nTEST DATA:")
print(f"Accuracy Rate: {total_right_test / len(testY) * 100:.2f}%")
print(f"Percentage of 3's incorrectly classified as 8's: {three_as_eight_test / total_threes_test * 100:.2f}%")
print(f"Percentage of 8's incorrectly classified as 3's: {eight_as_three_test / total_eights_test * 100:.2f}%")

# %% (f) show the "eigendigits"
v1 = vt_trainX[0, :]
v2 = vt_trainX[1, :]
fig, axs = plt.subplots(1, 2)
titles = ["$v_1$", "$v_2$"]
for i, vec in enumerate([v1, v2]):
    axs[i].imshow(np.reshape(vec, (28, 28)))
    axs[i].set_xticks([])
    axs[i].set_yticks([])
    axs[i].set_title(titles[i])
fig.suptitle("First and second right singular vectors, visualized")
plt.savefig("eigendigits.png", dpi=300)
# %%
