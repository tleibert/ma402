\documentclass{article}[10pt]
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{float,graphicx}

\setlength{\parindent}{0pt}

\begin{document}
	\title{Project \#4}
	\author{Trevor Leibert}	
	\date{\today}
	\maketitle
	\begin{center}
		I had no partners in the making of this project.
	\end{center}
	\pagebreak

	\section*{Part 1 Writeup}
	\subsection*{Introduction}
	K-means clustering is easily applied to datasets featuring a limited number of dimensions, but becomes less and less useful as the number of dimensions in the data increases, due to the aptly-named ``curse of dimensionality,'' where the space a dataset occupies rapidly becomes extremely sparse. To counteract this effect, we can use the truncated SVD to limit the dimensionality of our dataset by building a lower-rank approximation of the original data.
	\subsection*{Python Code}
	The code for this project was fairly straightforward. Of note is that in order to group based on joke rather than by student, the {\bf J} scores matrix had to be transposed. Also of note is that because NumPy returns the singular values as a vector, they must be manually placed on the diagonal of the ${\bf \Sigma}$ matrix, which I refer to as \texttt{ss}, as follows:
	\begin{minted}{Python}
import numpy as np
from scipy.io import loadmat

jokeData = loadmat("jokeData.mat")
J = jokeData["J"]

JT = J.T
u, s, vt = np.linalg.svd(JT)
ss = np.zeros(np.shape(JT))
np.fill_diagonal(ss, s)

K = 3
J_K = u[:,:K] @ ss[:K,:K] @ vt[:K,:]
	\end{minted}
	This code constructs the rank-K approximation of the transposed Joke scores matrix {\bf J}. \\
	I also found a nice library called \texttt{adjustText} that allowed me to produce the nicely labeled points in Figure 2.

	\pagebreak
	\subsection*{Results and Analysis}
	The singular values of {\bf J} fall off very quickly at first, with the first singular value $\sigma_1$ has a value of about 240, and the next one, $\sigma_2$, has a value of about 39. We can see this drop-off in the following figure:
	\begin{figure}[H]
		\centering
		\includegraphics[width=10cm]{sing_vals.png}
		\caption{Singular values of ${\bf J}^T$.}
	\end{figure}
	This means we can capture a lot of the information present in ${\bf J}^T$ with a relatively low-rank approximation, and adding dimensions will produce greatly diminishing returns in the form of accuracy in our approximation.

	I chose to reduce to rank 3, which gives a relative error of 0.1208 in the 2-norm, while allowing for K-means to function free from the effects of a much higher-dimension approximation.
	In order to reduce the relative error to below 10\%, we'd need to add 2-4 more dimensions to the data, which would impact K-means far more than the small gains in accuracy in the input data would be worth.

	The jokes seemed to cluster well into three well-defined groups, which I then plotted on the first and second principal components of the data matrix.
	\begin{figure}[H]
		\centering
		\includegraphics[width=12cm]{joke_clusters.png}
		\caption{Jokes grouped by K-means clustering. These groupings used a rank-3 approximation to the data matrix, and grouped into three clusters, indicated by the points' colors.}
	\end{figure}
	The groups didn't change much from run to run after nailing down the number of dimensions and the number of groups to use. The clusters seem to actually have some meaning. The green cluster in the lower left contains mostly STEM-themed jokes. The red cluster in the upper right has more than its fair share of cow-themed jokes. The middle cluster seems to contain lots of ``anti-jokes.''
	I'd hazard a guess that one of the axes has something to do with the ``funniness'' of the joke and another might have something to do with the ``nerdiness'' of the joke, though ascribing a real meaning to these first and second principal components of the scores would be difficult to impossible.
	I'd guess this mainly because the STEM jokes all have the trait of ``nerdiness'' common to them, and I found them to be all pretty funny compared to most of the other jokes.
	\subsection*{Conclusions}
	This part of the project shows the sheer potential for PCA/SVD and K-means to be used together to group data and assist in its interpretation, even when said data is rather complex and numerous. I'd be interested in exploring this technique further and reading more about its applications for automation and machine learning. I'd also be interested in comparing the makeup of the jokes based on the words they contained, and seeing how such an analysis would compare to this score-based grouping method.
	\pagebreak

	\section*{Part 2 Writeup}
	\subsection*{Introduction}
	This part of the project shows how least-squares techniques can be used along with the SVD's dimension-reducing properties to fit a linear model to high-dimensional data. In this case we will be operating on $28 \times 28$ images of the digits 3 and 8, from the \texttt{mnist} dataset. Using a linear regression via least-squares, we can fit a model to the reduced dataset that can identify a given image.
	\subsection*{Python Code}
	The code for this section was straightforward, though computing the SVD of the \texttt{trainX} data did take a little while on my machine, while using 100\% of the CPU. The code for this part of the project is in the file \texttt{digit\_classification.py}, with cells labelled by part letter.\\\\
	NumPy's array operations proved very useful in constructing things like the \texttt{Ztrain} vector, given here:
	\begin{minted}{Python}
trainY = mnist38["trainY"][0]
Ztrain = 1 * (trainY == 3) - 1 * (trainY == 8)
	\end{minted}
	Python's unpacking operator and list comprehensions were very useful for classifying the data, though constructing a new {\bf A} matrix to multiply by the solution vector {\bf x} would be more efficient in the long run:
	\begin{minted}{Python}
# fit model to A
x, _, _, _ = np.linalg.lstsq(A, Ztrain, rcond=None)
# lambda function that applies x to inputs x1 and x2
classifier = lambda x1, x2: x[0] + x[1] * x1 + x[2] * x2

train_classify = np.sign([classifier(*row) for row in trainX])
	\end{minted}

	\pagebreak

	\subsection*{Results and Analysis}
	The first step was to center the data by subtracting the mean of each column from that column's contents. After this, the data was reduced in dimension using the SVD. Taking the SVD of the dataset as
	\[
		{\bf X} = {\bf U\Sigma V}^T,
	\]
	the reduced data in whatever dimension $K$ can obtained as the matrix product of ${\bf U}[:, :K]$ and ${\bf \Sigma}[:K, :K]$. In this case we used the rank-2 truncation of the SVD, giving 2-dimensional data.\\\\
	The next step was fitting a linear model to this reduced data. This was done by solving the least squares problem $\min_x\|{\bf Ax} - {\bf b}\|_2$, where {\bf A}'s columns are a column of 1's, followed by the columns of the reduced version of {\bf X}. The entries in {\bf b} are given by the ``true classification'' variable \texttt{Ztrain}, which has entries of 1 if the corresponding row of {\bf X} was a 3, and -1 if the corresponding row of {\bf X} was a 8. The full values of {\bf A} and {\bf b} can be found in the associated Python file. In this case,
	\[
		{\bf x} = \begin{bmatrix}
			\alpha_0 \\ \alpha_1 \\ \alpha_2
		\end{bmatrix} =
		\begin{bmatrix}
			0.02336839 \\ -0.00100923 \\ 0.0007924
		\end{bmatrix},
	\]
	giving us the coefficients for our linear model
	\[
		z = \alpha_0 + \alpha_1 x_1 + \alpha_2 {x_2},
	\]
	where $x_1$ is the value of the first principal component in the image, and $x_2$ is the value of the second. These are represented as the first and second columns of our reduced data matrix. This model classifies the image as a three if $z$ is positive, and an eight if $z$ is negative. Plotting the data gives an idea of how good of a model this actually is:
	\begin{figure}[H]
		\centering
		\includegraphics[width=12cm]{digit_scatterplot.png}
		\caption{All digits plotted according to their first 2 principal components. The decision boundary was obtained by substituting 0 for $z$ in the linear model, and then solving for $x_2$ as a function of $x_1$. In the classification function, any point over the black line will be considered a 3, and any point under it an 8.}
	\end{figure}
	The line seems to bisect the data as much as possible, but there is a considerable amount of overlap between the points for each digit, meaning that no linear model will be able to tell them apart that well.
	We can see this imperfection reflected in the results when we use the model to classify the training and testing data.
	The model classifies 12.4\% of the training 3's as 8's, and 15.8\% of the training 8's as 3's. The overall accuracy rate on the training data is 85.9\%.
	With the testing data, 12.5\% of 3's are incorrectly classified as 8's and 13.5\% of 8's are incorrectly classified as 3's, and the overall accuracy rate is 87.1\%.
	Pretty darn good! It's somewhat surprising that the model did a bit better on the testing data, but a welcome result.
	They seem accurate to the amount of overlap between the two sets of points. \\\\
	We can get a further idea of what's going on if we construct the ``images'' represented by the first and second right singular vectors, represented as the first and second rows of ${\bf V}^T$:
	\begin{figure}[H]
		\centering
		\includegraphics[width=12cm]{eigendigits.png}
		\caption{The first vector seems to hold mostly the data for an 8, though it is a bit muddy. The second vector's shows a well-defined three, with an eight in the background. This means that if we were to go down to one dimension, we'd loose almost all of our ability to distinguish between the two.}
	\end{figure} 
	\subsection*{Conclusions}
	This project gave a fascinating insight into some of the mechanics behind a simple implementation of OCR using simple Linear Algebra concepts. The results were surprisingly good, though obviously lacking compared to the best attempts to classify digits from the \texttt{mnist} dataset, which I read about on Wikipedia. The best solutions seemed to utilize multiple different types of neural networks as a sort of committee, likely applying the same methods we learned earlier for variance reduction and utilizing the ``wisdom of groups.''

\end{document}