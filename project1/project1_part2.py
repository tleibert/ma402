"""
AUTHOR: Trevor Leibert (tmleibe2)
ASSIGNMENT: MA 402 Project 1
"""
# %% imports
import numpy as np
import matplotlib.pyplot as plt

# %% setup data
mytanh = lambda x : (np.exp(2*x) - 1) / (np.exp(2*x) + 1)

smol_range = np.linspace(-1e-15, 1e-15, 10000)
big_range = np.linspace(-500, 500, 1000)

# %% plot
fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(6,6))
ax1.plot(smol_range, mytanh(smol_range), label="mytanh", linewidth=2)
ax1.plot(smol_range, np.tanh(smol_range), label="numpy.tanh", linewidth=2)
ax1.set_title("Comparing mytanh and numpy.tanh")
ax1.set_xlabel("Input value")
ax1.set_ylabel("Function output")
ax1.legend(fontsize=10)
ax2.plot(big_range, mytanh(big_range), label="mytanh", linewidth=2)
ax2.set_title("mytanh over a larger range")
ax2.set_xlabel("Input value")
ax2.legend(fontsize=10)
ax2.set_ylabel("Function output")
ax2.set_xticks(np.linspace(-500,500,11))
plt.tight_layout()
plt.savefig("comparing_tanh.png", transparent=False, edgecolor="w", facecolor="w")
plt.show()

# %% do the above for mytanh2
mytanh2 = lambda x : (1 - np.exp(-2*x)) / (1 + np.exp(-2*x))
fig2, (ax12, ax22) = plt.subplots(2, 1, figsize=(6,6))

ax12.plot(smol_range, mytanh2(smol_range), label="mytanh2", linewidth=2)
ax12.plot(smol_range, np.tanh(smol_range), label="numpy.tanh", linewidth=2)
ax12.set_title("Comparing mytanh2 and numpy.tanh")
ax12.set_xlabel("Input value")
ax12.set_ylabel("Function output")
ax12.legend(fontsize=10)
ax22.plot(big_range, mytanh2(big_range), label="mytanh2", linewidth=2)
ax22.set_title("mytanh2 over a larger range")
ax22.set_xlabel("Input value")
ax22.legend(fontsize=10)
ax22.set_ylabel("Function output")
ax22.set_xticks(np.linspace(-500,500,11))
plt.tight_layout()
plt.savefig("comparing_tanh2.png", transparent=False, edgecolor="w", facecolor="w")
plt.show()

# %% use both tanh approximations together for increased accuracy

# Tanh function that uses both earlier functions,
# mytanh and mytanh2, to get a more accurate approximation.
# designed to operate on a numpy array
mytanh3 = lambda x : mytanh(x * (x < 0)) + mytanh2(x * (x >= 0))
fig3, (ax13, ax23) = plt.subplots(2, 1, figsize=(6,6))

ax13.plot(smol_range, mytanh3(smol_range), label="mytanh2", linewidth=2)
ax13.plot(smol_range, np.tanh(smol_range), label="numpy.tanh", linewidth=2)
ax13.set_title("Comparing mytanh3 and numpy.tanh")
ax13.set_xlabel("Input value")
ax13.set_ylabel("Function output")
ax13.legend(fontsize=10)
ax23.plot(big_range, mytanh3(big_range), label="mytanh3", linewidth=2)
ax23.set_title("mytanh3 over a larger range")
ax23.legend(fontsize=10)
ax23.set_xlabel("Input value")
ax23.set_ylabel("Function output")
ax23.set_xticks(np.linspace(-500,500,11))
plt.tight_layout()
plt.savefig("comparing_tanh3.png", transparent=False, edgecolor="w", facecolor="w")
plt.show()
# %%
