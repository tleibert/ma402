"""
AUTHOR: Trevor Leibert (tmleibe2)
ASSIGNMENT: MA 402 Project 1
"""
import numpy as np


def myroots(a, b, c):
    """
    Finds the roots of a quadratic equation of the form
    ax^2 + bx + c
    where a != 0. Returns smaller root first.
    """
    root_term = np.sqrt(b ** 2 - 4 * a * c)
    x_plus = (-b + root_term) / (2 * a)
    x_minus = (-b - root_term) / (2 * a)
    
    if x_minus < x_plus:
        return x_minus, x_plus
    return x_plus, x_minus


def myroots_acc(a, b, c):
    """
    Finds the roots of a quadratic equation using a method
    immune to negation.

    I sourced this algorithm from the spring 2020 lecture 04 notes.
    Returns the smaller root first.
    """
    root_term = (np.sign(b) if b != 0 else 1) * np.sqrt(b ** 2 - 4 * a * c)
    x_plus = (-b - root_term) / (2 * a)
    x_minus = c / (a * x_plus)
    
    if x_minus < x_plus:
        return x_minus, x_plus
    return x_plus, x_minus


def main():
    data = [
        (2, -6, 4),
        (3, 1e14, -1),
        (1e-14, 9, 3),
        (1, 2, -1e-14),
        (1e8 + 1, 2e8, 1e8 - 1),
    ]
    myroots_results = [myroots(*entry) for entry in data]
    myroots_acc_results = [myroots_acc(*entry) for entry in data]
    numpy_results = [tuple(sorted(np.roots(entry))) for entry in data]
    
    print(myroots_results)
    print(myroots_acc_results)
    print(numpy_results)
    with open("roots_results.txt", "w") as out:
        for i in range(5):
            out.write(
                f"${i+1}_{{low}}$ & {myroots_results[i][0]} & {myroots_acc_results[i][0]} & {numpy_results[i][0]} \\\\\n"
            )
            out.write(
                f"${i+1}_{{high}}$ & {myroots_results[i][1]} & {myroots_acc_results[i][1]} & {numpy_results[i][1]} \\\\\n"
            )


if __name__ == "__main__":
    main()
