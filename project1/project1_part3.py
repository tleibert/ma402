"""
AUTHOR: Trevor Leibert (tmleibe2)
ASSIGNMENT: MA 402 Project 1
"""
# %% imports
import numpy as np
import matplotlib.pyplot as plt

# %% setup part b
f = lambda x : np.log(1 + x)
h1 = np.logspace(-15, -1, 15)
h1_relative_errors = np.abs((f(h1) - np.log1p(h1)) / np.log1p(h1))

# %% plot part b
fig, ax = plt.subplots(1, 1)
ax.loglog(h1, h1_relative_errors, label="Relative error of f(x) = log(1 + x)")
ax.legend()
ax.set_xlabel("Input value of x (integer powers of 10 between -15 and -1, inclusive)", size=11)
ax.set_ylabel("Relative error of log(1+x) vs log1p(x)", size=11)
ax.set_title("Relative errors of log(1 + x) vs log1p(x) for powers of 10", size=16)
plt.tight_layout()
plt.savefig("relative_error_base10.png", transparent=False, edgecolor="w", facecolor="w")

# %% setup part d
h2 = np.logspace(-52, -3, 50, base=2)
h2_eval = f(h2)
h2_log1p = np.log1p(h2)
relerr = h2_eval / h2_log1p
h2_relative_errors = np.abs(1 - relerr)

# %% plot part d
fig2, ax2 = plt.subplots(1, 1)
ax2.loglog(h2_relative_errors, label="Relative error of f(x) = log(1 + x) for base-2 inputs")
ax2.legend()
ax2.set_ylabel("Relative error of log(1+x) vs log1p(x)", size=11)
ax2.set_xlabel("Input value of X (integer powers of 2 between -52 and -3, inclusive)", size=11)
ax2.set_title("Relative errors of log(1 + x) vs log1p(x) for powers of 2", size=16)
plt.tight_layout()
plt.savefig("relative_error_base2.png", transparent=False, edgecolor="w", facecolor="w")
plt.show()

# %%
