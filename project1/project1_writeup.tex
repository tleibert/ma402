\documentclass{article}[10pt]
\usepackage{enumitem}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{minted}
\usepackage{graphicx}
\usepackage{float}

\setlength{\parindent}{0pt}

\begin{document}
	\title{Project \#1}
	\author{Trevor Leibert}	
	\date{\today}
	
	\maketitle
	\begin{center}
		I had no collaborators in the making of any part of this project.
	\end{center}

	\pagebreak
	\section*{Part 1 Writeup}
	\subsection*{Introduction}
		This writeup serves as a quick demonstration of the limitations of floating-point arithmetic, and its susceptibility to rounding error and catastrophic cancellation.
		Its goal is to showcase how careful algorithm design is needed when working with floating point numbers in order to account for these issues.
		To prove the point, two Python 3 implementations of the quadratic formula will be tested, along with the ``reference'' implementation provided by NumPy.
	
	\subsection*{Python Implementations}
	The following is a naive, straightforward implementation of the quadratic formula in Python 3, with NumPy imported as ``np''.
	\begin{minted}{Python}
def myroots(a, b, c):
	"""
	Finds the roots of a quadratic equation of the form
	ax^2 + bx + c
	where a != 0. Returns smaller root first.
	"""
	root_term = np.sqrt(b ** 2 - 4 * a * c)
	x_plus = (-b + root_term) / (2 * a)
	x_minus = (-b - root_term) / (2 * a)

	if x_minus < x_plus:
		return x_minus, x_plus
	return x_plus, x_minus
	\end{minted}
	A second implementation was also used, designed to avoid cancellation in the case of the term $-b$ being very close (but opposite) to the $\sqrt{b^2 - 4ac}$ term.
	\begin{minted}{Python}
def myroots_acc(a, b, c):
	"""
	Finds the roots of a quadratic equation using a method
	immune to negation.

	I sourced this algorithm from the spring 2020 lecture 04 notes.
	Returns the smaller root first.
	"""
	root_term = (np.sign(b) if b != 0 else 1) * np.sqrt(b ** 2 - 4 * a * c)
	x_plus = (-b - root_term) / (2 * a)
	x_minus = c / (a * x_plus)
	
	if x_minus < x_plus:
		return x_minus, x_plus
	return x_plus, x_minus
	\end{minted}

	\subsection*{Results}
		The following inputs were be used for a quadratic equation of the form $ax^2 + bx + c$:
		\begin{center}
			\begin{tabular}{c|c|c|c}
				trial & $a$ & $b$ & $c$ \\\hline
				1 & 2 & -6 & 4 \\
				2 & 3 & $10^{14}$ & -1 \\
				3 & $10^{-14}$ & 9 & -3 \\
				4 & 1 & 2 & $-10^{-14}$ \\
				5 & $10^8+1$& $2\cdot 10^8$& $10^8-1$\\\hline
			\end{tabular}
		\end{center}

		
		The results were collected under Python 3.8.5 on Ubuntu Linux. They have been arranged with the smaller of the two roots first, followed by the larger of the two second.
		\begin{center}
			\begin{tabular}{l|l|l|l}
				trial & \texttt{myroots} & \texttt{myroots\_acc} & \texttt{numpy.roots}\\\hline
				$1_{low}$ & 1.0 & 1.0 & 1.0 \\
				$1_{high}$ & 2.0 & 2.0 & 2.0 \\
				$2_{low}$ & -33333333333333.332 & -33333333333333.332 & -33333333333333.33 \\
				$2_{high}$ & 0.0 & 1e-14 & 1.0000000000000002e-14 \\
				$3_{low}$ & -899999999999999.6 & -899999999999999.6 & -899999999999999.8 \\
				$3_{high}$ & -0.3552713678800501 & -0.3333333333333335 & -0.3333333333333334 \\
				$4_{low}$ & -2.000000000000005 & -2.000000000000005 & -2.000000000000005 \\
				$4_{high}$ & 4.884981308350689e-15 & 4.999999999999988e-15 & 4.999999999999988e-15 \\
				$5_{low}$ & -0.9999999900000001 & -0.9999999900000001 & -0.9999999900000002 \\
				$5_{high}$ & -0.9999999900000001 & -0.99999999 & -0.9999999900000002 \\\hline
			\end{tabular}
		\end{center}
		\subsubsection*{Verification}
			To verify the results I will find the roots of equation 1 by hand.\
			\begin{enumerate}
				\item Start with the equation: $f(x)=2x^2-6x+4$
				\item Factor out the 2: $f(x)=2(x^2-3x+2)$
				\item Factor inside the parentheses: $f(x)=2(x-1)(x-2)$
			\end{enumerate}
			We can clearly see the roots are at 1 and 2, as seen in the results of all three functions.
	\subsection*{Analysis and Conclusion}
		Note I will refer to the answers given by NumPy's roots function as the correct answer.\\\\
		Both implementations get the correct answer for equation 1, as it is the simplest one using a factorable equation with numbers sufficiently spaced (as in the spacing between the top two terms in $\frac{-b \pm \sqrt{b^2 - 4ac}}{2a}$).
		The naive implementation of \texttt{myroots} suffers catastrophic cancellation when calculating the higher-valued root of equation 2, while the reworked implementation in \texttt{myroots\_acc} does not and gets very very close to NumPy's answer.
		\texttt{myroots} also seems to experience dificulty calculating the higher roots for equations 3 and 4, probably also due to poor conditioning in the subtraction operation it utilizes, while \texttt{myroots\_acc} does not.
		\\
		All three implementations get equation 5 wrong. The steps to working it out are below.
		\begin{enumerate}
			\item The equation is $y = (10^8 + 1)x^2 + (2 \cdot 10^8)x + (10^8 - 1)$
			\item Plug this into the quadratic formula: $\frac{-(2\cdot10^8) \pm \sqrt{(2\cdot10^8)^2 - 4\cdot(10^8 + 1)\cdot(10^8 - 1)}}{2\cdot(10^8 + 1)}$
			\item Expand inside the sqrt: $\frac{-(2\cdot10^8)\pm\sqrt{4\cdot10^{16} - 4(10^{16} - 1)}}{2\cdot(10^8 + 1)}$
			\item $\frac{-(2\cdot10^8)\pm\sqrt{4\cdot10^{16} - 4 \cdot 10^{16} + 4)}}{2\cdot 10^8 + 2}$
			\item Simplify: $\frac{-2\cdot10^8\pm\sqrt{4}}{2\cdot 10^8 + 2}$
			\item $\frac{-2\cdot10^8 \pm 2}{2\cdot 10^8 + 2}$
		\end{enumerate}
		Clearly, one of the answers, $\frac{-2\cdot10^8-2}{2\cdot10^8+2}$, is equal to -1: $(2\cdot10^8+2)(\frac{-1}{1})$. The other, $\frac{-2\cdot10^8+2}{2\cdot10^8+2}$, is very close to to -1, but a little bit bigger: ($\frac{-199999998}{200000002}$). However all three implementations did not find the root at exactly -1. This is because the terms inside the square root cancelled during the poorly-conditioned subtraction of $4\cdot10^{16} - 4(10^{16} - 1)$.
		This is because the number $10^{16} + 1$ cannot be accurately represented as a 64 bit floating point number, due to a lack of precision, and the quantity resulting from the addition is rounded back to 1. This can be proven by taking the value of Matlab's \texttt{eps} function, or NumPy's \texttt{spacing} function at $10^{16}$:
		\\\texttt{>>> spacing(1e16)\\
		2.0}
		\texttt{eps} or \texttt{spacing} displays the distance to the next representable floating point number, when given a floating point number as an input. This result means that the next representable floating point after $10^{16}$ is $10^{16} + 2$. This means that the result of $10^{16} + 1$ must be rounded, and it is rounded towards the nearest ``even'' floating point number, $10^{16}$. We know $10^{16}$ is even because its fractional part is all zeros, as it has only one significant digit.\\\\
		\bfseries{Note that the source code for this part is in the file \texttt{project1\_part1.py}}
	\pagebreak
	
	\section*{Part 2 Writeup}
		\subsection*{Introduction}
			This part of the assignment shows the importance of using a well-conditioned function where precision is required, and the power of using Matlab/NumPy's array operations and conditionals to implement special cases for each input to ensure good conditioning.
			To make this point, the hyperbolic tangent, and one implementation of it, 
			\begin{equation*}
				\tanh(x) = \frac{e^{2x} -1 }{e^{2x} + 1},
			\end{equation*} will be discussed.
		\subsection*{Python Implementation}
			\bfseries{The full implementation is in the file \texttt{project1\_part2.py}.}
			Here are the three different implementations of the hyperbolic 	tangent function:
			\begin{minted}{Python}
mytanh = lambda x : (np.exp(2*x) - 1) / (np.exp(2*x) + 1)
mytanh2 = lambda x : (1 - np.exp(-2*x)) / (1 + np.exp(-2*x))
mytanh3 = lambda x : mytanh(x * (x < 0)) + mytanh2(x * (x >= 0))
			\end{minted}
			The implementation of all three functions utilize NumPy array operations, and will not work on basic python data structures. \texttt{mytanh3} uses both of the prior implementations where they are at their strongest and most well-conditioned, while rejecting their use where they are inaccurate.
		\subsection*{Results}
			\begin{figure}[H]
				\noindent\makebox[\textwidth]{%
				\includegraphics[width=1.4\textwidth]{comparing_tanh.png}}
				\caption[]{Pictured above is the plot of \texttt{mytanh}. This function represents \\
					\centering$tanh(x) = \frac{e^{2x} -1 }{e^{2x} + 1}$
				}
			\end{figure}

			\begin{figure}[H]
				\noindent\makebox[\textwidth]{%
				\includegraphics[width=1.4\textwidth]{comparing_tanh2.png}}
				\caption{Pictured above is the plot of \texttt{mytanh2}. This function represents \\
				\centering$tanh(x) = \frac{1 - e^{-2x}}{1 + e^{-2x}}$
			}
			\end{figure}
			\begin{figure}[H]
				\noindent\makebox[\textwidth]{%
				\includegraphics[width=1.4\textwidth]{comparing_tanh3.png}}
				\caption{Pictured above is the plot of \texttt{mytanh3}. It shows that \texttt{mytanh3} takes the left side of \texttt{mytanh2} and the right side of \texttt{mytanh} to get a better approximation of the true value of tanh. This function represents \texttt{mytanh2} when x is greater than zero, and \texttt{mytanh} when x is less than zero.
				}
			\end{figure}

		\subsection*{Analysis and Conclusion}
			In all three graphs three functions look like step functions approximating the plot of the hyperbolic tangent.
			This is because the subtraction  in the numerator of our function $|\frac{e^{2x} - 1}{e^{2x} + 1}|$ is poorly conditioned as x gets to be very small, causing a large loss in precision, which, combined with rounding error, causes our values to run together in this step-like manner.
			The second form of the function, used by \texttt{mytanh2}, uses a similar function, $|\frac{1 - e^{-2x}}{1 + e^{-2x}}|$. The steps are larger for the first function when x is greater than zero, while they are smaller in that region for the second function, and larger for the second function in the region where x is less than zero.
			The condition numbers also line up here.
			When x is greater than zero, the condition number of the subtraction in the first function is greater than one, while it is less than one in that function when x is less than zero.
			In the second function, the condition number of the subtraction is less than one when x is greater than zero, and greater than one when x is less than zero.
			This explains the steps in the positive x region being larger in the first function, and the steps in the positive x region being smaller in the second function.
			Combining the two functions together, utilizing boolean element-wise multiplication, we can have the best of both worlds, as shown in \texttt{mytanh3}.
		
	\pagebreak

	\section*{Part 3 Writeup}
		\subsection*{Introduction}
		This part of the assignment shows how unexpected errors in computations can arise when using straightforward ``self-implementations'' of functionality provided by a specialized library. For this example, the NumPy / Matlab function \texttt{log1p} will be examined. \texttt{log1p} implements the function
		\begin{equation*}
			f(x) = \log(1+x)
		\end{equation*}
		in such a manner that it is accurate for x values very close to zero, a point of concern when using that function directly when working with floating point values.

		\subsection*{Python Implementation}
			I will be comparing the following function to \texttt{numpy.log1p}:
			\begin{minted}{Python}
			f = lambda x : np.log(1 + x)
			\end{minted}
			The full source code can be found in the file \texttt{project1\_part3.py}

		\subsection*{Results}
			\begin{figure}[H]
				\noindent\makebox[\textwidth]{%
				\includegraphics[width=1.4\textwidth]{relative_error_base10.png}}
				\caption[]{Pictured above are the relative errors in the return from the python function \texttt{f(x)} implementing $\log{1 + x}$, compared to the ``true'' values returned by \texttt{numpy.log1p}. All input values for this graph came from the set of points $\texttt{h1} = \{10^{-15},10^{-14},\ldots,10^{-1}\}$.
				}
			\end{figure}

			\begin{figure}[H]
				\noindent\makebox[\textwidth]{%
				\includegraphics[width=1.4\textwidth]{relative_error_base2.png}}
				\caption{This graph shows the same relative error calculation as the one before it, but interestingly, the relative (and absolute) error for each point was zero. This means that, as this is a \texttt{loglog} graph, the line at zero is not visible, as a log cannot ever reach zero. The inputs for this graph came from the set of points $\texttt{h2} = \{2^{-52},2^{-51},\ldots,2^{-3}\}$.
			}
			\end{figure}
		\pagebreak
		\subsection*{Analysis and Conclusion}
			We can clearly see much larger relative errors on the first graph than we would expect for the simple operation of adding two positive doubles together and then taking their natural logarithm.
			In addition to this, we also see \emph{no} errors represented at all on the second graph.
			I believe these errors originate from the nature of the input. In a floating point number, the only fractions that can be accurately represented are those which are powers of two.
			Any other kind, like $\frac{1}{3}$ or $\frac{1}{10}$ for example, cannot be represented to exact precision like $\frac{1}{2}$, $\frac{1}{256}$, and $\frac{1}{2^{52}}$ all can (and any power of two or combination of powers of two).
			The second part of the problem can be explained the relative condition number of the function $\log(1 + x)$, though that is not the function that we are evaluating.
			The relative condition number for a function $f(x)$ is defined as
			\begin{equation*}
				\kappa_{rel} = \frac{x \cdot f^\prime(x)}{f(x)}.
			\end{equation*}
			For $\log(x)$, the part of the function we're interested in (we are not interested in the (1 + x) part, as it is well-conditioned when x is positive), the condition number is
			\begin{equation*}
				\kappa_{rel} = \frac{1}{\log(x)}.
			\end{equation*}
			This number gets to be very large as x approaches 1 from positive $\infty$, as the value of $\log(x)$ goes to 0. This means that any errors in the input to the function will be magnified greatly in the output.
			This magnification explains the errors that we see when using negative powers of 10 as inputs to \texttt{f(x)}.
			It also explains why we do not see any errors at all in the powers of 2.
			Those negative powers of 2 are represented exactly, so there is \emph{no error} to be magnified.
				
\end{document}